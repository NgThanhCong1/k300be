﻿using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Models;
using Models.RequestModels;
using Models.ResponseModels;
using Repository;
using Repository.Implementions;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace K300.UnitTest.RepositoryTest
{
    [TestClass]
    public class ComplexFunctionTest
    {
        IUnitOfWork unitOfWork;
        [TestInitialize]
        public void Initialize()
        {
            unitOfWork = new UnitOfWork("Data Source=LAPTOP-D490U9N2\\SQLEXPRESS;Initial Catalog=K300;Integrated Security=True");
        }
        //Comment
        [TestMethod]
        public async Task Repository_GetCommentByProductIdTest()
        {
            var result = await unitOfWork.CommentRepository.GetCommentByProductId(337);
            unitOfWork.Commit();
            Assert.IsNotNull(result);
            Assert.AreEqual(4, result.ToList().Count);
        }
        //Discount
        [TestMethod]
        public async Task Repository_SearchDiscountTest()
        {
            var result = await unitOfWork.DiscountRepository.SearchDiscount("discountcode1z");
            unitOfWork.Commit();
            Assert.IsNotNull(result);
            Assert.AreEqual("discountcode1z", result.DiscountCode);
        }
        //General
        [TestMethod]
        public async Task Repository_GetDataForDashBoardTest()
        {
            var result = await unitOfWork.GeneralRepository.GetDataForDashBoard();

            var statics = (await result.ReadAsync<Static>()).FirstOrDefault();
            var listInInvoices = (await result.ReadAsync<InInvoice>()).ToList();
            var listSubProductStatics = (await result.ReadAsync<SubProductStatic>()).ToList();
            var listSearchStaticDashBoards = (await result.ReadAsync<SearchStaticDashBoard>()).ToList();
            var listCategoryStatics = (await result.ReadAsync<CategoryStatic>()).ToList();
            unitOfWork.Commit();

            Assert.IsNotNull(statics);
            Assert.IsNotNull(listInInvoices);
            Assert.IsNotNull(listSubProductStatics);
            Assert.IsNotNull(listSearchStaticDashBoards);
            Assert.IsNotNull(listCategoryStatics);

            Assert.AreEqual(6, statics.UserCount);
            Assert.AreEqual(3, listInInvoices.Count);
            Assert.AreEqual(6, listSubProductStatics.Count);
            Assert.AreEqual(9, listSearchStaticDashBoards.Count);
            Assert.AreEqual(2, listCategoryStatics.Count);
        }
        //Import product
        [TestMethod]
        public async Task Repository_ImportProductTest()
        {
            List<ImportProductNew> listImportProduct = new List<ImportProductNew>()
            {
                new ImportProductNew()
                {
                    InPrice = "100",
                    Quantity = 3,
                    ProductCode = "ETWDIAW-GREY-L"
                },
                new ImportProductNew()
                {
                    InPrice = "200",
                    Quantity = 5,
                    ProductCode = "ETWDIAW-GREY-XXL"
                }
            };
            var result = await unitOfWork.ImportRepository.ImportProductNew(listImportProduct, 31, "cong unittest");
            unitOfWork.Commit();
            Assert.AreEqual(2, result);
        }
        [TestMethod]
        public async Task Repository_GetInInvoiceDetailByInInvoiceIdTest()
        {
            var result = await unitOfWork.ImportRepository.GetInInvoiceDetailByInInvoiceId(1156);
            unitOfWork.Commit();
            Assert.AreEqual(2, result.Count());
        }
        //Outport product
        [TestMethod]
        public async Task Repository_AddOutInvoiceTest()
        {
            OutInvoiceRequest outInvoice = new OutInvoiceRequest()
            {
                TotalMoney = "1000",
                DiscountId = 0,
                CustomerId = "6f00ac33-4685-489b-a6b6-d90ac939a4cc",
                ShipAt = "ship at test",
                ShipName = "cong test",
                ShipPhone = "phone test",
                ShippingCharge = "shipingcharge test"
            };
            var result = await unitOfWork.OutportRepository.AddOutInvoice(outInvoice);
            unitOfWork.Commit();
            Assert.AreEqual(242, result);
        }
        [TestMethod]
        public async Task Repository_AddOutInvoiceDetailTest()
        {
            List<OutportProduct> outportProducts = new List<OutportProduct>()
            {
                new OutportProduct()
                {
                    ProductCode = "ETWDIAW-GREY-XXL",
                    CartQuantity = 2,
                    Price = "500"
                },
                new OutportProduct()
                {
                    ProductCode = "ETWDIAW-GREY-L",
                    CartQuantity = 1,
                    Price = "600"
                },
            };
            var result = await unitOfWork.OutportRepository.AddOutInvoiceDetail(173, outportProducts, "cong test");
            unitOfWork.Commit();
            Assert.AreEqual("onepiecelolrcb@gmail.com", result);
        }
        [TestMethod]
        public async Task Repository_GetOutInvoiceDetailByOutInvoiceIdTest()
        {
            var result = await unitOfWork.OutportRepository.GetOutInvoiceDetailByOutInvoiceId(173);

            var outInvoiceMaster = await result.ReadAsync<OutInvoiceMaster>();
            var listSubProducts = await result.ReadAsync<SubProduct>();
            unitOfWork.Commit();

            Assert.AreEqual("onepiecelolrcb@gmail.com", outInvoiceMaster.FirstOrDefault().Email);
            Assert.AreEqual(2, listSubProducts.Count());
        }
        //Product
        [TestMethod]
        public async Task Repository_GetProductsBySlugTest()
        {
            var result = await unitOfWork.ProductRepository.GetProductsBySlug("test-pro-with-slug-co", "6f00ac33-4685-489b-a6b6-d90ac939a4cc");

            var listProductDetails = await result.ReadAsync<ProductDetail>();
            var listRelatedProducts = await result.ReadAsync<CombineProduct>();
            var ratingStatic = await result.ReadAsync<RatingStatic>();
            unitOfWork.Commit();

            Assert.AreEqual("test-pro-with-slug-co", listProductDetails.FirstOrDefault().Slug);
            Assert.AreEqual(5, listProductDetails.Count());
            Assert.AreEqual(4, listRelatedProducts.Count());
            Assert.AreEqual(20, ratingStatic.FirstOrDefault().TotalRatingPoint);
            Assert.AreEqual(6, ratingStatic.FirstOrDefault().TotalRatingCount);
            Assert.AreEqual("1", ratingStatic.FirstOrDefault().IsCustomerRated);
        }
        [TestMethod]
        public async Task Repository_GetProductsByIdAdminTest()
        {
            var result = await unitOfWork.ProductRepository.GetProductsByIdAdmin(319);
            var productNew = await result.ReadAsync<Product_New>();
            var listSubProduct = await result.ReadAsync<SubProduct>();
            unitOfWork.Commit();

            Assert.AreEqual("test-pro-with-slug-co", productNew.FirstOrDefault().Slug);
            Assert.AreEqual(5, listSubProduct.Count());
        }
        [TestMethod]
        public async Task Repository_SearchProductsByNameOrCategoryTest()
        {
            var result = await unitOfWork.ProductRepository.SearchProductsByNameOrCategory("T-SHIRTS", "t");
            unitOfWork.Commit();

            Assert.AreEqual(6, result.Count());
        }
    }
}
