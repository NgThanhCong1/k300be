﻿using Models.BaseModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Ultilities.Interfaces;

namespace Ultilities.Implementions
{
    public class SendMailHelper : ISendMailHelper
    {
        public async Task SendMail(DataToGmail gmail)
        {
            try
            {
                string body = string.Empty;
                using (StreamReader reader = new StreamReader($"{gmail.mailTemplate}"))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{Title}", gmail.title);
                body = body.Replace("{MainContent}", gmail.mainContent);
                body = body.Replace("{ListProducts}", gmail.mainContent);
                body = body.Replace("{outInvoiceId}", gmail.outInvoiceId);
                body = body.Replace("{deliveryAddress}", gmail.deliveryAddress);
                body = body.Replace("{deliveryDate}", DateTime.Now.AddDays(5).ToShortDateString());
                body = body.Replace("{totalMoney}", gmail.totalMoney);
                MailMessage mailMessage = new MailMessage(
                from: "0306181003@caothang.edu.vn",
                to: gmail.to,
                subject: gmail.subject,
                body: body
                );
                mailMessage.BodyEncoding = System.Text.Encoding.UTF8;
                mailMessage.SubjectEncoding = System.Text.Encoding.UTF8;
                mailMessage.IsBodyHtml = true;
                mailMessage.ReplyToList.Add(new MailAddress(gmail.from));
                mailMessage.Sender = new MailAddress(gmail.from);
                if(gmail.path != null)
                {
                    mailMessage.Attachments.Add(new Attachment(gmail.path));
                }

                using (SmtpClient client = new SmtpClient("smtp.gmail.com"))
                {
                    client.Port = 587;
                    client.UseDefaultCredentials = true;
                    client.Credentials = new NetworkCredential("k300nonreply@gmail.com", "confirmcc9");
                    client.EnableSsl = true;
                    await client.SendMailAsync(mailMessage);
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
