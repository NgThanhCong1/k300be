﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;
using Ultilities.Interfaces;

namespace Ultilities.Implementions
{
    public class BotTelegramHelper : IBotTelegramHelper
    {
        private TelegramBotClient _bot;
        private IConfiguration _configuration;
        public BotTelegramHelper(IConfiguration configuration)
        {
            _configuration = configuration;
            _bot = new TelegramBotClient(_configuration["bot_telegram:token"]);
        }
        public async Task TelegramBotSendError(string errorMessage, [CallerMemberName] string callerName = "")
        {
            string sendMessage = @"[DEV BUGGING] BOT Có lỗi xảy ra tại API:  " + callerName + " \n\rNội dung lỗi:  " + errorMessage;
            await _bot.SendTextMessageAsync(_configuration["bot_telegram:idChatRoom"], sendMessage);
        }
    }
}
