﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;
using Google.Apis.Util.Store;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Ultilities.Interfaces;

namespace Ultilities.Implementions
{
    public class GGSheetHelper : IGGSheetHelper
    {
        static string[] Scopes = { SheetsService.Scope.SpreadsheetsReadonly };
        static string ApplicationName = "Web client 1";
        UserCredential credential;

        public async Task<IEnumerable<dynamic>> ReadData(string link, string sheetName, string rangeData)
        {
            using (var stream =
                new FileStream("../K300/client_secret_831869497751-2djv83tpn6rddvsqfoq5ktl946q2dcmb.apps.googleusercontent.com.json", FileMode.Open, FileAccess.Read))
            {
                // The file token.json stores the user's access and refresh tokens, and is created
                // automatically when the authorization flow completes for the first time.
                string credPath = "token.json";
                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    Scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(credPath, true)).Result;
                Console.WriteLine("Credential file saved to: " + credPath);
            }

            // Create Google Sheets API service.
            var service = new SheetsService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName,
            });

            // Define request parameters.
            //String spreadsheetId = "1R-8KbVelLPcWYFK3fGpCQ53Uh2sO4FWt6tU2Oxymnz4";
            String spreadsheetId = link.Split("/")[^2];
            String range = $"{sheetName}!{rangeData}";
            SpreadsheetsResource.ValuesResource.GetRequest request =
                    service.Spreadsheets.Values.Get(spreadsheetId, range);

            // Prints the names and majors of students in a sample spreadsheet:
            // https://docs.google.com/spreadsheets/d/1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms/edit
            ValueRange response =  request.Execute();
            IList<IList<dynamic>> values = response.Values;
            
            return values;
        }
    }
}
