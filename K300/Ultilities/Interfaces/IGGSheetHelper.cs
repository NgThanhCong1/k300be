﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ultilities.Interfaces
{
    public interface IGGSheetHelper
    {
        Task<IEnumerable<dynamic>> ReadData(string link, string sheetName, string rangeData);
    }
}
