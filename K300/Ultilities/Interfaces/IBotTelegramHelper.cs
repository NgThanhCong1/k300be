﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Ultilities.Interfaces
{
    public interface IBotTelegramHelper
    {
        Task TelegramBotSendError(string errorMessage, [CallerMemberName] string callerName = "");
    }
}
