﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Ultilities.Interfaces
{
    public interface IPhoneHelper
    {
        string EncodeNonAsciiCharacters(string value);
        String getUserInfo();
        String sendSMS(String[] phones, String content, int type, String sender);
        String sendMMS(String[] phones, String content, String link, String sender);

    }
}
