﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Ultilities.Interfaces
{
    public interface IElastichsearchHelper
    {
        Task UpErrorToElasticSeearch(Exception ex, object obj, [CallerMemberName] string callerName = "");
    }
}
