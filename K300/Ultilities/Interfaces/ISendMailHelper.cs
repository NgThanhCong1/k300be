﻿using Models.BaseModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ultilities.Interfaces
{
    public interface ISendMailHelper
    {
        Task SendMail(DataToGmail gmail);
    }
}
