﻿using Models.ResponseModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services.Interfaces
{
    public interface IDistributorService
    {
        Task<IEnumerable<Distributor>> GetAll();
        Task<int> UpdateDistributor(Distributor distributor);
        Task<int> AddDistributor(Distributor distributor);
        Task<int> AddListDistributor(List<Distributor> listDistributor);
        Task<IEnumerable<Distributor>> SearchDistributor(Distributor distributor);
        Task<Distributor> GetDistributorById(int id);
        Task<int> DeleteDistributor(Distributor distributor);
    }
}
