﻿using Models.ResponseModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services.Interfaces
{
    public interface IStoreService
    {
        Task<IEnumerable<Store>> GetAllStore();
        Task<int> UpdateStore(Store store);
        Task<int> AddStore(Store store);
        Task<int> DeleteStore(Store store);
        Task<Store> GetStoreById(int id);
    }
}
