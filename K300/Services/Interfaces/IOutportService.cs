﻿using Models.RequestModels;
using Models.ResponseModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services.Interfaces
{
    public interface IOutportService
    {
        Task<IEnumerable<OutInvoiceRequest>> GetAll();
        Task<int> AddOutInvoice(OutInvoiceRequest request);
        Task<string> AddOutInvoiceDetail(int outInvoiceId, List<OutportProduct> listProducts, string createdBy);
        Task<int> UpdateOutInvoice(OutInvoiceRequest model, string type);
        Task<string> GetHistoryChangesByOutInvoiceId(int id);
        Task<OutInvoiceDetailMaster> GetOutInvoiceDetailByOutInvoiceId(int id);
        Task<OutInvoiceRequest> GetOutInvoicById(int id);
        Task<int> UpdateOutInvoiceData(OutInvoiceRequest model);
        Task<IEnumerable<HistoryOrders>> GetHistoryOrders(string userId);
        Task<OutInvoiceForUserTrackingViewModel> GetOutInvoiceByUser(string userId, int _page, int _limit, int status);
        Task<int> AddSearchStatic(SearchStatic searchStatic);
        Task<IEnumerable<SearchStatic>> GetAllSearchStatic();
        Task<IEnumerable<SearchStatic>> GetTop50Keyword();
        Task<IEnumerable<SearchStatic>> GetKeywordsByCustomer(string customerId);
    }
}
