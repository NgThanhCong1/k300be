﻿using Models;
using Models.ResponseModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services.Interfaces
{
    public interface IGeneralService
    {
        Task<DashBoard> GetDataForDashBoard();
        Task<StaticByMonth> GetDataStaticByMonth();
        Task<string> ExportStaticByMonthToExcel(StaticByMonth staticByMonth);
        Task<IEnumerable<SearchStatic>> Get2MonthKeyword();
    }
}
