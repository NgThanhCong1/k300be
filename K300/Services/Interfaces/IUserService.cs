﻿using Models;
using Models.ResponseModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services.Interfaces
{
    public interface IUserService
    {
        Task<IEnumerable<RolePermission>> GetAllPermissionByRole(string role);
        Task<int> AddListRolePermission(List<RolePermission> listRolePermission);
        Task<int> DeleteRolePermission(RolePermission rolePermission);
        Task<int> AddRolePermission(RolePermission rolePermission);
        Task<int> UpdatePermissionsOfRole(List<RolePermission> listRolePermission);

    }
}
