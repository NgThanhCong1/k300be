﻿using Models.ResponseModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services.Interfaces
{
    public interface ICategoryService
    {
        Task<IEnumerable<Category>> GetAll();
        Task<int> UpdateCate(Category cate);
        Task<int> AddCate(Category cate);
        Task<int> AddListCate(List<Category> listCate);
        Task<IEnumerable<Category>> SearchCate(Category cate);
        Task<int> DeleteCate(Category cate);
        Task<List<Category>> GetTop4Cates();
        Task<Category> GetCategoryById(int id);
        Task<IEnumerable<Category>> GetAllCategoryByParentCategory();

    }
}
