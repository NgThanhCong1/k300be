﻿using Models.ResponseModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services.Interfaces
{
    public interface IDiscountService
    {
        Task<IEnumerable<Discount>> GetAll();
        Task<int> UpdateDiscount(Discount discount);
        Task<int> AddDiscount(Discount discount);
        Task<int> AddListDiscount(List<Discount> listDiscount);
        Task<int> DeleteDiscount(Discount discount);
        Task<Discount> GetDiscountById(int id);
        Task<Discount> SearchDiscount(string discountCode);
    }
}
