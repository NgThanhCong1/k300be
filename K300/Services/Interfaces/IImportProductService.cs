﻿using Models.RequestModels;
using Models.ResponseModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services.Interfaces
{
    public interface IImportProductService
    {
        Task<int> ImportProduct(ImportProduct importProduct);
        Task<List<InInvoiceResponse>> GetAllInInvoice();
        Task<int> ImportProductNew(List<ImportProductNew> listImportProduct, int distributorId, string createdBy);
        Task<IEnumerable<ImportDetail>> GetAllImprotDetail();
        Task<List<ProductConfig>> GetProductConfigsByProductCode(string productCode);
        Task<int> UpdateImportDetail(string productCode, int inInvoceId, int productId, string productConfigsInInvoceDetail, string productConfigsInProduct);
        Task<IEnumerable<InInvoice>> GetAll();
        Task<IEnumerable<InInvoiceDetail>> GetInInvoiceDetailByInInvoiceId(int inInvoiceId);

    }
}
