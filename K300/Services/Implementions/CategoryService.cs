﻿using Microsoft.Extensions.Configuration;
using Models.ResponseModels;
using Repository;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using Ultilities;

namespace Services.Implementions
{
    public class CategoryService : ICategoryService
    {
        private readonly string connectionString;
        public CategoryService(IConfiguration configuration)
        {
            connectionString = configuration.GetConnectionString(configuration["SqlServer"]);
        }
        public async Task<IEnumerable<Category>> GetAll()
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.CategoryRepository.GetAllCate();
                    uow.Commit();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> UpdateCate(Category cate)
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.CategoryRepository.UpdateCate(cate);
                    uow.Commit();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> AddCate(Category cate)
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.CategoryRepository.AddCate(cate);
                    uow.Commit();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> AddListCate(List<Category> listCate)
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.CategoryRepository.AddListCate(listCate);
                    uow.Commit();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<IEnumerable<Category>> SearchCate(Category cate)
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.CategoryRepository.SearchCate(cate);
                    uow.Commit();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> DeleteCate(Category cate)
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.CategoryRepository.DeleteCate(cate);
                    uow.Commit();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<List<Category>> GetTop4Cates()
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.CategoryRepository.GetTop4Cates();
                    uow.Commit();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<Category> GetCategoryById(int id)
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.CategoryRepository.GetCategoryById(id);
                    uow.Commit();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<IEnumerable<Category>> GetAllCategoryByParentCategory()
        {
            try 
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.CategoryRepository.GetAllCategoryByParentCategory();
                    var listCategories = await result.ReadAsync<Category>();
                    var listParents = await result.ReadAsync<Category>();

                    var listCategoriesResult = new List<Category>();
                    listParents.ToList().ForEach(i =>
                    {
                        listCategoriesResult.Add(new Category()
                        {
                            Id = i.Id,
                            Name = i.Name,
                            ListCategories = listCategories.Where(s => s.ParentCategory == i.Id).ToList()
                        });
                    });

                    uow.Commit();
                    return listCategoriesResult;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
