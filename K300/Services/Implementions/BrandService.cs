﻿using Microsoft.Extensions.Configuration;
using Models;
using Models.ResponseModels;
using Repository;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Implementions
{
    public class BrandService : IBrandService
    {
        private string connectionString;
        public BrandService(IConfiguration configuration)
        {
            connectionString = configuration.GetConnectionString(configuration["SqlServer"]);
        }
        public async Task<IEnumerable<Brand>> GetAll()
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.BrandRepository.GetAll();
                    uow.Commit();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> UpdateBrand(Brand brand)
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.BrandRepository.UpdateBrand(brand);
                    uow.Commit();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> AddBrand(Brand brand)
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.BrandRepository.AddBrand(brand);
                    uow.Commit();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> AddListBrand(List<Brand> listBrand)
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.BrandRepository.AddListBrand(listBrand);
                    uow.Commit();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<IEnumerable<Brand>> SearchBrand(Brand brand)
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.BrandRepository.SearchBrand(brand);
                    uow.Commit();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> DeleteBrand(Brand brand)
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.BrandRepository.DeleteBrand(brand);
                    uow.Commit();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<Brand> GetBrandById(int id)
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.BrandRepository.GetBrandById(id);
                    uow.Commit();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
