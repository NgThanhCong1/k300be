﻿using Microsoft.Extensions.Configuration;
using Models.RequestModels;
using Models.ResponseModels;
using Newtonsoft.Json;
using Repository;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Implementions
{
    public class OutportService : IOutportService
    {
        private string connectionString;

        public OutportService(IConfiguration configuration)
        {
            connectionString = configuration.GetConnectionString(configuration["SqlServer"]);
        }
        public async Task<int> AddOutInvoice(OutInvoiceRequest request)
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.OutportRepository.AddOutInvoice(request);
                    uow.Commit();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<string> AddOutInvoiceDetail(int outInvoiceId, List<OutportProduct> listProducts, string createdBy)
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.OutportRepository.AddOutInvoiceDetail(outInvoiceId, listProducts, createdBy);
                    uow.Commit();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<IEnumerable<OutInvoiceRequest>> GetAll()
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.OutportRepository.GetAll();
                    uow.Commit();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> UpdateOutInvoice(OutInvoiceRequest model, string type)
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.OutportRepository.UpdateOutInvoice(model, type);
                    uow.Commit();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<string> GetHistoryChangesByOutInvoiceId(int id)
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.OutportRepository.GetHistoryChangesByOutInvoiceId(id);
                    uow.Commit();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<OutInvoiceDetailMaster> GetOutInvoiceDetailByOutInvoiceId(int id)
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.OutportRepository.GetOutInvoiceDetailByOutInvoiceId(id);
                    var outInvoiceMaster = await result.ReadAsync<OutInvoiceMaster>();
                    var listSubProducts = await result.ReadAsync<SubProduct>();
                    var finalResult = new OutInvoiceDetailMaster()
                    {
                        OutInvoiceMaster = outInvoiceMaster.FirstOrDefault(),
                        ListSubProducts = listSubProducts.ToList()
                    };
                    finalResult.ListSubProducts.ForEach(item => item.ListImagesLink = JsonConvert.DeserializeObject<List<string>>(item.ListImages));
                    if (finalResult.OutInvoiceMaster.HistoryChanges != null)
                    {
                        finalResult.ListHistoryChanges = JsonConvert.DeserializeObject<List<OutInvoiceRequest>>(finalResult.OutInvoiceMaster.HistoryChanges);
                    }
                    uow.Commit();
                    return finalResult;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<OutInvoiceRequest> GetOutInvoicById(int id)
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.OutportRepository.GetOutInvoicById(id);
                    uow.Commit();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> UpdateOutInvoiceData(OutInvoiceRequest model)
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.OutportRepository.UpdateOutInvoiceData(model);
                    uow.Commit();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<IEnumerable<HistoryOrders>> GetHistoryOrders(string userId)
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.OutportRepository.GetHistoryOrders(userId);
                    uow.Commit();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<OutInvoiceForUserTrackingViewModel> GetOutInvoiceByUser(string userId, int _page, int _limit, int status)
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.OutportRepository.GetOutInvoiceByUser(userId);
                    var listInvoices = await result.ReadAsync<OutInvoiceForUserTracking>();
                    var totalListInvoices = listInvoices.Count();
                    var listProductInInvoices = await result.ReadAsync<HistoryOrders>();
                    var finalListInvoices = status == 0 ? listInvoices.Where(s => s.Status == 0).Skip((_page - 1) * _limit).Take(_limit).ToList()
                        : status == 1 ? listInvoices.Where(s => s.Status == 1).Skip((_page - 1) * _limit).Take(_limit).ToList()
                        : status == 2 ? listInvoices.Where(s => s.Status == 2).Skip((_page - 1) * _limit).Take(_limit).ToList()
                        : status == 3 ? listInvoices.Where(s => s.Status == 3).Skip((_page - 1) * _limit).Take(_limit).ToList()
                        : status == 4 ? listInvoices.Where(s => s.Status == 4).Skip((_page - 1) * _limit).Take(_limit).ToList()
                        : listInvoices.Skip((_page - 1) * _limit).Take(_limit).ToList();

                    finalListInvoices.ToList().ForEach(_ =>
                    {
                        _.ListHistoryOrders = listProductInInvoices.Where(s => s.OutInvoiceId == _.Id).ToList();
                        _.ListHistoryOrders.ForEach(s => s.ListImagesLink = JsonConvert.DeserializeObject<List<string>>(s.ListImages));
                    });
                    uow.Commit();
                    return new OutInvoiceForUserTrackingViewModel()
                    {
                        Pagination = new Pagination()
                        {
                            _page = _page,
                            _limit = _limit,
                            _totalRows =
                            status == 0 ? listInvoices.Where(s => s.Status == 0).ToList().Count()
                            : status == 1 ? listInvoices.Where(s => s.Status == 1).ToList().Count()
                            : status == 2 ? listInvoices.Where(s => s.Status == 2).ToList().Count()
                            : status == 3 ? listInvoices.Where(s => s.Status == 3).ToList().Count()
                            : status == 4 ? listInvoices.Where(s => s.Status == 4).ToList().Count()
                            : totalListInvoices
                        },
                        ListOutinvoices = finalListInvoices
                    };
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> AddSearchStatic(SearchStatic searchStatic)
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.OutportRepository.AddSearchStatic(searchStatic);
                    uow.Commit();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<IEnumerable<SearchStatic>> GetAllSearchStatic()
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.OutportRepository.GetAllSearchStatic();
                    uow.Commit();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<IEnumerable<SearchStatic>> GetTop50Keyword()
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.OutportRepository.GetTop50Keyword();
                    uow.Commit();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<IEnumerable<SearchStatic>> GetKeywordsByCustomer(string customerId)
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.OutportRepository.GetKeywordsByCustomer(customerId);
                    uow.Commit();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
