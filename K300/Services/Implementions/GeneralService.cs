﻿using ClosedXML.Excel;
using Microsoft.Extensions.Configuration;
using Models;
using Models.RequestModels;
using Models.ResponseModels;
using Newtonsoft.Json;
using Repository;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Implementions
{
    public class GeneralService : IGeneralService
    {
        private string connectionString;

        public GeneralService(IConfiguration configuration)
        {
            connectionString = configuration.GetConnectionString(configuration["SqlServer"]);
        }
        public async Task<DashBoard> GetDataForDashBoard()
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.GeneralRepository.GetDataForDashBoard();
                    var finalResult = new DashBoard()
                    {
                        Static = (await result.ReadAsync<Static>()).FirstOrDefault(),
                        ListInInvoices = (await result.ReadAsync<InInvoice>()).ToList(),
                        ListSubProductStatics = (await result.ReadAsync<SubProductStatic>()).ToList(),
                        ListSearchStaticDashBoards = (await result.ReadAsync<SearchStaticDashBoard>()).ToList(),
                        ListCategoryStatics = (await result.ReadAsync<CategoryStatic>()).ToList(),
                    };
                    finalResult.ListSubProductStatics.ForEach(p => p.ListImagesLink = JsonConvert.DeserializeObject<List<string>>(p.ListImages));
                    uow.Commit();
                    return finalResult;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<StaticByMonth> GetDataStaticByMonth()
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.GeneralRepository.GetDataStaticByMonth();
                    var finalResult = new StaticByMonth()
                    {
                        ListNewCustomers = (await result.ReadAsync<ApplicationUser>()).ToList(),
                        ListNewInInvoices = (await result.ReadAsync<InInvoice>()).ToList(),
                        ListNewOutInvoices = (await result.ReadAsync<OutInvoiceForUserTracking>()).ToList(),
                        ListTopSubProducts = (await result.ReadAsync<SubProductStatic>()).ToList(),
                        ListKeywordRankings = (await result.ReadAsync<SearchStaticDashBoard>()).ToList(),
                        ListCategoryRankings = (await result.ReadAsync<CategoryStatic>()).ToList(),
                    };
                    //finalResult.ListSubProductStatics.ForEach(p => p.ListImagesLink = JsonConvert.DeserializeObject<List<string>>(p.ListImages));
                    uow.Commit();
                    return finalResult;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<string> ExportStaticByMonthToExcel(StaticByMonth staticByMonth)
        {
            try
            {
                string directory = Path.Combine(Directory.GetCurrentDirectory(), "export-static-by-month", $"{DateTime.Now:yyyyMMdd}");

                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }
                string filePath = Path.Combine(directory, $"ExportStaticByMonth{DateTime.Now.Month}{DateTime.Now.Year}.xlsx");

                if (staticByMonth != null)
                {
                    using (var workbook = new XLWorkbook())
                    {
                        //list customers
                        var worksheet = workbook.Worksheets.Add("ListNewCustomers");
                        var currentRow = 1;
                        worksheet.Cell(currentRow, 1).Value = "Username";
                        worksheet.Cell(currentRow, 2).Value = "Firstname";
                        worksheet.Cell(currentRow, 3).Value = "Lastname";
                        worksheet.Cell(currentRow, 4).Value = "Address";
                        worksheet.Cell(currentRow, 5).Value = "PhoneNumber";
                        foreach (var user in staticByMonth.ListNewCustomers)
                        {
                            currentRow++;
                            worksheet.Cell(currentRow, 1).Value = user.UserName;
                            worksheet.Cell(currentRow, 2).Value = user.FirstName;
                            worksheet.Cell(currentRow, 3).Value = user.LastName;
                            worksheet.Cell(currentRow, 4).Value = user.Address;
                            worksheet.Cell(currentRow, 5).Value = user.PhoneNumber;
                        }
                        worksheet.ColumnWidth = 20;
                        //list InInvoices
                        var worksheet2 = workbook.Worksheets.Add("ListNewInInvoices");
                        var currentRow2 = 1;
                        worksheet2.Cell(currentRow2, 1).Value = "Id";
                        worksheet2.Cell(currentRow2, 2).Value = "TotalMoney";
                        worksheet2.Cell(currentRow2, 3).Value = "DistributorName";
                        worksheet2.Cell(currentRow2, 4).Value = "CreatedBy";
                        foreach (var ininvoice in staticByMonth.ListNewInInvoices)
                        {
                            currentRow2++;
                            worksheet2.Cell(currentRow2, 1).Value = ininvoice.Id;
                            worksheet2.Cell(currentRow2, 2).Value = ininvoice.TotalMoney;
                            worksheet2.Cell(currentRow2, 3).Value = ininvoice.DistributorName;
                            worksheet2.Cell(currentRow2, 4).Value = ininvoice.CreatedBy;
                        }
                        worksheet2.ColumnWidth = 20;
                        //list OutInvoices
                        var worksheet3 = workbook.Worksheets.Add("ListNewOutInvoices");
                        var currentRow3 = 1;
                        worksheet3.Cell(currentRow3, 1).Value = "Id";
                        worksheet3.Cell(currentRow3, 2).Value = "TotalMoney";
                        worksheet3.Cell(currentRow3, 3).Value = "Discount";
                        worksheet3.Cell(currentRow3, 4).Value = "Status";
                        worksheet3.Cell(currentRow3, 5).Value = "ShipAt";
                        worksheet3.Cell(currentRow3, 6).Value = "ShipPhone";
                        worksheet3.Cell(currentRow3, 7).Value = "ShipName";
                        worksheet3.Cell(currentRow3, 7).Value = "ShippingCharge";
                        foreach (var outinvoice in staticByMonth.ListNewOutInvoices)
                        {
                            currentRow3++;
                            worksheet3.Cell(currentRow3, 1).Value = outinvoice.Id;
                            worksheet3.Cell(currentRow3, 2).Value = outinvoice.TotalMoney;
                            worksheet3.Cell(currentRow3, 3).Value = outinvoice.Discount;
                            worksheet3.Cell(currentRow3, 4).Value = outinvoice.Status;
                            worksheet3.Cell(currentRow3, 5).Value = outinvoice.ShipAt;
                            worksheet3.Cell(currentRow3, 6).Value = outinvoice.ShipPhone;
                            worksheet3.Cell(currentRow3, 7).Value = outinvoice.ShipName;
                            worksheet3.Cell(currentRow3, 8).Value = outinvoice.ShippingCharge;
                        }
                        worksheet3.ColumnWidth = 20;
                        //list bestProduct
                        var worksheet4 = workbook.Worksheets.Add("ListTopProducts");
                        var currentRow4 = 1;
                        worksheet4.Cell(currentRow4, 1).Value = "ProductCode";
                        worksheet4.Cell(currentRow4, 2).Value = "ProductName";
                        worksheet4.Cell(currentRow4, 3).Value = "Quantity";
                        worksheet4.Cell(currentRow4, 4).Value = "OutPrice";
                        worksheet4.Cell(currentRow4, 5).Value = "InPrice";
                        worksheet4.Cell(currentRow4, 6).Value = "QuantitySellInMonth";
                        foreach (var subProduct in staticByMonth.ListTopSubProducts)
                        {
                            currentRow4++;
                            worksheet4.Cell(currentRow4, 1).Value = subProduct.ProductCode;
                            worksheet4.Cell(currentRow4, 2).Value = subProduct.ProductName;
                            worksheet4.Cell(currentRow4, 3).Value = subProduct.Quantity;
                            worksheet4.Cell(currentRow4, 4).Value = subProduct.OutPrice;
                            worksheet4.Cell(currentRow4, 5).Value = subProduct.InPrice;
                            worksheet4.Cell(currentRow4, 6).Value = subProduct.QuantitySellInMonth;
                        }
                        worksheet4.ColumnWidth = 20;
                        //list top keyword
                        var worksheet5 = workbook.Worksheets.Add("ListTopKeywords");
                        var currentRow5 = 1;
                        worksheet5.Cell(currentRow5, 1).Value = "Keyword";
                        worksheet5.Cell(currentRow5, 2).Value = "KeywordCount";
                        foreach (var keyword in staticByMonth.ListKeywordRankings)
                        {
                            currentRow5++;
                            worksheet5.Cell(currentRow5, 1).Value = keyword.Keyword;
                            worksheet5.Cell(currentRow5, 2).Value = keyword.KeywordCount;
                        }
                        worksheet5.ColumnWidth = 20;
                        //list top category
                        var worksheet6 = workbook.Worksheets.Add("ListTopCategories");
                        var currentRow6 = 1;
                        worksheet6.Cell(currentRow6, 1).Value = "Category";
                        worksheet6.Cell(currentRow6, 2).Value = "SoldProductCount";
                        foreach (var category in staticByMonth.ListCategoryRankings)
                        {
                            currentRow6++;
                            worksheet6.Cell(currentRow6, 1).Value = category.Name;
                            worksheet6.Cell(currentRow6, 2).Value = category.BuyQuantityOfCategoryInMonth;
                        }
                        worksheet6.ColumnWidth = 20;

                        using (var stream = new MemoryStream())
                        {
                            workbook.SaveAs(stream);
                            stream.Position = 0;
                            using (var fileStream = new FileStream(filePath, FileMode.Create, FileAccess.Write))
                            {
                                stream.CopyTo(fileStream);
                                fileStream.Close();
                            }
                        }
                    }
                    return filePath;
                }
                else
                {
                    return "";
                }
            }
            catch (Exception ex)
            {
                //_elasticSearchService.UpErrorToElaticSearch(ex, new { });
                throw ex;
            }
        }
        public async Task<IEnumerable<SearchStatic>> Get2MonthKeyword()
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.GeneralRepository.Get2MonthKeyword();
                    uow.Commit();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
