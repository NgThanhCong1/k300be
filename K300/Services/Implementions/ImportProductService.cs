﻿using Microsoft.Extensions.Configuration;
using Models.RequestModels;
using Models.ResponseModels;
using Repository;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Implementions
{
    public class ImportProductService : IImportProductService
    {
        private string connectionString;

        public ImportProductService(IConfiguration configuration)
        {
            connectionString = configuration.GetConnectionString(configuration["SqlServer"]);
        }
        public async Task<int> ImportProduct(ImportProduct importProduct)
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.ImportRepository.ImportProduct(importProduct);
                    uow.Commit();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<List<InInvoiceResponse>> GetAllInInvoice()
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.ImportRepository.GetAllInInvoice();
                    List<InInvoiceResponse> outResult = new List<InInvoiceResponse>();
                    var listInInvoice = await result.ReadAsync<InInvoice>();
                    var listInInvoiceDetail = await result.ReadAsync<InInvoiceDetail>();

                    foreach (var item in listInInvoice)
                    {
                        InInvoiceResponse response = new InInvoiceResponse()
                        {
                            InInvoice = new InInvoice()
                            {
                                Id = item.Id,
                                DistributorName = item.DistributorName,
                                TotalMoney = item.TotalMoney,
                                CreatedAt = item.CreatedAt
                            },
                            ListInvoiceDetail = listInInvoiceDetail.Where(l => l.InInvoiceId == item.Id).ToList()
                        };
                        outResult.Add(response);
                    }
                    uow.Commit();
                    return outResult;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> ImportProductNew(List<ImportProductNew> listImportProduct, int distributorId, string createdBy)
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.ImportRepository.ImportProductNew(listImportProduct, distributorId, createdBy);
                    uow.Commit();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<IEnumerable<ImportDetail>> GetAllImprotDetail()
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.ImportRepository.GetAllImprotDetail();
                    uow.Commit();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<List<ProductConfig>> GetProductConfigsByProductCode(string productCode)
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.ImportRepository.GetProductConfigsByProductCode(productCode);
                    uow.Commit();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> UpdateImportDetail(string productCode, int inInvoceId, int productId, string productConfigsInInvoceDetail, string productConfigsInProduct)
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.ImportRepository.UpdateImportDetail(productCode, inInvoceId, productId, productConfigsInInvoceDetail, productConfigsInProduct);
                    uow.Commit();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<IEnumerable<InInvoice>> GetAll()
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.ImportRepository.GetAll();
                    uow.Commit();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<IEnumerable<InInvoiceDetail>> GetInInvoiceDetailByInInvoiceId(int inInvoiceId)
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.ImportRepository.GetInInvoiceDetailByInInvoiceId(inInvoiceId);
                    uow.Commit();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
