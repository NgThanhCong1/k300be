﻿using Microsoft.Extensions.Configuration;
using Models.RequestModels;
using Models.ResponseModels;
using Repository;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ultilities;

namespace Services.Implementions
{
    public class ProductService : IProductService
    {
        private string connectionString;

        public ProductService(IConfiguration configuration)
        {
            connectionString = configuration.GetConnectionString(configuration["SqlServer"]);
        }
        public async Task<IEnumerable<Product_New>> GetAll()
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.ProductRepository.GetAll();
                    uow.Commit();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> UpdateProduct(AddProduct addProduct)
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.ProductRepository.UpdateProduct(addProduct);
                    uow.Commit();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<IEnumerable<Product>> SearchProduct(Product product)
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.ProductRepository.SearchProduct(product);
                    uow.Commit();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<IEnumerable<Product>> SearchProductByCate(Category cate)
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.ProductRepository.SearchProductByCate(cate);
                    uow.Commit();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> AddProductCate(AddProductCate cate)
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.ProductRepository.AddProductCate(cate);
                    uow.Commit();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> DeleteProduct(Product_New product)
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.ProductRepository.DeleteProduct(product);
                    uow.Commit();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<IEnumerable<int>> GetListCateByProduct(int productId)
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.ProductRepository.GetListCateByProduct(productId);
                    uow.Commit();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<IEnumerable<ProductDetail>> GetListProductsByCate(string cateName)
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.ProductRepository.GetListProductsByCate(cateName);
                    uow.Commit();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<GetProductById> GetProductsById(int id)
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.ProductRepository.GetProductsById(id);
                    var listProductDetails = await result.ReadAsync<ProductDetail>();
                    var listRelatedProducts = await result.ReadAsync<CombineProduct>();
                    uow.Commit();
                    return new GetProductById()
                    {
                        ListProductDetails = listProductDetails.ToList(),
                        ListCombineProducts = listRelatedProducts.ToList()
                    };
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<GetProductBySlug> GetProductsBySlug(string slug, string customerId)
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.ProductRepository.GetProductsBySlug(slug, customerId);
                    var listProductDetails = await result.ReadAsync<ProductDetail>();
                    var listRelatedProducts = await result.ReadAsync<ProductDetail>();
                    var ratingStatic = await result.ReadAsync<RatingStatic>();
                    uow.Commit();
                    return new GetProductBySlug()
                    {
                        ListProductDetails = listProductDetails.ToList(),
                        ListCombineProducts = listRelatedProducts.ToList(),
                        RatingStatic = ratingStatic.FirstOrDefault()
                    };
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<GetDataForUpdateProduct> GetProductsByIdAdmin(int id)
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.ProductRepository.GetProductsByIdAdmin(id);
                    var productNew = await result.ReadAsync<Product_New>();
                    var listSubProduct = await result.ReadAsync<SubProduct>();

                    if(listSubProduct != null || listSubProduct.Any())
                    {
                        listSubProduct.ToList().ForEach(s => s.ListImagesLink = SolutionsCenter.ConvertImagesTextToListImages(s.ListImages));
                    }

                    uow.Commit();
                    return new GetDataForUpdateProduct()
                    {
                        ListSubProducts = listSubProduct.ToList(),
                        Product_New = productNew.FirstOrDefault()
                    };
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<List<ProductConfig>> GetProductConfigByProductId(int id)
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.ProductRepository.GetProductConfigByProductId(id);
                    uow.Commit();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> AddProduct(AddProduct addProduct)
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.ProductRepository.AddProduct(addProduct);
                    uow.Commit();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> AddProductGGSheet(List<Product_New> listProducts)
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.ProductRepository.AddProductGGSheet(listProducts);
                    uow.Commit();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> UpdateProductConfig(string listProductConfigs, int productId, int quantityInStock, string updatedBy)
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.ProductRepository.UpdateProductConfig(listProductConfigs, productId, quantityInStock, updatedBy);
                    uow.Commit();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> AddSubProduct(SubProduct product)
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.ProductRepository.AddSubProduct(product);
                    uow.Commit();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<IEnumerable<SubProduct>> GetAllSubProduct()
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.ProductRepository.GetAllSubProduct();
                    uow.Commit();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<SubProduct> GetSubProductByProductCode(string productCode)
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.ProductRepository.GetSubProductByProductCode(productCode);
                    uow.Commit();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> UpdateSubProduct(SubProduct subProduct)
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.ProductRepository.UpdateSubProduct(subProduct);
                    uow.Commit();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> DeleteSubProduct(string productCode, string updatedBy)
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.ProductRepository.DeleteSubProduct(productCode, updatedBy);
                    uow.Commit();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<IEnumerable<CombineProduct>> GetAllCombineProducts()
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.ProductRepository.GetAllCombineProducts();
                    uow.Commit();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<IEnumerable<ProductDetail>> SearchProductsByNameOrCategory(string categoryName, string productName)
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.ProductRepository.SearchProductsByNameOrCategory(categoryName, productName);
                    uow.Commit();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> AddRating(Rating rating)
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.ProductRepository.AddRating(rating);
                    uow.Commit();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<IEnumerable<ProductDetail>> GetTop4NewInProduct()
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(connectionString))
                {
                    var result = await uow.ProductRepository.GetTop4NewInProduct();
                    uow.Commit();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
