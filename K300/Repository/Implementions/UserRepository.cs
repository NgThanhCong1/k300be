﻿using Models;
using Models.ResponseModels;
using Newtonsoft.Json;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Implementions
{
    public class UserRepository : BaseRepository, IUserRepository
    {
        public UserRepository(IDbTransaction transaction) : base(transaction) { }

        public async Task<IEnumerable<RolePermission>> GetAllPermissionByRole(string role)
        {
            try
            {
                var result = await QueryAsync<RolePermission>("[sp_GetAllPermissionByRole]", new
                {
                    @role = role
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> DeleteRolePermission(RolePermission rolePermission)
        {
            try
            {
                var result = await ExecuteAsync("[sp_DeletePermission]", new
                {
                    @role = rolePermission.Role,
                    @object = rolePermission.Object,
                    @permission = rolePermission.Permission,
                    @updatedBy = rolePermission.UpdatedBy
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> AddRolePermission(RolePermission rolePermission)
        {
            try
            {
                var result = await ExecuteAsync("[sp_AddPermission]", new
                {
                    @role = rolePermission.Role,
                    @object = rolePermission.Object,
                    @permission = rolePermission.Permission,
                    @createdBy = rolePermission.CreatedBy
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> AddListRolePermission(List<RolePermission> listRolePermission)
        {
            try
            {
                var json = JsonConvert.SerializeObject(listRolePermission);
                var result = await ExecuteAsync("[sp_AddListRolePermission]", new
                {
                    @JsonData = json
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> UpdatePermissionsOfRole(List<RolePermission> listRolePermission)
        {
            try
            {
                var json = JsonConvert.SerializeObject(listRolePermission);
                var result = await ExecuteAsync("[sp_UpdatePermissionsOfRole]", new
                {
                    @roleName = listRolePermission[0].Role,
                    @JsonData = json
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
