﻿using Models.ResponseModels;
using Newtonsoft.Json;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Dapper.SqlMapper;

namespace Repository.Implementions
{
    public class CategoryRepository : BaseRepository, ICategoryRepository
    {
        public CategoryRepository(IDbTransaction transaction) : base(transaction) { }
        public async Task<IEnumerable<Category>> GetAllCate()
        {
            try
            {
                var result = await QueryAsync<Category>("[sp_GetAllSubCategories]", new
                {
                    
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<GridReader> GetAllCategoryByParentCategory()
        {
            try
            {
                var result = await QueryMultipleAsync("[sp_GetAllCategoriesByParentCategory]", new
                {
                    
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> UpdateCate(Category cate)
        {
            try
            {
                var result = await ExecuteAsync("[sp_UpdateCate]", new
                {
                    @id = cate.Id,
                    @name = cate.Name,
                    @parentCategory = cate.ParentCategory,
                    @images = cate.Images,
                    @updatedBy = cate.UpdatedBy
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> AddCate(Category cate)
        {
            try
            {
                var result = await ExecuteAsync("[sp_AddCate]", new
                {
                    @name = cate.Name,
                    @images = cate.Images,
                    @parentCategory = cate.ParentCategory,
                    @createdBy = cate.CreatedBy
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> AddListCate(List<Category> listCate)
        {
            try
            {
                var json = JsonConvert.SerializeObject(listCate);
                var result = await ExecuteAsync("[sp_AddListCate]", new
                {
                    @JsonData = json
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<IEnumerable<Category>> SearchCate(Category cate)
        {
            try
            {
                var result = await QueryAsync<Category>("[sp_SearchCate]", new
                {
                    @name = cate.Name,
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> DeleteCate(Category cate)
        {
            try
            {
                var result = await ExecuteAsync("[sp_DeleteCate]", new
                {
                    @id = cate.Id,
                    @updatedBy = cate.UpdatedBy
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<List<Category>> GetTop4Cates()
        {
            try
            {
                var result = await QueryAsync<Category>("[sp_GetTop4Cates]", new
                {

                });
                return result.AsList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<Category> GetCategoryById(int id)
        {
            try
            {
                var result = await QueryAsync<Category>("[sp_GetCategoryById]", new
                {
                    @id = id
                });
                return result.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
