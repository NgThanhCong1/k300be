﻿using Models.ResponseModels;
using Newtonsoft.Json;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Implementions
{
    public class DiscountRepository : BaseRepository, IDiscountRepository
    {
        public DiscountRepository(IDbTransaction transaction) : base(transaction) { }
        public async Task<IEnumerable<Discount>> GetAll()
        {
            try
            {
                var result = await QueryAsync<Discount>("[sp_GetAll]", new
                {
                    @className = "Discount"
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> UpdateDiscount(Discount discount)
        {
            try
            {
                var result = await ExecuteAsync("[sp_UpdateDiscount]", new
                {
                    @id = discount.Id,
                    @discountCode = discount.DiscountCode,
                    @price = discount.Price,
                    @quantity = discount.Quantity,
                    @updatedBy = discount.UpdatedBy
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> AddDiscount(Discount discount)
        {
            try
            {
                var result = await ExecuteAsync("[sp_AddDiscount]", new
                {
                    @discountCode = discount.DiscountCode,
                    @price = discount.Price,
                    @quantity = discount.Quantity,
                    @createdBy = discount.CreatedBy
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> AddListDiscount(List<Discount> listDiscount)
        {
            try
            {
                var json = JsonConvert.SerializeObject(listDiscount);
                var result = await ExecuteAsync("[sp_AddListDiscount]", new
                {
                    @JsonData = json
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<IEnumerable<Discount>> SearchBrand(Discount discount)
        {
            try
            {
                var result = await QueryAsync<Discount>("[sp_SearchDiscount]", new
                {
                    //@name = Discount.Name,
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> DeleteDiscount(Discount discount)
        {
            try
            {
                var result = await ExecuteAsync("[sp_DeleteDiscount]", new
                {
                    @id = discount.Id,
                    @updatedBy = discount.UpdatedBy
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<Discount> SearchDiscount(string discountCode)
        {
            try
            {
                var result = await QueryAsync<Discount>("[sp_SearchDiscount]", new
                {
                    @discountCode = discountCode
                });
                return result.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<Discount> GetDiscountById(int id)
        {
            try
            {
                var result = await QueryAsync<Discount>("[sp_GetDiscountById]", new
                {
                    @id = id
                });
                return result.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
