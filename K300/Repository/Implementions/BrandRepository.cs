﻿using Models;
using Newtonsoft.Json;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Implementions
{
    public class BrandRepository : BaseRepository, IBrandRepository
    {
        public BrandRepository(IDbTransaction transaction) : base(transaction) { }

        public async Task<IEnumerable<Brand>> GetAll()
        {
            try
            {
                var result = await QueryAsync<Brand>("[sp_GetAll]", new
                {
                    @className = "Brand"
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> UpdateBrand(Brand brand)
        {
            try
            {
                var result = await ExecuteAsync("[sp_UpdateBrand]", new
                {
                    @id = brand.Id,
                    @description = brand.Description,
                    @name = brand.Name,
                    @updatedBy = brand.UpdatedBy
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> AddBrand(Brand brand)
        {
            try
            {
                var result = await ExecuteAsync("[sp_AddBrand]", new
                {
                    @description = brand.Description,
                    @name = brand.Name,
                    @createdBy = brand.CreatedBy
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> AddListBrand(List<Brand> listBrand)
        {
            try
            {
                var json = JsonConvert.SerializeObject(listBrand);
                var result = await ExecuteAsync("[sp_AddListBrand]", new
                {
                    @JsonData = json
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<IEnumerable<Brand>> SearchBrand(Brand brand)
        {
            try
            {
                var result = await QueryAsync<Brand>("[sp_SearchBrand]", new
                {
                    @name = brand.Name,
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> DeleteBrand(Brand brand)
        {
            try
            {
                var result = await ExecuteAsync("[sp_DeleteBrand]", new
                {
                    @id = brand.Id,
                    @updatedBy = brand.UpdatedBy
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<Brand> GetBrandById(int id)
        {
            try
            {
                var result = await QueryAsync<Brand>("[sp_GetBrandById]", new
                {
                    @id = id
                });
                return result.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
