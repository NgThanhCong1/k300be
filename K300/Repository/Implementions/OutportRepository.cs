﻿using Models.RequestModels;
using Models.ResponseModels;
using Newtonsoft.Json;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Dapper.SqlMapper;

namespace Repository.Implementions
{
    public class OutportRepository : BaseRepository, IOutportRepository
    {
        public OutportRepository(IDbTransaction transaction) : base(transaction) { }
        public async Task<IEnumerable<OutInvoiceRequest>> GetAll()
        {
            try
            {
                var result = await QueryAsync<OutInvoiceRequest>("[sp_GetAll]", new
                {
                    @className = "Out_Invoice"
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> AddOutInvoice(OutInvoiceRequest request)
        {
            try
            {
                var result = await QueryAsync<int>("[sp_AddOutInvoice]", new
                {
                    @totalMoney = request.TotalMoney,
                    @discountId = request.DiscountId,
                    @customerId = request.CustomerId,
                    @shipAt = request.ShipAt,
                    @shipPhone= request.ShipPhone,
                    @shipName = request.ShipName,
                    @shippingCharge = request.ShippingCharge,
                    @paymentMethod = request.PaymentMethod,
                    @createdBy = request.CreatedBy
                });
                return result.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<string> AddOutInvoiceDetail(int outInvoiceId, List<OutportProduct> listProducts, string createdBy)
        {
            try
            {
                var result = await QueryAsync<string>("[sp_AddOutInvoicesDetails]", new
                {
                    @outInvoiceId = outInvoiceId,
                    @jsonData = JsonConvert.SerializeObject(listProducts),
                    @createdBy = createdBy
                });
                return result.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> UpdateOutInvoice(OutInvoiceRequest model, string type)
        {
            try
            {
                var result = await ExecuteAsync("[sp_UpdateOutInvoice]", new
                {
                    @outInvoiceId = model.Id,
                    @staffId = model.StaffId,
                    @status = type == "edit" ? model.Status + 1 : 0,
                    @updateBy = model.UpdatedBy,
                    @historyChanges = model.HistoryChanges
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<string> GetHistoryChangesByOutInvoiceId(int id)
        {
            try
            {
                var result = await QueryAsync<string>("[sp_GetHistoryChangesByOutInvoiceId]", new
                {
                    @id = id
                });
                return result.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<GridReader> GetOutInvoiceDetailByOutInvoiceId(int id)
        {
            try
            {
                var result = await QueryMultipleAsync("[sp_GetOutInvoiceDetailByOutInvoiceId]", new
                {
                    @outInvoiceId = id
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<OutInvoiceRequest> GetOutInvoicById(int id)
        {
            try
            {
                var result = await QueryAsync<OutInvoiceRequest>("[sp_GetOutInvoiceById]", new
                {
                    @id = id
                });
                return result.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> UpdateOutInvoiceData(OutInvoiceRequest model)
        {
            try
            {
                var result = await ExecuteAsync("[sp_UpdateOutInvoiceData]", new
                {
                    @id = model.Id,
                    @totalMoney = model.TotalMoney,
                    @shipName = model.ShipName,
                    @shipPhone = model.ShipPhone,
                    @shipAt = model.ShipAt,
                    @historyChanges = model.HistoryChanges,
                    @updatedBy = model.UpdatedBy,
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<IEnumerable<HistoryOrders>> GetHistoryOrders(string userId)
        {
            try
            {
                var result = await QueryAsync<HistoryOrders>("[sp_GetHistoryOrdersByUser]", new
                {
                    @userId = userId
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<GridReader> GetOutInvoiceByUser(string userId)
        {
            try
            {
                var result = await QueryMultipleAsync("[sp_GetOutInvoiceByUser]", new
                {
                    @userId = userId
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> AddSearchStatic(SearchStatic searchStatic)
        {
            try
            {
                var result = await ExecuteAsync("[sp_AddKeyword]", new
                {
                    @customerId = searchStatic.CustomerId,
                    @keyword = searchStatic.Keyword
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<IEnumerable<SearchStatic>> GetAllSearchStatic()
        {
            try
            {
                var result = await QueryAsync<SearchStatic>("[sp_GetAllKeyword]", new
                {
                    
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<IEnumerable<SearchStatic>> GetTop50Keyword()
        {
            try
            {
                var result = await QueryAsync<SearchStatic>("[sp_GetTop50Keyword]", new
                {
                    
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<IEnumerable<SearchStatic>> GetKeywordsByCustomer(string customerId)
        {
            try
            {
                var result = await QueryAsync<SearchStatic>("[sp_GetKeywordsByCustomer]", new
                {
                    @customerId = customerId
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
