﻿using Models.ResponseModels;
using Newtonsoft.Json;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Implementions
{
    public class DistributorRepository : BaseRepository, IDistributorRepository
    {
        public DistributorRepository(IDbTransaction transaction) : base(transaction) { }

        public async Task<IEnumerable<Distributor>> GetAll()
        {
            try
            {
                var result = await QueryAsync<Distributor>("[sp_GetAll]", new
                {
                    @className = "Distributor"
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> UpdateDistributor(Distributor distributor)
        {
            try
            {
                var result = await ExecuteAsync("[sp_UpdateDistributor]", new
                {
                    @id = distributor.Id,
                    @name = distributor.Name,
                    @phone = distributor.Phone,
                    @address = distributor.Address,
                    @updatedBy = distributor.UpdatedBy
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> AddDistributor(Distributor distributor)
        {
            try
            {
                var result = await ExecuteAsync("[sp_AddDistributorErr]", new
                {
                    @name = distributor.Name,
                    @phone = distributor.Phone,
                    @address = distributor.Address,
                    @createdBy = distributor.CreatedBy
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> AddListDistributor(List<Distributor> listDistributor)
        {
            try
            {
                var json = JsonConvert.SerializeObject(listDistributor);
                var result = await ExecuteAsync("[sp_AddListDistributor]", new
                {
                    @JsonData = json
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<IEnumerable<Distributor>> SearchDistributor(Distributor distributor)
        {
            try
            {
                var result = await QueryAsync<Distributor>("[sp_SearchDistributor]", new
                {
                    @name = distributor.Name,
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> DeleteDistributor(Distributor distributor)
        {
            try
            {
                var result = await ExecuteAsync("[sp_DeleteDistributor]", new
                {
                    @updatedBy = distributor.UpdatedBy,
                    @id = distributor.Id
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<Distributor> GetDistributorById(int id)
        {
            try
            {
                var result = await QueryAsync<Distributor>("[sp_GetDistributorById]", new
                {
                    @id = id
                });
                return result.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
