﻿using Models;
using Models.ResponseModels;
using Newtonsoft.Json;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Implementions
{
    public class NotifyRepository : BaseRepository, INotifyRepository
    {
        public NotifyRepository(IDbTransaction transaction) : base(transaction) { }

        public async Task<IEnumerable<Notify>> GetNotifyByCustomer(string customerId)
        {
            try
            {
                var result = await QueryAsync<Notify>("[sp_GetNotifyByCustomerId]", new
                {
                    @customerId = customerId
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> UpdateNotify(Notify notify)
        {
            try
            {
                var result = await ExecuteAsync("[sp_ModifyNotify]", new
                {
                    @id = notify.Id,
                    @description = notify.IsWatched,
                    @updatedBy = notify.UpdatedBy
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> AddNotify(Notify notify)
        {
            try
            {
                var result = await ExecuteAsync("[sp_AddNotify]", new
                {
                    @customerId = notify.CustomerId,
                    @image = notify.Image,
                    @title = notify.Title,
                    @content = notify.Content,
                    @createdBy = notify.CreatedBy
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
