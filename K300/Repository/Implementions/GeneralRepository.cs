﻿using Models;
using Models.ResponseModels;
using Newtonsoft.Json;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Dapper.SqlMapper;

namespace Repository.Implementions
{
    public class GeneralRepository : BaseRepository, IGeneralRepository
    {
        public GeneralRepository(IDbTransaction transaction) : base(transaction) { }

        public async Task<GridReader> GetDataForDashBoard()
        {
            try
            {
                var result = await QueryMultipleAsync("[sp_GetDataForDashBoard]", new
                {
                    
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<GridReader> GetDataStaticByMonth()
        {
            try
            {
                var result = await QueryMultipleAsync("[sp_StaticByMonth]", new
                {

                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<IEnumerable<SearchStatic>> Get2MonthKeyword()
        {
            try
            {
                var result = await QueryAsync<SearchStatic>("[sp_GetAllKeyword]", new
                {

                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
