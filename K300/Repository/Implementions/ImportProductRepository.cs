﻿using Models.RequestModels;
using Models.ResponseModels;
using Newtonsoft.Json;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Dapper.SqlMapper;

namespace Repository.Implementions
{
    public class ImportProductRepository : BaseRepository, IImportProductRepository
    {
        public ImportProductRepository(IDbTransaction transaction) : base(transaction) { }

        public async Task<int> ImportProduct(ImportProduct importProduct)
        {
            try
            {
                var jsonProduct = JsonConvert.SerializeObject(importProduct.ListInProduct);
                var result = await ExecuteAsync("[sp_ImportProduct]", new
                {
                    @distributorId = importProduct.DistributorId,
                    @createdBy = importProduct.CreatedBy,
                    @jsonProduct = jsonProduct
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<GridReader> GetAllInInvoice()
        {
            try
            {
                var result = await QueryMultipleAsync("[sp_GetAllInvoice]", new
                {

                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<IEnumerable<InInvoice>> GetAll()
        {
            try
            {
                var result = await QueryAsync<InInvoice>("[sp_GetAll]", new
                {
                    @className = "In_Invoice"
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> ImportProductNew(List<ImportProductNew> listImportProduct, int distributorId, string createdBy)
        {
            try
            {
                var jsonProduct = JsonConvert.SerializeObject(listImportProduct);
                var result = await ExecuteAsync("[sp_ImportProduct]", new
                {
                    @distributorId = distributorId,
                    @jsonListImportProduct = jsonProduct,
                    @createdBy = createdBy
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<IEnumerable<ImportDetail>> GetAllImprotDetail()
        {
            try
            {
                var result = await QueryAsync<ImportDetail>("[sp_GetAllImportDetail]", new
                {
                   
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<List<ProductConfig>> GetProductConfigsByProductCode(string productCode)
        {
            try
            {
                var result = await QueryAsync<string>("[sp_GetProductConfigsByProductCode]", new
                {
                    @productCode = productCode
                });
                return result.FirstOrDefault() != null ? JsonConvert.DeserializeObject<List<ProductConfig>>(result.FirstOrDefault()).ToList
                    () : null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> UpdateImportDetail(string productCode, int inInvoceId, int productId, string productConfigsInInvoceDetail, string productConfigsInProduct)
        {
            try
            {
                var result = await ExecuteAsync("[sp_UpdateImportDetail]", new
                {
                    @productCode = productCode,
                    @inInvoceId = inInvoceId,
                    @productConfigsInInvoceDetail = productConfigsInInvoceDetail,
                    @productConfigsInProduct = productConfigsInProduct,
                    @productId = productId
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<IEnumerable<InInvoiceDetail>> GetInInvoiceDetailByInInvoiceId(int inInvoiceId)
        {
            try
            {
                var result = await QueryAsync<InInvoiceDetail>("[sp_GetInInvoiceDetailsByIninvoiceId]", new
                {
                    @inInvoiceId = inInvoiceId
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
