﻿using Models.ResponseModels;
using Newtonsoft.Json;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Dapper.SqlMapper;

namespace Repository.Implementions
{
    public class StoreRepository : BaseRepository, IStoreRepository
    {
        public StoreRepository(IDbTransaction transaction) : base(transaction) { }
        public async Task<IEnumerable<Store>> GetAllStore()
        {
            try
            {
                var result = await QueryAsync<Store>("[sp_GetAllStore]", new
                {
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> UpdateStore(Store store)
        {
            try
            {
                var result = await ExecuteAsync("[sp_UpdateStore]", new
                {
                    @id = store.Id,
                    @image = store.Image,
                    @address = store.Address,
                    @phone = store.Phone,
                    @updatedBy = store.UpdatedBy
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> AddStore(Store store)
        {
            try
            {
                var result = await ExecuteAsync("[sp_AddStore]", new
                {
                    @image = store.Image,
                    @address = store.Address,
                    @phone = store.Phone,
                    @createdBy = store.CreatedBy
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> DeleteStore(Store store)
        {
            try
            {
                var result = await ExecuteAsync("[sp_DeleteStore]", new
                {
                    @id = store.Id,
                    @updatedBy = store.UpdatedBy
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<Store> GetStoreById(int id)
        {
            try
            {
                var result = await QueryAsync<Store>("[sp_GetStoreById]", new
                {
                    @id = id,
                });
                return result.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
