﻿using Models.RequestModels;
using Models.ResponseModels;
using Newtonsoft.Json;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using static Dapper.SqlMapper;

namespace Repository.Implementions
{
    public class ProductRepository : BaseRepository, IProductRepository
    {
        public ProductRepository(IDbTransaction transaction) : base(transaction) { }
        public async Task<IEnumerable<Product_New>> GetAll()
        {
            try
            {
                var result = await QueryAsync<Product_New>("[sp_GetAllProduct]", new
                {
                    
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> UpdateProduct(AddProduct addProduct)
        {
            try
            {
                var result = await ExecuteAsync("[sp_ModifyProduct]", new
                {
                    @id = addProduct.productNew.Id,
                    @name = addProduct.productNew.Name,
                    @price = addProduct.productNew.Price,
                    @isPopular = addProduct.productNew.IsPopular,
                    @brandId = addProduct.productNew.BrandId,
                    @description = addProduct.productNew.Description,
                    @material = addProduct.productNew.Material,
                    @updatedBy = addProduct.productNew.UpdatedBy,
                    @jsonListCategories = JsonConvert.SerializeObject(addProduct.listProductCategoryNews),
                    @jsonListSubProduct = JsonConvert.SerializeObject(addProduct.ListSubProducts)
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<IEnumerable<Product>> SearchProduct(Product product)
        {
            try
            {
                var result = await QueryAsync<Product>("[sp_SearchProduct]", new
                {
                    @name = product.Name,
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<IEnumerable<Product>> SearchProductByCate(Category cate)
        {
            try
            {
                var result = await QueryAsync<Product>("[sp_SearchProductByCate]", new
                {
                    @cateName = cate.Name,
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> AddProductCate(AddProductCate cate)
        {
            try
            {
                var json = JsonConvert.SerializeObject(cate.ListCategorie);
                var result = await ExecuteAsync("[sp_AddProductCate]", new
                {
                    @productId = cate.ProductId,
                    @jsonCates = json,
                    @createdBy = cate.CreatedBy
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> DeleteProduct(Product_New product)
        {
            try
            {
                var result = await ExecuteAsync("[sp_DeleteProduct]", new
                {
                    @id = product.Id,
                    @updatedBy = product.UpdatedBy
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<IEnumerable<int>> GetListCateByProduct(int productId)
        {
            try
            {
                var result = await QueryAsync<int>("[sp_GetCatesByProduct]", new
                {
                    @productId = productId
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<IEnumerable<ProductDetail>> GetListProductsByCate(string cateName)
        {
            try
            {
                var result = await QueryAsync<ProductDetail>("[sp_GetProductsByCate]", new
                {
                    @cateName = cateName
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<GridReader> GetProductsById(int id)
        {
            try
            {
                var result = await QueryMultipleAsync("[sp_GetProductById]", new
                {
                    @id = id
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<GridReader> GetProductsBySlug(string slug, string customerId)
        {
            try
            {
                var result = await QueryMultipleAsync("[sp_GetProductBySlug]", new
                {
                    @slug = slug,
                    @customerId = customerId
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<GridReader> GetProductsByIdAdmin(int id)
        {
            try
            {
                var result = await QueryMultipleAsync("[sp_GetProductByIdAdmin]", new
                {
                    @id = id
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<List<ProductConfig>> GetProductConfigByProductId(int id)
        {
            try
            {
                var result = await QueryAsync<string>("[sp_GetProductConfigByProductId]", new
                {
                    @id = id
                });
                return result.FirstOrDefault() != null ? JsonConvert.DeserializeObject<List<ProductConfig>>(result.FirstOrDefault()).ToList
                    (): null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> UpdateProductConfig(string listProductConfigs, int productId, int quantityInStock, string updatedBy)
        {
            try
            {
                var result = await ExecuteAsync("[sp_UpdateProductConfigs]", new
                {
                    @productId = productId,
                    @productConfigs = listProductConfigs,
                    @quantityInStock = quantityInStock,
                    @updatedBy = updatedBy
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> AddProduct(AddProduct addProduct)
        {
            try
            {

                var result = await QueryAsync<int>("[sp_AddProduct]", new
                {
                    @name = addProduct.productNew.Name,
                    @slug = addProduct.productNew.Slug,
                    @price = addProduct.productNew.Price,
                    @brandId = addProduct.productNew.BrandId,
                    @description = addProduct.productNew.Description,
                    @material = addProduct.productNew.Material,
                    @createdBy = addProduct.productNew.CreatedBy,
                    @jsonListCate = JsonConvert.SerializeObject(addProduct.listProductCategoryNews)
                });
                return result.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> AddProductGGSheet(List<Product_New> listProducts)
        {
            try
            {
                var jsonListProduct = JsonConvert.SerializeObject(listProducts);
                var result = await ExecuteAsync("[sp_AddListProduct]", new
                {
                    @jsonListProduct = jsonListProduct
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> AddSubProduct(SubProduct product)
        {
            try
            {
                var result = await ExecuteAsync("[sp_AddSubProduct]", new
                {
                    @productCode = product.ProductCode,
                    @productId = product.ProductId,
                    @color = product.Color,
                    @size = product.Size,
                    @createdBy = product.CreatedBy,
                    @jsonListImages = JsonConvert.SerializeObject(product.ListImages)
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<IEnumerable<SubProduct>> GetAllSubProduct()
        {
            try
            {
                var result = await QueryAsync<SubProduct>("[sp_GetAllSubProducts]", new
                {
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<SubProduct> GetSubProductByProductCode(string productCode)
        {
            try
            {
                var result = await QueryAsync<SubProduct>("[sp_GetSubProductByProductCode]", new
                {
                    @productCode = productCode
                });
                return result.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> UpdateSubProduct(SubProduct subProduct)
        {
            try
            {
                var result = await ExecuteAsync("[sp_UpdateSubProduct]", new
                {
                    @productCode = subProduct.ProductCode,
                    @color = subProduct.Color,
                    @size = subProduct.Size,
                    @updatedBy = subProduct.UpdatedBy,
                    @jsonListImages = subProduct.ListImagesLink != null && subProduct.ListImagesLink.Any() ? JsonConvert.SerializeObject(subProduct.ListImagesLink) : null
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> DeleteSubProduct(string productCode, string updatedBy)
        {
            try
            {
                var result = await ExecuteAsync("[sp_DeleteSubProduct]", new
                {
                    @productCode= productCode,
                    @updatedBy = updatedBy
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<IEnumerable<CombineProduct>> GetAllCombineProducts()
        {
            try
            {
                var result = await QueryAsync<CombineProduct>("[sp_GetCombineProducts]", new
                {
                    
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<IEnumerable<ProductDetail>> SearchProductsByNameOrCategory(string categoryName, string productName)
        {
            try
            {
                var result = await QueryAsync<ProductDetail>("[sp_GetProductByNameOrCategories]", new
                {
                    @productName = productName,
                    @categoryName = categoryName
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<IEnumerable<ProductDetail>> GetTop4NewInProduct()
        {
            try
            {
                var result = await QueryAsync<ProductDetail>("[sp_GetNewInProducts]", new
                {
                    
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> AddRating(Rating rating)
        {
            try
            {
                var result = await ExecuteAsync("[sp_AddRating]", new
                {
                    @productId = rating.ProductId,
                    @rank = rating.Rank,
                    @customerId = rating.CustomerId
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
