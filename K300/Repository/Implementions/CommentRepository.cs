﻿using Models;
using Models.ResponseModels;
using Newtonsoft.Json;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Implementions
{
    public class CommentRepository : BaseRepository, ICommentRepository
    {
        public CommentRepository(IDbTransaction transaction) : base(transaction) { }

        public async Task<IEnumerable<Comment>> GetAll()
        {
            try
            {
                var result = await QueryAsync<Comment>("[sp_GetAllComment]", new
                {
                    
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<IEnumerable<Comment>> GetCommentByProductId(int productId)
        {
            try
            {
                var result = await QueryAsync<Comment>("[sp_GetCommentByProductId]", new
                {
                    @productId = productId
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> UpdateComment(Comment comment)
        {
            try
            {
                var result = await ExecuteAsync("[sp_UpdateComment]", new
                {
                    @id = comment.Id,
                    @content = comment.Content,
                    @updatedBy = comment.UpdatedBy
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> AddComment(Comment comment)
        {
            try
            {
                var result = await ExecuteAsync("[sp_AddComment]", new
                {
                    @customerId = comment.CustomerId,
                    @productId = comment.ProductId,
                    @content = comment.Content,
                    @createdBy = comment.CreatedBy
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> DeleteComment(Comment comment)
        {
            try
            {
                var result = await ExecuteAsync("[sp_DeleteComment]", new
                {
                    @id = comment.Id,
                    @deletedBy = comment.UpdatedBy
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
