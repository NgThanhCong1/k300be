﻿using Models.RequestModels;
using Models.ResponseModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using static Dapper.SqlMapper;

namespace Repository.Interfaces
{
    public interface IOutportRepository
    {
        Task<IEnumerable<OutInvoiceRequest>> GetAll();
        Task<int> AddOutInvoice(OutInvoiceRequest request);
        Task<string> AddOutInvoiceDetail(int outInvoiceId, List<OutportProduct> listProducts, string createdBy);
        Task<int> UpdateOutInvoice(OutInvoiceRequest model, string type);
        Task<string> GetHistoryChangesByOutInvoiceId(int id);
        Task<GridReader> GetOutInvoiceDetailByOutInvoiceId(int id);
        Task<OutInvoiceRequest> GetOutInvoicById(int id);
        Task<int> UpdateOutInvoiceData(OutInvoiceRequest model);
        Task<IEnumerable<HistoryOrders>> GetHistoryOrders(string userId);
        Task<GridReader> GetOutInvoiceByUser(string userId);
        Task<int> AddSearchStatic(SearchStatic searchStatic);
        Task<IEnumerable<SearchStatic>> GetAllSearchStatic();
        Task<IEnumerable<SearchStatic>> GetTop50Keyword();
        Task<IEnumerable<SearchStatic>> GetKeywordsByCustomer(string customerId);

    }
}
