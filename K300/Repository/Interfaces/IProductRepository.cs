﻿using Models.RequestModels;
using Models.ResponseModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using static Dapper.SqlMapper;

namespace Repository.Interfaces
{
    public interface IProductRepository
    {
        Task<IEnumerable<Product_New>> GetAll();
        Task<int> UpdateProduct(AddProduct addProduct);
        Task<IEnumerable<Product>> SearchProduct(Product product);
        Task<IEnumerable<Product>> SearchProductByCate(Category cate);
        Task<int> AddProductCate(AddProductCate cate);
        Task<int> DeleteProduct(Product_New product);
        Task<IEnumerable<int>> GetListCateByProduct(int productId);
        Task<IEnumerable<ProductDetail>> GetListProductsByCate(string cateName);
        Task<GridReader> GetProductsById(int id);
        Task<GridReader> GetProductsBySlug(string slug, string customerId);
        Task<GridReader> GetProductsByIdAdmin(int id);
        Task<List<ProductConfig>> GetProductConfigByProductId(int id);
        Task<int> AddProduct(AddProduct addProduct);
        Task<int> AddProductGGSheet(List<Product_New> listProducts);
        Task<int> UpdateProductConfig(string listProductConfigs, int productId, int quantityInStock, string updatedBy);
        Task<int> AddSubProduct(SubProduct product);
        Task<IEnumerable<SubProduct>> GetAllSubProduct();
        Task<SubProduct> GetSubProductByProductCode(string productCode);
        Task<int> UpdateSubProduct(SubProduct subProduct);
        Task<int> DeleteSubProduct(string productCode, string updatedBy);
        Task<IEnumerable<CombineProduct>> GetAllCombineProducts();
        Task<IEnumerable<ProductDetail>> SearchProductsByNameOrCategory(string categoryName, string productName);
        Task<int> AddRating(Rating rating);
        Task<IEnumerable<ProductDetail>> GetTop4NewInProduct();

    }
}
