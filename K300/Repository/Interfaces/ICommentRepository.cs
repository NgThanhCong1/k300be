﻿using Models;
using Models.ResponseModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Interfaces
{
    public interface ICommentRepository
    {
        Task<IEnumerable<Comment>> GetAll();
        Task<IEnumerable<Comment>> GetCommentByProductId(int productId);
        Task<int> UpdateComment(Comment comment);
        Task<int> AddComment(Comment comment);
        Task<int> DeleteComment(Comment comment);
    }
}
