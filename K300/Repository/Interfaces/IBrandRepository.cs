﻿using Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Interfaces
{
    public interface IBrandRepository
    {
        Task<IEnumerable<Brand>> GetAll();
        Task<int> UpdateBrand(Brand brand);
        Task<int> AddBrand(Brand brand);
        Task<int> AddListBrand(List<Brand> listBrand);
        Task<IEnumerable<Brand>> SearchBrand(Brand brand);
        Task<Brand> GetBrandById(int id);
        Task<int> DeleteBrand(Brand brand);
    }
}
