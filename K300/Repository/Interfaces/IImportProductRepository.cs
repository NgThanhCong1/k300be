﻿using Models.RequestModels;
using Models.ResponseModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using static Dapper.SqlMapper;

namespace Repository.Interfaces
{
    public interface IImportProductRepository
    {
        Task<int> ImportProduct(ImportProduct importProduct);
        Task<GridReader> GetAllInInvoice();
        Task<IEnumerable<InInvoice>> GetAll();

        Task<int> ImportProductNew(List<ImportProductNew> listImportProduct, int distributorId, string createdBy);
        Task<IEnumerable<ImportDetail>> GetAllImprotDetail();
        Task<List<ProductConfig>> GetProductConfigsByProductCode(string productCode);
        Task<int> UpdateImportDetail(string productCode, int inInvoceId, int productId, string productConfigsInInvoceDetail, string productConfigsInProduct);
        Task<IEnumerable<InInvoiceDetail>> GetInInvoiceDetailByInInvoiceId(int inInvoiceId);

    }
}
