﻿using Models;
using Models.ResponseModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Interfaces
{
    public interface INotifyRepository
    {
        Task<IEnumerable<Notify>> GetNotifyByCustomer(string customerId);
        Task<int> UpdateNotify(Notify notify);
        Task<int> AddNotify(Notify notify);
    }
}
