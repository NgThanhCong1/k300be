﻿using Models;
using Models.ResponseModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using static Dapper.SqlMapper;

namespace Repository.Interfaces
{
    public interface IGeneralRepository
    {
        Task<GridReader> GetDataForDashBoard();
        Task<GridReader> GetDataStaticByMonth();
        Task<IEnumerable<SearchStatic>> Get2MonthKeyword();
    }
}
