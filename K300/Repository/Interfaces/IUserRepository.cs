﻿using Models;
using Models.ResponseModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Interfaces
{
    public interface IUserRepository
    {
        Task<IEnumerable<RolePermission>> GetAllPermissionByRole(string role);
        Task<int> DeleteRolePermission(RolePermission rolePermission);
        Task<int> AddListRolePermission(List<RolePermission> listRolePermission);
        Task<int> AddRolePermission(RolePermission rolePermission);
        Task<int> UpdatePermissionsOfRole(List<RolePermission> listRolePermission);

    }
}
