﻿using Models.ResponseModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Interfaces
{
    public interface IDiscountRepository
    {
        Task<IEnumerable<Discount>> GetAll();
        Task<int> UpdateDiscount(Discount discount);
        Task<int> AddDiscount(Discount discount);
        Task<int> AddListDiscount(List<Discount> listDiscount);
        Task<int> DeleteDiscount(Discount discount);
        Task<Discount> GetDiscountById(int id);
        Task<Discount> SearchDiscount(string discountCode);
    }
}
