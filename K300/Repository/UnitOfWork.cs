﻿using Microsoft.Extensions.Configuration;
using Repository.Implementions;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        private IDbTransaction _dbTransaction;
        private IDbConnection _dbConnection;
        private bool _disposed;
        private IBrandRepository _brandRepository;
        private IDistributorRepository _distributorRepository;
        private IImportProductRepository _importRepository;
        private ICategoryRepository _categoryRepository;
        private IProductRepository _productRepository;
        private IDiscountRepository _discountRepository;
        private IOutportRepository _outportRepository;
        private ICommentRepository _commentRepository;
        private INotifyRepository _notifyRepository;
        private IUserRepository _userRepository;
        private IGeneralRepository _generalRepository;
        private IStoreRepository _storeRepository;

        public UnitOfWork(string connectionString)
        {
            _dbConnection = new SqlConnection(connectionString);
            _dbConnection.Open();
            _dbTransaction = _dbConnection.BeginTransaction();
            _disposed = false;
        }
        public IStoreRepository StoreRepository
        {
            get
            {
                if (_storeRepository == null)
                {
                    _storeRepository = new StoreRepository(_dbTransaction);
                }
                return _storeRepository;
            }
        }
        public IGeneralRepository GeneralRepository
        {
            get
            {
                if (_generalRepository == null)
                {
                    _generalRepository = new GeneralRepository(_dbTransaction);
                }
                return _generalRepository;
            }
        }
        public IUserRepository UserRepository
        {
            get
            {
                if (_userRepository == null)
                {
                    _userRepository = new UserRepository(_dbTransaction);
                }
                return _userRepository;
            }
        }
        public INotifyRepository NotifyRepository
        {
            get
            {
                if (_notifyRepository == null)
                {
                    _notifyRepository = new NotifyRepository(_dbTransaction);
                }
                return _notifyRepository;
            }
        }
        public IOutportRepository OutportRepository
        {
            get
            {
                if (_outportRepository == null)
                {
                    _outportRepository = new OutportRepository(_dbTransaction);
                }
                return _outportRepository;
            }
        }
        public ICommentRepository CommentRepository
        {
            get
            {
                if (_outportRepository == null)
                {
                    _commentRepository = new CommentRepository(_dbTransaction);
                }
                return _commentRepository;
            }
        }
        public IDiscountRepository DiscountRepository
        {
            get
            {
                if (_discountRepository == null)
                {
                    _discountRepository = new DiscountRepository(_dbTransaction);
                }
                return _discountRepository;
            }
        }
        public IBrandRepository BrandRepository
        {
            get
            {
                if (_brandRepository == null)
                {
                    _brandRepository = new BrandRepository(_dbTransaction);
                }
                return _brandRepository;
            }
        }
        public IDistributorRepository DistributorRepository
        {
            get
            {
                if (_distributorRepository == null)
                {
                    _distributorRepository = new DistributorRepository(_dbTransaction);
                }
                return _distributorRepository;
            }
        }
        public IImportProductRepository ImportRepository
        {
            get
            {
                if (_importRepository == null)
                {
                    _importRepository = new ImportProductRepository(_dbTransaction);
                }
                return _importRepository;
            }
        }
        public ICategoryRepository CategoryRepository
        {
            get
            {
                if (_categoryRepository == null)
                {
                    _categoryRepository = new CategoryRepository(_dbTransaction);
                }
                return _categoryRepository;
            }
        }
        public IProductRepository ProductRepository
        {
            get
            {
                if (_productRepository == null)
                {
                    _productRepository = new ProductRepository(_dbTransaction);
                }
                return _productRepository;
            }
        }
        private void ResetRepositories()
        {
            _brandRepository = null;
        }
        public void Commit()
        {
            try
            {
                _dbTransaction.Commit();
            }
            catch
            {
                _dbTransaction.Rollback();
                throw;
            }
            finally
            {
                _dbConnection.Dispose();
                _dbConnection.Close();
                _dbTransaction.Dispose();
                ResetRepositories();
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (_dbTransaction != null)
                {
                    _dbTransaction.Dispose();
                    _dbTransaction = null;
                }

                if (_dbConnection != null)
                {
                    _dbConnection.Dispose();
                    _dbConnection = null;
                }

                _disposed = true;
            }
        }
    }
}
