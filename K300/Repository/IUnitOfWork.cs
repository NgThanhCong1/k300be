﻿using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Repository
{
    public interface IUnitOfWork : IDisposable
    {
        IBrandRepository BrandRepository { get; }
        IDistributorRepository DistributorRepository { get; }
        IImportProductRepository ImportRepository { get; }
        ICategoryRepository CategoryRepository { get; }
        IProductRepository ProductRepository { get; }
        IDiscountRepository DiscountRepository { get; }
        IOutportRepository OutportRepository { get; }
        ICommentRepository CommentRepository { get; }
        INotifyRepository NotifyRepository { get; }
        IUserRepository UserRepository { get; }
        IGeneralRepository GeneralRepository { get; }
        IStoreRepository StoreRepository { get; }
        void Commit();
    }
}
