﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Models;
using Models.BaseModel;
using Models.RequestModels;
using Models.ResponseModels;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ultilities.Interfaces;

namespace K300.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class DistributorController : BaseController //: ControllerBase 
    {
        private readonly IDistributorService _distributorService;
        public DistributorController(
            IDistributorService distributorService,
            IElastichsearchHelper elastichsearchHelper,
            IBotTelegramHelper botTelegramHelper,
            IRedisCacheHelper redisCacheHelper
            ) : base(elastichsearchHelper, botTelegramHelper, redisCacheHelper)
        {
            _distributorService = distributorService;
        }
        [Authorize(Roles = "Admin, RepoStaff")]
        [HttpGet("get-all-distributor")]
        public async Task<IActionResult> GetAllDistributor(int _page = 0, int _limit = 0, string nameLike = "")
        {
            try
            {
                var result = await _distributorService.GetAll();
                if (result == null || !result.Any()) return NoData;

                var outModel = new DistributorViewModel()
                {
                    /**
                    nếu page = 0 thì lấy tất cả record,
                    nếu page khác 0 và nameLike bằng null thì lấy theo paging
                    nếu page khác 0 và nameLike khác null thì lấy các record contain nameLike theo paging
                     */
                    ListDistributor = _page == 0 
                        ? result.ToList()
                        : nameLike.Equals("null") 
                            ? result.Skip((_page - 1) * _limit).Take(_limit).ToList()
                            : result.Where(s => s.Name.ToLower().Contains(nameLike.ToLower())).Skip((_page - 1) * _limit).Take(_limit).ToList(),

                    Pagination = new Pagination()
                    {
                        _page = _page,
                        _limit = _limit,
                        _totalRows = nameLike.Equals("null") 
                            ? result.Count() 
                            : result.Where(s => s.Name.Contains(nameLike)).ToList().Count(),
                    }
                };

                return outModel.ListDistributor == null || !outModel.ListDistributor.Any()
                    ? NoData
                    : Ok(new BaseResponse<DistributorViewModel>(outModel, 1, "Success"));
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [Authorize(Roles = "Admin")]
        [HttpPost("update-distributor")]
        public async Task<IActionResult> UpdateDistributor(Distributor distributor)
        {
            try
            {
                if (distributor == null) return NotEnoungh;
                distributor.UpdatedBy = UserName;
                var result = await _distributorService.UpdateDistributor(distributor);
                return result > 0
                    ? SuccessNodata
                    : Duplicate;
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [Authorize(Roles = "Admin")]
        [HttpPost("add-distributor")]
        public async Task<IActionResult> AddDistributor(Distributor distributor)
        {
            try
            {
                if (distributor == null) return NotEnoungh;
                distributor.CreatedBy = UserName;
                var result = await _distributorService.AddDistributor(distributor);
                return result > 0
                    ? SuccessNodata
                    : Duplicate;
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        #region add by ggsheet
        //[HttpPost("add-distributors-by-ggSheet")]
        //public async Task<IActionResult> AddDistributorsByGGSheet(GGSheet ggSheet)
        //{
        //    try
        //    {
        //        bool flagCheck = false;
        //        var allDistributors = await _distributorService.GetAll();

        //        List<Distributor> listDistributor = new List<Distributor>();
        //        var ggSheetResult = await _ggSheetHelper.ReadData(ggSheet.Link, ggSheet.SheetName, ggSheet.RangeData);
        //        if (ggSheetResult != null && ggSheetResult.Count() > 0)
        //        {
        //            Console.WriteLine("Task");
        //            foreach (var row in ggSheetResult)
        //            {
        //                if (allDistributors.Any(s => s.Name.ToLower() == row[0].ToLower()))
        //                {
        //                    flagCheck = true;
        //                    continue;
        //                }
        //                listDistributor.Add(new Distributor()
        //                {
        //                    Name = row[0],
        //                    Phone = row[1],
        //                    Address = row[2],
        //                    CreatedBy = UserName
        //                });
        //            }
        //        }
        //        else
        //        {
        //            Console.WriteLine("No data found.");
        //        }
        //        var importResult = await _distributorService.AddListDistributor(listDistributor);
        //        return importResult > 0 ?
        //         flagCheck == false ? Ok(new BaseResponse<int>(importResult, 1, "Success"))
        //            : Ok(new BaseResponse<int>(importResult, 2, "Success, Skip some duplicate"))
        //            : Ok(new BaseResponse<int>(importResult, -1, "Import fail, may be full duplicate"));
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
        #endregion
        [Authorize(Roles = "Admin")]
        [HttpPost("delete-distributor")]
        public async Task<IActionResult> DeleteBrand(Distributor distributor)
        {
            try
            {
                if (distributor == null) return NotEnoungh;
                distributor.UpdatedBy = UserName;
                var result = await _distributorService.DeleteDistributor(distributor);
                return result > 0
                   ? SuccessNodata
                   : Fail;
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [Authorize(Roles = "Admin")]
        [HttpGet("get-data-for-update-distributor")]
        public async Task<IActionResult> GetDataForUpdateDistributor(int id)
        {
            try
            {
                if (id < 1) return NotEnoungh;
                var result = await _distributorService.GetDistributorById(id);
                return result != null 
                    ? Ok(new BaseResponse<Distributor>(result, 1, "Success")) 
                    : NoData;
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
    }
}
