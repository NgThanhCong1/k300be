﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Models.BaseModel;
using Models.RequestModels;
using Models.ResponseModels;
using Newtonsoft.Json;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Ultilities;
using Ultilities.Interfaces;

namespace K300.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class OutportController : BaseController
    {
        private readonly IOutportService _outportService;
        private readonly ISendMailHelper _mailHelper;
        private readonly IConfiguration _configuration;
        public OutportController(
            IOutportService outportService,
            IElastichsearchHelper elastichsearchHelper,
            IBotTelegramHelper botTelegramHelper,
            ISendMailHelper mailHelper,
            IConfiguration configuration,
            IRedisCacheHelper redisCacheHelper
            ) : base(elastichsearchHelper, botTelegramHelper, redisCacheHelper)
        {
            _mailHelper = mailHelper;
            _outportService = outportService;
            _configuration = configuration;
        }
        private string ToVNCurency(string number)
        {
            CultureInfo cul = CultureInfo.GetCultureInfo("vi-VN");   // try with "en-US"
            string money = (double.Parse(number) * 1000).ToString("#,###", cul.NumberFormat);
            return money + " VND";
        }
        private async Task<DataToGmail> GenerateSummaryBuyInfor(int outInvoiceId, List<OutportProduct> listProduct, string totalMoney, string shipAt, string mailTo)
        {
            
            string sListProduct = "";
            foreach (var item in listProduct)
            {

                sListProduct += $"<tr><td width=\"50%\" align = \"left\" style = \"font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px; padding: 15px 100px 5px 10px;\">{item.Name}/{item.ProductCode}</td><td width =\"25%\" align = \"left\" style=\"font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px; padding: 15px 10px 5px 10px;\">{item.CartQuantity}</td><td width =\"25%\" align = \"left\" style=\"font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px; padding: 15px 10px 5px 10px;\">{ToVNCurency(item.Price)}</td></tr>";
            }
            return new DataToGmail()
            {
                mailTemplate = "orderComplete.html",
                from = _configuration["EmailConfig:from"],
                to = mailTo,
                subject = "Order complete confirm",
                title = "Thanks for order",
                mainContent = sListProduct,
                outInvoiceId = outInvoiceId.ToString(),
                totalMoney = ToVNCurency(totalMoney),
                deliveryAddress =shipAt,
            };
        }

        [Authorize(Roles = "Customer")]
        [HttpPost("order")]
        public async Task<IActionResult> Order(OutInvoiceRequest request)
        {
            try
            {
                if (request == null) return NotEnoungh;
                request.CreatedBy = UserName;
                var outInvoiceId = await _outportService.AddOutInvoice(request);
                var result = await _outportService.AddOutInvoiceDetail(outInvoiceId, request.ListOutportProducts, UserName);
                var dataToGmail = await GenerateSummaryBuyInfor(outInvoiceId, request.ListOutportProducts, request.TotalMoney, request.ShipAt, result);
                if (result != null)
                {
                    await _mailHelper.SendMail(dataToGmail);
                    return Ok(new BaseResponse<int>(outInvoiceId, 1, "Success"));
                }
                return Fail;
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [Authorize(Roles = "Customer")]
        [HttpPost("order-with-payment-online")]
        public async Task<IActionResult> OrderWithPaymentOnline(PaymentOnline paymentOnline)
        {
            try
            {
                if (paymentOnline == null) return NotEnoungh;
                paymentOnline.OrderInfor.CreatedBy = UserName;
                var outInvoiceId = await _outportService.AddOutInvoice(paymentOnline.OrderInfor);
                var result = await _outportService.AddOutInvoiceDetail(outInvoiceId, paymentOnline.OrderInfor.ListOutportProducts, UserName);

                var dataToGmail = await GenerateSummaryBuyInfor(outInvoiceId, paymentOnline.OrderInfor.ListOutportProducts, paymentOnline.OrderInfor.TotalMoney, paymentOnline.OrderInfor.ShipAt, result);

                if (result != null)
                {
                    await _mailHelper.SendMail(dataToGmail);
                }
                string paymentUrl = await Pay(paymentOnline.PaymentInfor, outInvoiceId);
                return Ok(new BaseResponse<object>(new
                { 
                    paymentUrl = paymentUrl,
                    outInvoiceId = outInvoiceId
                }
                , 1, "Success"));
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        private async Task<string> Pay(VNPayPayment paymentInfor, int outInvoiceId)
        {
            try
            {
                if (paymentInfor == null || outInvoiceId < 1) return string.Empty;
                string vnp_Url = "http://sandbox.vnpayment.vn/paymentv2/vpcpay.html"; //URL thanh toan cua VNPAY 
                string vnp_TmnCode = _configuration["VPPayConfig:vnp_TmnCode"]; //Ma website
                string vnp_HashSecret = _configuration["VPPayConfig:vnp_HashSecret"]; //Chuoi bi mat

                //Build URL for VNPAY
                VnPayLibrary vnpay = new VnPayLibrary();

                vnpay.AddRequestData("vnp_Version", "2.0.0");
                vnpay.AddRequestData("vnp_Command", "pay");
                vnpay.AddRequestData("vnp_TmnCode", vnp_TmnCode);

                vnpay.AddRequestData("vnp_Locale", "vn");

                vnpay.AddRequestData("vnp_CurrCode", "VND");
                vnpay.AddRequestData("vnp_TxnRef", outInvoiceId.ToString());
                vnpay.AddRequestData("vnp_OrderInfo", "shopping at K300 happy");
                vnpay.AddRequestData("vnp_OrderType", "other"); //default value: other
                vnpay.AddRequestData("vnp_Amount", (Convert.ToDecimal(paymentInfor.TotalMoney * 1000) * 100).ToString());
                vnpay.AddRequestData("vnp_ReturnUrl", $"{_configuration["ClientHost"]}order-complete");
                vnpay.AddRequestData("vnp_IpAddr", paymentInfor.Ip);
                vnpay.AddRequestData("vnp_CreateDate", DateTime.Now.ToString("yyyyMMddHHmmss"));

                vnpay.AddRequestData("vnp_BankCode", paymentInfor.BankCode);

                string paymentUrl = vnpay.CreateRequestUrl(vnp_Url, vnp_HashSecret);
                return paymentUrl;
            }
            catch(Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return string.Empty;
            }
        }
        [Authorize(Roles = "Admin, SellStaff")]
        [HttpGet("get-all-invoices")]
        public async Task<IActionResult> GetAll(int _page = 0, int _limit = 0, int status = -99, int id = 0)
        {
            try
            {
                var result = await _outportService.GetAll();
                if (result == null || !result.Any()) return NoData;

                var outModel = new OutInvoiceRequestViewModel()
                {
                    ListOutInvoiceRequest = _page == 0 
                        ? result.ToList()
                        : id == 0 ?
                            status == 0 ? result.Where(s => s.Status == 0).Skip((_page - 1) * _limit).Take(_limit).ToList()
                            : status == 1 ? result.Where(s => s.Status == 1).Skip((_page - 1) * _limit).Take(_limit).ToList()
                            : status == 2 ? result.Where(s => s.Status == 2).Skip((_page - 1) * _limit).Take(_limit).ToList()
                            : status == 3 ? result.Where(s => s.Status == 3).Skip((_page - 1) * _limit).Take(_limit).ToList()
                            : status == 4 ? result.Where(s => s.Status == 4).Skip((_page - 1) * _limit).Take(_limit).ToList() 
                            : result.Skip((_page - 1) * _limit).Take(_limit).ToList()
                    :
                    result.Where(s=> s.Id == id).ToList()
                    ,

                    Pagination = new Pagination()
                    {
                        _page = _page,
                        _limit = _limit,
                        _totalRows = 
                        id == 0 ? 
                            status == 0 ? result.Count(s => s.Status == 0)
                            : status == 1 ? result.Count(s => s.Status == 1)
                            : status == 2 ? result.Count(s => s.Status == 2)
                            : status == 3 ? result.Count(s => s.Status == 3)
                            : status == 4 ? result.Count(s => s.Status == 4)
                            : result.ToList().Count()
                        : result.Count(s => s.Id == id),
                    }
                };
                return outModel.ListOutInvoiceRequest == null || !outModel.ListOutInvoiceRequest.Any()
                    ? NoData
                    : Ok(new BaseResponse<OutInvoiceRequestViewModel>(outModel, 1, "Success"));
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [Authorize(Roles = "Customer, Admin, SellStaff")]
        [HttpPost("update-outinvoice")]
        public async Task<IActionResult> UpdateOutInvoice(OutInvoiceRequest model, string type)
        {
            try
            {
                if (model == null || string.IsNullOrEmpty(type)) return NotEnoungh;
                model.UpdatedBy = UserName;
                model.StaffId = UserId;

                var listHistoryChanges = new List<OutInvoiceRequest>();
                if(model.HistoryChanges != null) listHistoryChanges = JsonConvert.DeserializeObject<List<OutInvoiceRequest>>(model.HistoryChanges);
                listHistoryChanges.Add(model);
                model.HistoryChanges = JsonConvert.SerializeObject(listHistoryChanges);

                var result = await _outportService.UpdateOutInvoice(model, type);
                return result > 0
                    ? SuccessNodata
                    : Fail;
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [Authorize(Roles = "Admin, SellStaff")]
        [HttpGet("get-outinvoice-detail-by-outInvoiceId")]
        public async Task<IActionResult> GetOutInvoiceDetailByOutInvoiceId(int id)
        {
            try
            {
                if(id < 1) return NotEnoungh;
                var result = await _outportService.GetOutInvoiceDetailByOutInvoiceId(id);
                return result != null
                    ? Ok(new BaseResponse<OutInvoiceDetailMaster>(result, 1, "Success"))
                    : NoData;
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [Authorize(Roles = "Customer, Admin, SellStaff")]
        [HttpGet("get-data-for-update-outInvoice")]
        public async Task<IActionResult> GetDataFroUpdateOutInvoice(int id)
        {
            try
            {
                if (id < 1) return NotEnoungh;
                var result = await _outportService.GetOutInvoicById(id);
                return result != null
                    ? Ok(new BaseResponse<OutInvoiceRequest>(result, 1, "Success"))
                    : NoData;
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [Authorize(Roles = "Admin, SellStaff")]
        [HttpPost("update-outInvoice-data")]
        public async Task<IActionResult> UpdateOutInvoiceData(OutInvoiceRequest outInvoice)
        {
            try
            {
                if (outInvoice == null) return NotEnoungh;
                outInvoice.UpdatedBy = UserName;
                var currentHistory = new List<OutInvoiceRequest>();
                if (outInvoice.HistoryChanges != null) currentHistory = JsonConvert.DeserializeObject<List<OutInvoiceRequest>>(outInvoice.HistoryChanges);
                currentHistory.Add(outInvoice);
                outInvoice.HistoryChanges = JsonConvert.SerializeObject(currentHistory);
                var result = await _outportService.UpdateOutInvoiceData(outInvoice);
                return result > 0 
                    ? SuccessNodata
                    : Fail;
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [Authorize(Roles = "Customer")]
        [HttpGet("get-outinvoice-by-user")]
        public async Task<IActionResult> GetOutinvoiceByUser(int _page, int _limit, string userId, int status)
        {
            try
            {
                if (string.IsNullOrEmpty(userId)) return NotEnoungh;
                var result = await _outportService.GetOutInvoiceByUser(userId, _page, _limit, status);
                return result != null 
                    ? Ok(new BaseResponse<OutInvoiceForUserTrackingViewModel>(result, 1, "Success"))
                    : NoData;
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [Authorize(Roles = "Customer")]
        [HttpGet("get-history-orders")]
        public async Task<IActionResult> GetHistoryOrders(string userId)
        {
            try
            {
                if (string.IsNullOrEmpty(userId)) return NotEnoungh;
                var result = await _outportService.GetHistoryOrders(userId);
                if(result == null || !result.Any()) return NoData;
                //foreach(var item in result)
                //{
                //    item.ListImagesLink = JsonConvert.DeserializeObject<List<string>>(item.ListImages);
                //}
                result.ToList().ForEach(s => s.ListImagesLink = SolutionsCenter.ConvertImagesTextToListImages(s.ListImages));
                return Ok(new BaseResponse<IEnumerable<HistoryOrders>>(result, 1, "Success"));
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [AllowAnonymous]
        [HttpPost("add-keyword")]
        public async Task<IActionResult> AddKeyword(SearchStatic searchStatic)
        {
            try
            {
                if (searchStatic == null) return NotEnoungh;
                if (searchStatic.CustomerId == null || searchStatic.CustomerId == "") 
                    searchStatic.CustomerId = "anonymousCustomerId";
                var result = await _outportService.AddSearchStatic(searchStatic);
                return result > 0
                    ? SuccessNodata
                    : Fail;
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [Authorize(Roles = "Admin, SellStaff, RepoStaff")]
        [HttpGet("get-top-50-keyword")]
        public async Task<IActionResult> GetTop50Keyword()
        {
            try
            {
                var result = await _outportService.GetTop50Keyword();
                return result != null && result.Any()
                    ? NoData
                    : Ok(new BaseResponse<object>(result, 1, "Success"));
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
    }
}
