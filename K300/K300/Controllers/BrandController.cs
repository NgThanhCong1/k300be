﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Models;
using Models.BaseModel;
using Models.RequestModels;
using Models.ResponseModels;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ultilities;
using Ultilities.Interfaces;

namespace K300.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class BrandController : BaseController //: ControllerBase 
    {
        private readonly IGGSheetHelper _ggSheetHelper;
        private readonly IBrandService _brandService;
        public BrandController(
            IBrandService brandService,
            IGGSheetHelper ggSheetHelper,
            IElastichsearchHelper elastichsearchHelper,
            IBotTelegramHelper botTelegramHelper,
            IRedisCacheHelper redisCacheHelper
            ) : base(elastichsearchHelper, botTelegramHelper, redisCacheHelper)
        {
            _brandService = brandService;
            _ggSheetHelper = ggSheetHelper;
        }
        //[Authorize(Roles = "Admin, SellStaff, RepoStaff")]
        [AllowAnonymous]
        [HttpGet("get-all-brand")]
        public async Task<IActionResult> GetAllBrand(int _page = 0, int _limit = 0, string nameLike = "")
        {
            try
            {
                var result = await _redisCacheHelper.GetAsync<IEnumerable<Brand>>(SolutionsCenter.RedisKey_GetAllBrand);
                if(result == null || !result.Any())
                {
                    result = await _brandService.GetAll();
                    if (result == null || !result.Any())
                        return NoData; 
                    await _redisCacheHelper.SetAsync(SolutionsCenter.RedisKey_GetAllBrand, result, TimeSpan.FromDays(30));
                }
                
                var outModel = new BrandViewModel()
                {
                    /**
                    nếu page = 0 thì lấy tất cả record,
                    nếu page khác 0 và nameLike bằng null thì lấy theo paging
                    nếu page khác 0 và nameLike khác null thì lấy các record contain nameLike theo paging
                     */
                    ListBrand = _page == 0 
                        ? result.ToList()
                        : nameLike.Equals("null") 
                            ? result.Skip((_page - 1) * _limit).Take(_limit).ToList()
                            : result.Where(s => s.Name.ToLower().Contains(nameLike.ToLower())).Skip((_page - 1) * _limit).Take(_limit).ToList(),

                    Pagination = new Pagination()
                    {
                        _page = _page,
                        _limit = _limit,
                        _totalRows = nameLike.Equals("null") 
                            ? result.Count() 
                            : result.Count(s => s.Name.Contains(nameLike)),
                    }
                };
                return outModel.ListBrand == null || !outModel.ListBrand.Any()
                    ? NoData 
                    : Ok(new BaseResponse<BrandViewModel>(outModel, 1, ReturnMessages["success"]));
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [Authorize(Roles = "Admin")]
        [HttpPost("update-brand")]
        public async Task<IActionResult> UpdateBrand(Brand brand)
        {
            try
            {
                if (brand == null) 
                    return NotEnoungh;
                brand.UpdatedBy = UserName;
                var result = await _brandService.UpdateBrand(brand);

                if(result > 0)
                {
                    await _redisCacheHelper.DeleteAsync(SolutionsCenter.RedisKey_GetAllBrand);
                    return SuccessNodata;
                }
                return Duplicate;
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [Authorize(Roles = "Admin")]
        [HttpGet("get-data-for-update-brand")]
        public async Task<IActionResult> GetDataForUpdateBrand(int id)
        {
            try
            {
                if(id < 1) return NotEnoungh;
                var result = await _brandService.GetBrandById(id);
                return result != null
                    ? Ok(new BaseResponse<Brand>(result, 1, "Success"))
                    : NoData;
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [Authorize(Roles = "Admin")]
        [HttpPost("add-brand")]
        public async Task<IActionResult> AddBrand(Brand addBrand)
        {
            try
            {
                if (addBrand == null) return NotEnoungh;
                addBrand.CreatedBy = UserName;
                var result = await _brandService.AddBrand(addBrand);

                if (result > 0)
                {
                    await _redisCacheHelper.DeleteAsync(SolutionsCenter.RedisKey_GetAllBrand);
                    return SuccessNodata;
                }
                return Duplicate;
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        #region add by ggSheet
        //[Authorize(Roles = "Admin")]
        //[HttpPost("add-brands-by-ggSheet")]
        //public async Task<IActionResult> AddBrandsByGGSheet(GGSheet ggSheet)
        //{
        //    try
        //    {
        //        if (ggSheet == null)
        //        {
        //            return Ok(new BaseResponse<int>(0, 0, "No ggSheet found from request"));
        //        }
        //        List<Brand> listBrand = new List<Brand>();
        //        var ggSheetResult = await _ggSheetHelper.ReadData(ggSheet.Link, ggSheet.SheetName, ggSheet.RangeData);
        //        if (ggSheetResult != null && ggSheetResult.Any())
        //        {
        //            foreach (var row in ggSheetResult)
        //            {
        //                listBrand.Add(new Brand()
        //                {
        //                    Name = row[0],
        //                    Description = row[1],
        //                    CreatedBy = UserName
        //                });
        //            }
        //        }
        //        else
        //        {
        //            return Ok(new BaseResponse<int>(0, 0, "No data found from ggSheet"));
        //        }
        //        var result = await _brandService.AddListBrand(listBrand);
        //        return
        //            result > 0 && result < listBrand.Count
        //            ? Ok(new BaseResponse<int>(result, 1, "Success, remove some duplicate"))
        //            :
        //            result > 0 && result == listBrand.Count
        //                ? Ok(new BaseResponse<int>(result, 1, "Success"))
        //            : Ok(new BaseResponse<int>(-1, -1, "Fail"));
        //    }
        //    catch (Exception ex)
        //    {
        //        await _botTelegramHelper.TelegramBotSendError(ex.Message);
        //        _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
        //        return Error;
        //    }
        //}
        #endregion
        [Authorize(Roles = "Admin")]
        [HttpPost("delete-brand")]
        public async Task<IActionResult> DeleteBrand(Brand brand)
        {
            try
            {
                if (brand == null) return NotEnoungh;
                brand.UpdatedBy = UserName;
                var result = await _brandService.DeleteBrand(brand);
                if (result > 0)
                {
                    await _redisCacheHelper.DeleteAsync(SolutionsCenter.RedisKey_GetAllBrand);
                    return SuccessNodata;
                }
                return Fail;
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
    }
}
