﻿using K300.Hubs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Models;
using Models.BaseModel;
using Models.RequestModels;
using Models.ResponseModels;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ultilities.Interfaces;

namespace K300.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class NotifyController : BaseController //: ControllerBase 
    {
        private readonly INotifyService _notifyService;
        private readonly IRedisCacheHelper _redisService;
        private readonly ISendMailHelper _sendMailHelper;
        public UserManager<ApplicationUser> _userManager;
        private readonly IConfiguration _configuration;

        public NotifyController(
            INotifyService notifyService,
            IElastichsearchHelper elastichsearchHelper,
            IRedisCacheHelper redisService,
            UserManager<ApplicationUser> userManager,
            IBotTelegramHelper botTelegramHelper,
            ISendMailHelper sendMailHelper,
            IConfiguration configuration,
            IRedisCacheHelper redisCacheHelper
            ) : base(elastichsearchHelper, botTelegramHelper, redisCacheHelper)
        {
            _notifyService = notifyService;
            _redisService = redisService;
            _userManager = userManager;
            _sendMailHelper = sendMailHelper;
            _configuration = configuration;
        }
        [Authorize(Roles = "Customer")]
        [HttpGet("get-notify-by-customer")]
        public async Task<IActionResult> GetNotifyByCustomer(string customerId)
        {
            try
            {
                if (string.IsNullOrEmpty(customerId)) return NotEnoungh;
                string key = $"Notify_{customerId}";
                var dataCache = await _redisService.GetAsync<List<Notify>>(key);
                if(dataCache != null && dataCache.Any())
                {
                    dataCache.Reverse();
                    return Ok(new BaseResponse<List<Notify>>(dataCache, 1, "Success"));
                }
                return NoData;
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [Authorize(Roles = "Customer")]
        [HttpPost("update-notify")]
        public async Task<IActionResult> UpdateNotify(Notify updateNotify)
        {
            try
            {
                if (updateNotify == null) return NotEnoungh; 
                string key = $"Notify_{updateNotify.CustomerId}";
                var dataCache = await _redisService.GetAsync<List<Notify>>(key);
                if (dataCache != null && dataCache.Any())
                {
                    dataCache.ForEach(notify =>
                    {
                        if (notify.Content == updateNotify.Content
                        && notify.Title == updateNotify.Title
                        && notify.Image == updateNotify.Image)
                        {
                            notify.IsWatched = 1;
                        }
                    });
                }
                await _redisService.SetAsync(key, dataCache, TimeSpan.FromDays(10));
                return SuccessNodata;
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [Authorize(Roles = "Admin, SellStaff, Customer")]
        [HttpPost("delete-notify")]
        public async Task<IActionResult> DeleteNotify(Notify deleteNotify)
        {
            try
            {
                if (deleteNotify == null) return NotEnoungh;
                string key = $"Notify_{deleteNotify.CustomerId}";
                var dataCache = await _redisService.GetAsync<List<Notify>>(key);
                if(dataCache != null && dataCache.Any())
                {
                    var deleteItem = new Notify();
                    dataCache.ForEach(notify =>
                        {
                            if (notify.Content == deleteNotify.Content
                            && notify.Title == deleteNotify.Title
                            && notify.Image == deleteNotify.Image)
                            {
                                deleteItem = notify;
                                return;
                            }
                        });
                    dataCache.Remove(deleteItem);
                }
                await _redisService.SetAsync(key, dataCache, TimeSpan.FromDays(10));
                return SuccessNodata;
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [Authorize(Roles = "Admin, SellStaff, Customer")]
        [HttpPost("add-notify")]
        public async Task<IActionResult> AddNotify(Notify addNotify)
        {
            try
            {
                if (addNotify == null) return NotEnoungh;
                if (addNotify.Type == 1)
                {
                    addNotify.Title = $"#{addNotify.InvoiceId} Thank you for new order";
                    addNotify.IsWatched = 0;
                    addNotify.Content = "ContentYour order is processing";
                    addNotify.Image = "/Images/Notifications/pending.png";
                }
                else if(addNotify.Type == 2)
                {
                    addNotify.Title = $"#{addNotify.InvoiceId} Ready to shiping";
                    addNotify.IsWatched = 0;
                    addNotify.Content = "ready to shipping to you";
                    addNotify.Image = "/Images/Notifications/approve.png";
                }
                else if (addNotify.Type == 3)
                {
                    addNotify.Title = $"#{addNotify.InvoiceId} Shiping";
                    addNotify.IsWatched = 0;
                    addNotify.Content = "on the road to your house";
                    addNotify.Image = "/Images/Notifications/shiping.png";
                }
                else if (addNotify.Type == 4)
                {
                    addNotify.Title = $"#{addNotify.InvoiceId} Recieve";
                    addNotify.IsWatched = 0;
                    addNotify.Content = "at your house";
                    addNotify.Image = "/Images/Notifications/recieve.png";
                }
                else if (addNotify.Type == 0)
                {
                    addNotify.Title = $"#{addNotify.InvoiceId} Opps cancel";
                    addNotify.IsWatched = 0;
                    addNotify.Content = "see you later";
                    addNotify.Image = "/Images/Notifications/cancel.png";
                }
                else if (addNotify.Type == 5)
                {
                    addNotify.Title = $"#Discount for you here";
                    addNotify.IsWatched = 0;
                    addNotify.Content = $"discount code: {addNotify.Content}, limited, let go shopping now";
                    addNotify.Image = "/Images/Notifications/discount.png";

                    DataToGmail gmail = new DataToGmail()
                    {
                        mailTemplate = "templateEmail.html",
                        from = _configuration["EmailConfig:from"],
                        to = (await _userManager.FindByIdAsync(addNotify.CustomerId)).Email,
                        subject = "Voucher From K300 to you",
                        title = "Let enjoy my shop",
                        mainContent = $"Discount code: {addNotify.Content}",
                    };
                    await _sendMailHelper.SendMail(gmail);
                }
                string key = $"Notify_{addNotify.CustomerId}";
                var dataCache = await _redisService.GetAsync<List<Notify>>(key);
                if(dataCache == null) dataCache = new List<Notify>();

                dataCache.Add(addNotify);
                await _redisService.SetAsync(key, dataCache, TimeSpan.FromDays(10));
                return SuccessNodata;
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [Authorize(Roles = "Admin")]
        [HttpPost("add-discount-notify")]
        public async Task<IActionResult> AddDiscountNotify(Notify addNotify)
        {
            try
            {
                if (addNotify == null) return NotEnoungh;
                addNotify.Title = $"#Discount for you here";
                addNotify.IsWatched = 0;
                addNotify.Content = $"discount code: {addNotify.Content}, limited, let go shopping now";
                addNotify.Image = "/Images/Notifications/discount.png";

                NotifyHub notifyHub = new NotifyHub(_notifyService, _redisService); 
                var allCustomers = (await _userManager.GetUsersInRoleAsync("Customer")).Where(u => u.IsActive == 1).ToList();
                //var allCustomerIds = allCustomers.Select(s => s.Id).ToList();
                allCustomers.ForEach(async customer =>
                {
                    string key = $"Notify_{customer.Id}";
                    addNotify.CustomerId = customer.Id;
                    var dataCache = await _redisService.GetAsync<List<Notify>>(key);
                    if (dataCache == null) dataCache = new List<Notify>();
                    dataCache.Add(addNotify);
                    await _redisService.SetAsync(key, dataCache, TimeSpan.FromDays(10));
                    DataToGmail gmail = new DataToGmail()
                    {
                        mailTemplate = "templateEmail.html",
                        from = _configuration["EmailConfig:from"],
                        to = customer.Email,
                        subject = "Voucher From K300 to you",
                        title = "Let enjoy my shop",
                        mainContent = $"Discount code: {addNotify.Content}",
                    };
                    await _sendMailHelper.SendMail(gmail);
                });
                return SuccessNodata;
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
    }
}
