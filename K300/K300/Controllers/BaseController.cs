﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Models.BaseModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Ultilities.Interfaces;


namespace K300.Controllers
{
    public class BaseController : ControllerBase
    {
        protected IElastichsearchHelper _elastichsearchHelper;
        protected readonly IRedisCacheHelper _redisCacheHelper;
        protected readonly IBotTelegramHelper _botTelegramHelper;

        protected readonly Dictionary<string, string> ReturnMessages = new Dictionary<string, string>()
        {
            ["not_enough"] = "Not enough data from request or not found object in database",
            ["success"] = "Action success",
            ["fail"] = "Action fail",
            ["no_data"] = "No data return",
            ["error"] = "Error from backend, check telegram or elastich log",
            ["duplicate"] = "Check for duplicate",
        };

        public BaseController(
            IElastichsearchHelper elastichsearchHelper,
            IBotTelegramHelper botTelegramHelper,
            IRedisCacheHelper redisCacheHelper
            )
        {
            _elastichsearchHelper = elastichsearchHelper;
            _botTelegramHelper = botTelegramHelper;
            _redisCacheHelper = redisCacheHelper;
        }
        public string UserName
        {
            get
            {
                return User.Identity.IsAuthenticated ? User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name).Value : string.Empty;
            }
        }
        public string UserId
        {
            get
            {
                return User.Identity.IsAuthenticated ? User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.PrimarySid).Value : string.Empty;
            }
        }
        public OkObjectResult NotEnoungh
        {
            get
            {
                return Ok(new BaseResponse<int>(0, 0, ReturnMessages["not_enough"]));
            }
        }
        public OkObjectResult Fail
        {
            get
            {
                return Ok(new BaseResponse<int>(0, 0, ReturnMessages["fail"]));
            }
        }
        public OkObjectResult SuccessNodata
        {
            get
            {
                return Ok(new BaseResponse<int>(1, 1, ReturnMessages["success"]));
            }
        }
        public OkObjectResult NoData
        {
            get
            {
                return Ok(new BaseResponse<int>(0, 0, ReturnMessages["no_data"]));
            }
        }
        public OkObjectResult Error
        {
            get
            {
                return Ok(new BaseResponse<int>(-1, -1, ReturnMessages["error"]));
            }
        }
        public OkObjectResult Duplicate
        {
            get
            {
                return Ok(new BaseResponse<int>(0, 0, ReturnMessages["duplicate"]));
            }
        }
        //public dynamic FailWithMessage(string value)
        //{
        //    return Ok(new BaseResponse<int>(0, 0, $"{ReturnMessages["fail"]}. Message: ${value}"));
        //}
    }
}
