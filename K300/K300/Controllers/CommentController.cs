﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Models;
using Models.BaseModel;
using Models.RequestModels;
using Models.ResponseModels;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ultilities.Interfaces;

namespace K300.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CommentController : BaseController //: ControllerBase 
    {
        private readonly ICommentService _commentService;
        public CommentController(
            ICommentService commentService,
            IElastichsearchHelper elastichsearchHelper,
            IBotTelegramHelper botTelegramHelper,
            IRedisCacheHelper redisCacheHelper
            ) : base(elastichsearchHelper, botTelegramHelper, redisCacheHelper)
        {
            _commentService = commentService;
        }
        [AllowAnonymous]
        [HttpGet("get-comments-by-product-id")]
        public async Task<IActionResult> GetCommentsByProductId(int _page = 0, int _limit = 0, int productId = 0)
        {
            try
            {
                var result = await _commentService.GetCommentByProductId(productId);
                if (result == null || !result.Any()) return NoData;
                dynamic outModel = new
                {
                    /**
                    nếu page = 0 thì lấy tất cả record,
                    nếu page khác 0 thì lấy theo paging
                     */
                    ListComments = _page == 0 
                        ? result.ToList()
                        : result.Skip((_page - 1) * _limit).Take(_limit).ToList(),

                    Pagination = new Pagination()
                    {
                        _page = _page,
                        _limit = _limit,
                        _totalRows = result.Count(),
                    }
                };
                return Ok(new BaseResponse<object>(outModel, 1, "Success"));
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;;
            }
        }
        [Authorize(Roles = "Customer")]
        [HttpPost("update-comment")]
        public async Task<IActionResult> UpdateComment(Comment comment)
        {
            try
            {
                if (comment == null) return NotEnoungh;
                var result = await _commentService.UpdateComment(comment);
                return result > 0
                    ? SuccessNodata
                    : Fail;
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;;
            }
        }
        [Authorize(Roles = "Customer")]
        [HttpPost("add-comment")]
        public async Task<IActionResult> AddComment(Comment comment)
        {
            try
            {
                if (comment == null) return NotEnoungh;
                var result = await _commentService.AddComment(comment);
                return result > 0
                    ? SuccessNodata
                    : Fail;
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;;
            }
        }
        [Authorize(Roles = "Customer")]
        [HttpPost("delete-comment")]
        public async Task<IActionResult> DeleteComment(Comment comment)
        {
            try
            {
                if (comment == null) return NotEnoungh;
                comment.UpdatedBy = UserName;
                var result = await _commentService.DeleteComment(comment);
                return result > 0
                    ? SuccessNodata
                    : Fail;
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;;
            }
        }
    }
}
