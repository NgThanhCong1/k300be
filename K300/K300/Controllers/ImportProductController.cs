﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Models;
using Models.BaseModel;
using Models.RequestModels;
using Models.ResponseModels;
using Newtonsoft.Json;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Ultilities.Interfaces;

namespace K300.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ImportProductController : BaseController //: ControllerBase 
    {
        private readonly IImportProductService _importProductService;
        private readonly IProductService _productService;
        private readonly IDistributorService _distributorService;
        private readonly IWebHostEnvironment _host;
        private readonly IGGSheetHelper _ggSheetHelper;
        public ImportProductController(
            IImportProductService importProductService,
            IElastichsearchHelper elastichsearchHelper,
            IWebHostEnvironment host,
            IProductService productService,
            IDistributorService distributorService,
            IGGSheetHelper ggSheetHelper,
            IBotTelegramHelper botTelegramHelper,
            IRedisCacheHelper redisCacheHelper
            ) : base(elastichsearchHelper, botTelegramHelper, redisCacheHelper)
        {
            _importProductService = importProductService;
            _host = host;
            _productService = productService;
            _distributorService = distributorService;
            _ggSheetHelper = ggSheetHelper;
        }
        [Authorize(Roles = "Admin, RepoStaff")]
        [HttpGet("get-all-ininvoice")]
        public async Task<IActionResult> GetAllInvoice(int _page = 0, int _limit = 0, int inInvoiceId = -1)
        {
            try
            {
                var result = await _importProductService.GetAll();
                if (result == null || !result.Any()) return NoData;

                var outModel = new InInvoiceViewModel()
                {
                    /**
                    nếu page = 0 thì lấy tất cả record,
                    nếu page khác 0 và inInvoiceId bằng -1 thì lấy theo paging
                    nếu page khác 0 và inInvoiceId khác -1 thì lấy các record = inInvoiceId theo paging
                     */
                    ListInInvoices = _page == 0 
                        ? result.ToList()
                        : inInvoiceId == -1 
                            ? result.Skip((_page - 1) * _limit).Take(_limit).ToList()
                            : result.Where(s => s.Id == inInvoiceId).Skip((_page - 1) * _limit).Take(_limit).ToList(),

                    Pagination = new Pagination()
                    {
                        _page = _page,
                        _limit = _limit,
                        _totalRows = inInvoiceId == -1 
                            ? result.Count() 
                            : result.Where(s => s.Id == inInvoiceId).ToList().Count(),
                    },
                };
                return outModel.ListInInvoices == null || !outModel.ListInInvoices.Any()
                    ? NoData
                    : Ok(new BaseResponse<InInvoiceViewModel>(outModel, 1, "Success"));
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [Authorize(Roles = "Admin, RepoStaff")]
        [HttpPost("add-ininvoice")]
        public async Task<IActionResult> ImportProducts(ImportProductRequest import)
        {
            try
            {
                if (import == null) return NotEnoungh;
                import.CreatedBy = UserName;
                List<ImportProductNew> listImportProduct = new List<ImportProductNew>();
                import.CreatedBy = UserName;
                var ggSheetResult = await _ggSheetHelper.ReadData(import.GGSheet.Link, import.GGSheet.SheetName, import.GGSheet.RangeData);

                if (ggSheetResult == null || !ggSheetResult.Any())
                {
                    return Ok(new BaseResponse<int>(0, 0, "No data found from ggSheet"));
                }
                else
                {
                    foreach (var row in ggSheetResult)
                    {
                        listImportProduct.Add(new ImportProductNew()
                        {
                            ProductCode = row[0],
                            InPrice = row[1],
                            Quantity = Int32.Parse(row[2]),
                        });
                    }
                }
                var result = await _importProductService.ImportProductNew(listImportProduct, import.DistributorId, UserName);
                return result > 0
                    ? SuccessNodata
                    : Fail;
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [Authorize(Roles = "Admin, RepoStaff")]
        [HttpGet("get-all-import-details")]
        public async Task<IActionResult> GetAllImprotDetail(int _page = 0, int _limit = 0, string nameLike = "")
        {
            try
            {
                var result = await _importProductService.GetAllImprotDetail();
                if (result == null || !result.Any()) return NoData;

                var outModel = new ImportDetailViewModel()
                {
                    ListImportDetails = _page == 0 
                        ? result.ToList()
                        : nameLike.Equals("null") 
                            ? result.Skip((_page - 1) * _limit).Take(_limit).ToList()
                            : result.Where(s => s.Name.ToLower().Contains(nameLike.ToLower())).Skip((_page - 1) * _limit).Take(_limit).ToList(),

                    Pagination = new Pagination()
                    {
                        _page = _page,
                        _limit = _limit,
                        _totalRows = nameLike.Equals("null") ? result.Count() : result.Count(s => s.Name.Contains(nameLike)),
                    }
                };
                return Ok(new BaseResponse<ImportDetailViewModel>(outModel, 1, "Success"));
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [Authorize(Roles = "Admin, RepoStaff")]
        [HttpPost("update-import-product")]
        public async Task<IActionResult> UpdateImportProduct(ImportDetail importDetail)
        {
            try
            {
                if (importDetail == null) return NotEnoungh;
                var listProductConfigs = await _importProductService.GetProductConfigsByProductCode(importDetail.ProductCode);
                var listImportProductConfigs = JsonConvert.DeserializeObject<List<ProductConfig>>(importDetail.ProductConfigs);
                var newList = new List<ProductConfig>();

                if (listProductConfigs != null && listProductConfigs.Any())
                {
                    foreach (var itemNew in listImportProductConfigs)
                    {
                        foreach (var itemOld in listProductConfigs)
                        {
                            if (itemNew.Color == itemOld.Color && itemNew.Size == itemOld.Size)
                            {
                                //if(Enum.IsDefined(int, itemNew))
                                itemOld.Quantity += itemNew.Quantity;
                                break;
                            }
                            else if (!CheckProductConfigsExist(listProductConfigs, itemNew))
                            {
                                newList.Add(itemNew);
                                break;
                            }
                        }
                    }
                    listProductConfigs.AddRange(newList);
                }
                else
                {
                    listProductConfigs = listImportProductConfigs;
                }
                var result = await _importProductService.UpdateImportDetail(importDetail.ProductCode, importDetail.InInvoceId, importDetail.ProductId, importDetail.ProductConfigs, JsonConvert.SerializeObject(listProductConfigs));
                return result > 0
                    ? SuccessNodata
                    : Fail;
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        private bool CheckProductConfigsExist(List<ProductConfig> listProductConfigs, ProductConfig productConfig)
        {
            foreach(var item in listProductConfigs)
            {
                if(item.Color == productConfig.Color && item.Size == productConfig.Size)
                {
                    return true;
                }
            }
            return false;
        }
        [Authorize(Roles = "Admin, RepoStaff")]
        [HttpGet("get-data-for-add-ininvoice")]
        public async Task<IActionResult> GetDataForAddInInvoice()
        {
            try
            {
                var result = await _distributorService.GetAll();
                return result != null && result.Any()
                    ? Ok(new BaseResponse<object>(new { listDistributors = result.ToList() }, 1, "Success"))
                    : NoData;
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [Authorize(Roles = "Admin, RepoStaff")]
        [HttpGet("get-ininvoice-detail")]
        public async Task<IActionResult> GetInInvoiceDetail(int inInvoiceId)
        {
            try
            {
                if (inInvoiceId < 1) return NotEnoungh;
                var result = await _importProductService.GetInInvoiceDetailByInInvoiceId(inInvoiceId);
                return result != null && result.Any()
                    ? Ok(new BaseResponse<object>(new { listInInvoiceDetails = result.ToList() }, 1, "Success"))
                    : NoData;
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
    }
}
