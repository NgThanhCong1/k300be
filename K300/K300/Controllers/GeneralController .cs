﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Models;
using Models.BaseModel;
using Models.RequestModels;
using Models.ResponseModels;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ultilities.Interfaces;

namespace K300.Controllers
{
    [Authorize(Roles = "Admin, SellStaff, RepoStaff")]
    [Route("api/[controller]")]
    [ApiController]
    public class GeneralController : BaseController //: ControllerBase 
    {
        private readonly IRedisCacheHelper _redisService;
        private readonly IGeneralService _generalService;
        public GeneralController(
            IGeneralService generalService,
            IElastichsearchHelper elastichsearchHelper,
            IRedisCacheHelper redisService,
            IBotTelegramHelper botTelegramHelper,
            IRedisCacheHelper redisCacheHelper
            ) : base(elastichsearchHelper, botTelegramHelper, redisCacheHelper)
        {
            _generalService = generalService;
            _redisService = redisService;
        }
        [HttpGet("get-data-for-dashboard")]
        public async Task<IActionResult> GetDataForDashBoard()
        {
            try
            {
                string key = $"Dashboard";
                var dataCache = await _redisService.GetAsync<DashBoard>(key);
                if(dataCache != null)
                {
                    //return Ok(new BaseResponse<DashBoard>(dataCache, 1, "Success"));
                }
                var result = await _generalService.GetDataForDashBoard();

                if (result != null)
                {
                    await _redisService.SetAsync(key, result, TimeSpan.FromDays(1));
                    return Ok(new BaseResponse<DashBoard>(result, 1, "Success"));
                }
                return NoData;
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [HttpPost("add-direct-chat")]
        public async Task<IActionResult> AddDirectChat(DirectChat directChat)
        {
            try
            {
                if (directChat == null) return NotEnoungh;
                string key = $"Direct_chat";
                directChat.CreatedAt = DateTime.Now;
                var dataCache = await _redisService.GetAsync<List<DirectChat>>(key);
                if (dataCache == null) dataCache = new List<DirectChat>();
                dataCache.Add(directChat);
                await _redisService.SetAsync(key, dataCache, TimeSpan.FromDays(36500));
                return SuccessNodata;
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [HttpGet("get-2months-recent-keywords")]
        public async Task<IActionResult> Get2MonthKeyword(int _page = 0, int _limit = 0, string keyword = "")
        {
            try
            {
                var result = await _generalService.Get2MonthKeyword();
                if (result == null || !result.Any()) return NoData;

                var outModel = new
                {
                    /**
                    nếu page = 0 thì lấy tất cả record,
                    nếu page khác 0 và keyword bằng null thì lấy theo paging
                    nếu page khác 0 và keyword khác null thì lấy các record contain keyword theo paging
                     */
                    ListKeywords = _page == 0 
                        ? result.ToList()
                        : keyword.Equals("null") 
                            ? result.Skip((_page - 1) * _limit).Take(_limit).ToList()
                            : result.Where(s => s.Keyword.ToLower().Contains(keyword.ToLower())).Skip((_page - 1) * _limit).Take(_limit).ToList(),

                    Pagination = new Pagination()
                    {
                        _page = _page,
                        _limit = _limit,
                        _totalRows = keyword.Equals("null") 
                            ? result.Count() 
                            : result.Where(s => s.Keyword.Contains(keyword)).ToList().Count(),
                    }
                };
                return outModel.ListKeywords == null || !outModel.ListKeywords.Any()
                    ? NoData
                    : Ok(new BaseResponse<object>(outModel, 1, "Success"));
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
    }
}
