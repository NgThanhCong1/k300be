﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Models.BaseModel;
using Models.RequestModels;
using Models.ResponseModels;
using Newtonsoft.Json;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Ultilities.Interfaces;

namespace K300.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : BaseController 
    {
        public UserManager<ApplicationUser> _userManager;
        public SignInManager<ApplicationUser> _signInManager;
        private readonly IWebHostEnvironment _host;
        private readonly IUserService _userService;
        public RoleManager<IdentityRole> _roleManager;
        public IdentityUserRole<string> _userRole;
        public IConfiguration _config;
        public ISendMailHelper _mailHelper;
        private readonly JwtSecurityTokenHandler _jwtSecurityTokenHandler = new JwtSecurityTokenHandler();
        public UserController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            RoleManager<IdentityRole> roleManager,
            IConfiguration config,
            IWebHostEnvironment host,
            ISendMailHelper mailHelper,
            IUserService userService,
            IElastichsearchHelper elastichsearchHelper,
            IBotTelegramHelper botTelegramHelper,
            IRedisCacheHelper redisCacheHelper
            ) : base(elastichsearchHelper, botTelegramHelper, redisCacheHelper)
        {
            _signInManager = signInManager;
            _userManager = userManager;
            _roleManager = roleManager;
            _config = config;
            _host = host;
            _mailHelper = mailHelper;
            _userService = userService;
        }
        [AllowAnonymous]
        [HttpPost("create-user")]
        public async Task<IActionResult> CreatUser(UserRequest userRequest, string role)
        {
            try
            {
                if (userRequest == null || string.IsNullOrEmpty(role)) 
                    return NotEnoungh;
                if(role.ToLower() == "customer")
                {
                    var user = new ApplicationUser()
                    {
                        UserName = userRequest.UserName,
                        Email = userRequest.UserName,
                        FirstName = userRequest.FirstName,
                        LastName = userRequest.LastName,
                        IsActive = 1,
                        CreatedAt = DateTime.Now
                    };
                    var result = await _userManager.CreateAsync(user, userRequest.Password);
                    if (result.Succeeded)
                    {
                        var resultRole = await _userManager.AddToRolesAsync(user, new string[] { $"{"Customer"}" });
                        var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                        code = WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(code));

                        var href = $"{_config["ClientHost"]}confirm-email-result/{user.Id}/{code}";
                        var content = $"click to confirm email: <a href='{href}'>Click here</a>";
                        DataToGmail gmail = new DataToGmail()
                        {
                            mailTemplate = "templateEmail.html",
                            from = _config["EmailConfig:from"],
                            to = userRequest.Email,
                            subject = "Authenticate Email K300",
                            title = "Authennticate email from K300 shop",
                            mainContent = content,
                        };
                        await _mailHelper.SendMail(gmail);
                        return SuccessNodata;
                    }
                }
                else 
                {
                    var user = new ApplicationUser()
                    {
                        UserName = userRequest.UserName,
                        FirstName = userRequest.FirstName,
                        LastName = userRequest.LastName,
                        Email = userRequest.UserName,
                        EmailConfirmed = true,
                        IsActive = 1,
                        CreatedAt = DateTime.Now
                    };
                    var result = await _userManager.CreateAsync(user, userRequest.Password);
                    if (result.Succeeded)
                    {
                        var resultRole = await _userManager.AddToRolesAsync(user, new string[] { $"{role}" });
                        return SuccessNodata;
                    }
                }
                return Fail;
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [AllowAnonymous]
        [HttpPost("user-login")]
        public async Task<IActionResult> UserLogin(UserLogin userLogin)
        {
            try
            {
                if (userLogin == null) 
                    return NotEnoungh;
                var signIn = await _signInManager.PasswordSignInAsync(userLogin.username, userLogin.password, false, false);
                if (signIn.Succeeded)
                {
                    var userCheck = await _userManager.FindByNameAsync(userLogin.username);
                    if (userCheck.EmailConfirmed != true || userCheck.IsActive == 0)
                    {
                        return Ok(new BaseResponse<int>(-1, -1, "Login fail, email not confirmed yet or account disabled"));
                    }
                    var roles = await _userManager.GetRolesAsync(userCheck);
                    var claims = new List<Claim>{
                        new Claim(ClaimTypes.PrimarySid, userCheck.Id),
                        new Claim(ClaimTypes.Name, userCheck.FirstName),
                        new Claim(ClaimTypes.Name, userCheck.LastName),
                        new Claim(ClaimTypes.NameIdentifier, userCheck.UserName),
                        new Claim(ClaimTypes.UserData, userCheck.Avatar ?? "")
                    };
                    var listPermissions = new List<RolePermission>();
                    foreach (var userRole in roles)
                    {
                        listPermissions.AddRange(await _userService.GetAllPermissionByRole(userRole));
                        claims.Add(new Claim(ClaimTypes.Role, userRole));
                    }

                    var secrectKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
                    var signingCredentials = new SigningCredentials(secrectKey, SecurityAlgorithms.HmacSha256);

                    var token = new JwtSecurityToken(
                        _config["Jwt:Issuer"],
                        _config["Jwt:Audience"],
                        claims,
                        DateTime.Now,
                        DateTime.Now.AddDays(7),
                        signingCredentials);
                    var encodedToken = _jwtSecurityTokenHandler.WriteToken(token);
                    JWTInfor jwt = new JWTInfor(encodedToken);
                    return Ok(new BaseResponse<object>(new //UserResponseViewModel
                    {
                        JWTInfor = jwt,
                        User = new UserRequest()
                        {
                            Id = userCheck.Id,
                            Address = userCheck.Address,
                            Avatar = userCheck.Avatar,
                            UserName = userCheck.UserName,
                            Email = userCheck.Email,
                            Phone = userCheck.PhoneNumber,
                            FirstName = userCheck.FirstName,
                            LastName = userCheck.LastName,
                        },
                        ListPermissions = listPermissions
                    }
                , 1, "Success"));
                }
                return Ok(new BaseResponse<string>("", 0, "Login fail, check username and password again"));
                //WithMessage("Login fail, check username or password again")
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [AllowAnonymous]
        [HttpPost("get-refresh-token")]
        public async Task<IActionResult> GetRefreshToken(RefreshToken rfToken)
        {
            try
            {
                var stream = rfToken.Token;
                var handler = new JwtSecurityTokenHandler();
                var jsonToken = handler.ReadToken(stream);
                var tokenS = jsonToken as JwtSecurityToken;

                var userId = tokenS.Claims.First(claim => claim.Type == "http://schemas.microsoft.com/ws/2008/06/identity/claims/primarysid").Value;

                var userCheck = await _userManager.FindByIdAsync(userId);
                if (userCheck.EmailConfirmed != true || userCheck.IsActive == 0)
                {
                    return Ok(new BaseResponse<int>(-1, -1, "Login fail, email not confirmed yet or account disabled"));
                }
                var roles = await _userManager.GetRolesAsync(userCheck);
                var claims = new List<Claim>{
                        new Claim(ClaimTypes.PrimarySid, userCheck.Id),
                        new Claim(ClaimTypes.Name, userCheck.FirstName),
                        new Claim(ClaimTypes.Name, userCheck.LastName),
                        new Claim(ClaimTypes.NameIdentifier, userCheck.UserName),
                        new Claim(ClaimTypes.UserData, userCheck.Avatar ?? "")
                    };
                var listPermissions = new List<RolePermission>();
                foreach (var userRole in roles)
                {
                    listPermissions.AddRange(await _userService.GetAllPermissionByRole(userRole));
                    claims.Add(new Claim(ClaimTypes.Role, userRole));
                }

                var secrectKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
                var signingCredentials = new SigningCredentials(secrectKey, SecurityAlgorithms.HmacSha256);

                var token = new JwtSecurityToken(
                    _config["Jwt:Issuer"],
                    _config["Jwt:Audience"],
                    claims,
                    DateTime.Now,
                    DateTime.Now.AddDays(7),
                    signingCredentials);
                var encodedToken = _jwtSecurityTokenHandler.WriteToken(token);
                JWTInfor jwt = new JWTInfor(encodedToken);
                return Ok(new BaseResponse<object>(new //UserResponseViewModel
                {
                    JWTInfor = jwt,
                    User = new UserRequest()
                    {
                        Id = userCheck.Id,
                        Address = userCheck.Address,
                        Avatar = userCheck.Avatar,
                        UserName = userCheck.UserName,
                        Email = userCheck.Email,
                        Phone = userCheck.PhoneNumber,
                        FirstName = userCheck.FirstName,
                        LastName = userCheck.LastName,
                    },
                    ListPermissions = listPermissions
                }
            , 1, "Success"));
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        #region get user login
        //[HttpPost("get-logined-user")]
        //public async Task<IActionResult> GetLoginedUser()
        //{
        //    string userName = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value;
        //    if(userName == null)
        //    {
        //        //await _signInManager.SignOutAsync();
        //        return Ok(new BaseResponse<int>(1, 0, "login again"));
        //    }
        //    return Ok(new BaseResponse<string>(userName, 1, "da dang nhap"));
        //}
        //[HttpPost("add-role")]
        //public async Task<IActionResult> AddRole(string roleName)
        //{
        //    var newRole = new IdentityRole(roleName);
        //    // Thực hiện tạo Role mới
        //    var rsNewRole = await _roleManager.CreateAsync(newRole);

        //    if (rsNewRole.Succeeded)
        //    {
        //        return Ok(new BaseResponse<int>(1, 1, "Success"));
        //    }
        //    else
        //    {
        //        string statusMessage = "Error: ";
        //        foreach (var er in rsNewRole.Errors)
        //        {
        //            statusMessage += er.Description;
        //        }
        //        return Ok(new BaseResponse<string>(statusMessage, -1, "Fail"));
        //    }
        //}
        #endregion
        [HttpPost("remove-all-roles-of-user")]
        public async Task<IActionResult> RemoveAllRolesOfUser(AssignRole assignRoles)
        {
            try
            {
                if (assignRoles == null) 
                    return NotEnoungh;
                var user = await _userManager.FindByNameAsync(assignRoles.UserName);
                if (user != null)
                {
                    var roles = await _userManager.GetRolesAsync(user);
                    var result = await _userManager.RemoveFromRolesAsync(user, roles);
                    if (result.Succeeded)
                    {
                        await _userManager.AddToRolesAsync(user, assignRoles.ListRoleNames);
                        return SuccessNodata;
                    }
                }
                return Fail;
            }
            catch(Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        //[HttpPost("assign-role")]
        //public async Task<IActionResult> AssignRole(AssignRole assignRole)
        //{
        //    //string userName = User.FindFirst(ClaimTypes.Name)?.Value;
        //    //string userName = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value;
        //    var user = await _userManager.FindByNameAsync(assignRole.UserName);
        //    if(user != null)
        //    {
        //        var result = await _userManager.AddToRolesAsync(user, new string[] { $"{assignRole.RoleName}" });

        //        if (result.Succeeded)
        //        {
        //            return Ok(new BaseResponse<int>(1, 1, "Success"));
        //        }
        //    }
        //    return Ok(new BaseResponse<int>(-1, -1, "Fail"));
        //}
        [Authorize(Roles = "Admin")]
        [HttpGet("get-data-for-assign-role")]
        public async Task<IActionResult> GetDataForAssignRole(string userName)
        {
            try
            {
                if (string.IsNullOrEmpty(userName)) 
                    return NotEnoungh;
                var allRoles = _roleManager.Roles.ToList();
                // Resolve the user via their email
                var user = await _userManager.FindByNameAsync(userName);
                if (user != null)
                {
                    // Get the roles for the user
                    var rolesOfUser = await _userManager.GetRolesAsync(user);
                    return Ok(new BaseResponse<object>(new
                    {
                        listRoles = allRoles,
                        listRolesOfUser = rolesOfUser
                    }
                    , 1, "Ok"));
                }
                return Fail;
            }
            catch(Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [Authorize(Roles = "Admin, SellStaff, RepoStaff, Customer")] //Customer
        [HttpPost("update-user")]
        public async Task<IActionResult> UpdateUser([FromForm]UserRequest requestUser)
        {
            try 
            {
                if (requestUser == null) return NotEnoungh;
                var user = await _userManager.FindByIdAsync(requestUser.Id);
                if (user != null)
                {
                    if(requestUser.Address != "undefined")
                    {
                        user.Address = requestUser.Address;
                        user.PhoneNumber = requestUser.Phone;
                        user.FirstName = requestUser.FirstName;
                        user.LastName = requestUser.LastName;
                    }
                    
                    if (requestUser.ImageFile != null && requestUser.ImageFile.Length > 0)
                    {
                        string directory = Path.Combine(_host.WebRootPath, "Images/Users/", requestUser.Id);
                        //directory = Regex.Replace(directory, @"\s+", "");
                        if (!Directory.Exists(directory))
                        {
                            Directory.CreateDirectory(directory);
                        }
                        var fileName = requestUser.Id + "0";
                        var filePath = Path.Combine(directory, fileName + ".jpg");
                        //filePath = Regex.Replace(filePath, @"\s+", "");
                        using (var stream = new FileStream(filePath, FileMode.Create))
                        {
                            user.Avatar = filePath.Replace(_host.WebRootPath, null).Replace("\\", "/");
                            await requestUser.ImageFile.CopyToAsync(stream);
                        }
                    }
                    var resukt = await _userManager.UpdateAsync(user);
                    return Ok(new BaseResponse<UserRequest>(
                        new UserRequest()
                        {
                            Id = user.Id,
                            Address = user.Address,
                            Avatar = user.Avatar,
                            UserName = user.UserName,
                            Email = user.Email,
                            Phone = user.PhoneNumber,
                            FirstName = user.FirstName,
                            LastName = user.LastName,
                        }
                        , 1, "Success"));
                }
                return Fail;
            }
            catch(Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [AllowAnonymous]
        [HttpPost("confirm-email")]
        public async Task<IActionResult> ConfirmEmail(ConfirmEmail confirm)
        {
            try
            {
                if (confirm == null) 
                    return NotEnoungh;
                var user = await _userManager.FindByIdAsync(confirm.UserId);
                if (user == null)
                {
                    return Ok(new BaseResponse<string>("Confirmc fail", -1, $"Unable to load user with ID '{confirm.UserId}'."));
                }
                confirm.Code = Encoding.UTF8.GetString(WebEncoders.Base64UrlDecode(confirm.Code));
                var result = await _userManager.ConfirmEmailAsync(user, confirm.Code);
                return result.Succeeded 
                    ? Ok(new BaseResponse<string>("Confirmc success", 1, "success"))
                    : Fail;
            }
            catch(Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [AllowAnonymous]
        [HttpPost("forgot-password")]
        public async Task<IActionResult> ForgotPassword(UserRequest userRequest)
        {
            try
            {
                if (userRequest == null) 
                    return NotEnoungh;
                var user = await _userManager.FindByEmailAsync(userRequest.Email);
                if (user == null || !(await _userManager.IsEmailConfirmedAsync(user)))
                {
                    return Ok(new BaseResponse<string>("", -1, "donot exist or no confirm email yet"));
                }

                var code = await _userManager.GeneratePasswordResetTokenAsync(user);
                code = WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(code));

                var href = $"{_config["ClientHost"]}reset-password/{code}/{userRequest.Email}";
                var content = $"click to reset password: <a href='{href}'>Click here</a>";
                DataToGmail gmail = new DataToGmail()
                {
                    mailTemplate = "templateEmail.html",
                    from = _config["EmailConfig:from"],
                    to = userRequest.Email,
                    subject = "Recovery Password K300",
                    title = "Recovery",
                    mainContent = content,
                };
                await _mailHelper.SendMail(gmail);
                return Ok(new BaseResponse<string>("", 1, "check your mail"));
            }
            catch(Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [AllowAnonymous]
        [HttpPost("reset-password")]
        public async Task<IActionResult> ResetPassword(ResetPassword reset)
        {
            try
            {
                if (reset == null) 
                    return NotEnoungh;
                IdentityResult result = null;
                if (reset.Id == null)
                {
                    var user = await _userManager.FindByEmailAsync(reset.Email);
                    if (user == null)
                        return Ok(new BaseResponse<string>("No user found", -1, "fail"));

                    result = await _userManager.ResetPasswordAsync(user, Encoding.UTF8.GetString(WebEncoders.Base64UrlDecode(reset.Code)), reset.NewPassword);
                }
                else
                {
                    var user = await _userManager.FindByIdAsync(reset.Id);
                    PasswordVerificationResult passresult = _userManager.PasswordHasher.VerifyHashedPassword(user, user.PasswordHash, reset.OldPassword);
                    if ((int)(passresult) != 1)
                        return Ok(new BaseResponse<string>("", -1, "old password not match"));
                    if (user == null)
                        return Ok(new BaseResponse<string>("Reset fail", -1, "fail"));
                    var token = await _userManager.GeneratePasswordResetTokenAsync(user);
                    result = await _userManager.ResetPasswordAsync(user, token, reset.NewPassword);
                }
                if (result.Succeeded) 
                    return SuccessNodata;
                return Fail;
            }
            catch(Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [Authorize(Roles = "Admin")]
        [HttpGet("get-all-users")]
        public async Task<IActionResult> GetAllUsers(int _page = 0, int _limit = 0, string userName = "")
        {
            try
            {
                var result = _userManager.Users.Where(s => s.IsActive == 1);
                if (result == null || !result.Any()) return NoData;

                var outModel = new ApplicationUserViewModel()
                {
                    ListApplicationUsers = _page == 0 
                        ? result.ToList()
                        : userName.Equals("null") 
                            ? result.Skip((_page - 1) * _limit).Take(_limit).ToList()
                            : result.Where(s => s.UserName.ToLower().Contains(userName.ToLower())).Skip((_page - 1) * _limit).Take(_limit).ToList(),

                    Pagination = new Pagination()
                    {
                        _page = _page,
                        _limit = _limit,
                        _totalRows = userName.Equals("null") 
                            ? result.Count() 
                            : result.Count(s => s.UserName.Contains(userName)),
                    }
                };
                return outModel.ListApplicationUsers != null && outModel.ListApplicationUsers.Any()
                    ? Ok(new BaseResponse<ApplicationUserViewModel>(outModel, 1, "Success"))
                    : NoData;
            }
            catch(Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [Authorize(Roles = "Admin, SellStaff, RepoStaff")]
        [HttpGet("get-users-by-role")]
        public async Task<IActionResult> GetUsersByRole(int _page = 0, int _limit = 0, string userName = "", string role="")
        {
            try
            {
                var userInRole = role.ToLower().Equals("all") 
                    ? _userManager.Users.Where(s => s.IsActive == 1).ToList()
                    : await _userManager.GetUsersInRoleAsync(role);
                var result = userInRole.Where(s => s.IsActive == 1);
                if (result == null || !result.Any()) return NoData;
                
                var outModel = new ApplicationUserViewModel()
                {
                    ListApplicationUsers = _page == 0 
                        ? result.ToList()
                        : userName.Equals("null") 
                            ? result.Skip((_page - 1) * _limit).Take(_limit).ToList()
                            : result.Where(s => s.UserName.ToLower().Contains(userName.ToLower())).Skip((_page - 1) * _limit).Take(_limit).ToList(),

                    Pagination = new Pagination()
                    {
                        _page = _page,
                        _limit = _limit,
                        _totalRows = userName.Equals("null") 
                            ? result.Count() 
                            : result.Count(s => s.UserName.Contains(userName)),
                    }
                };

                return Ok(new BaseResponse<ApplicationUserViewModel>(outModel, 1, "Success"));
            }
            catch(Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [Authorize(Roles = "Customer, Admin, SellStaff, RepoStaff")]
        [HttpGet("get-user-by-id")]
        public async Task<IActionResult> GetUserById(string id)
        {
            try
            {
                if (string.IsNullOrEmpty(id)) return NotEnoungh;
                var result = await _userManager.FindByIdAsync(id);
                return result == null
                    ? NoData
                    : Ok(new BaseResponse<ApplicationUser>(result, 1, "Success"));
            }
            catch(Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [Authorize(Roles = "Admin")]
        [HttpGet("get-all-roles")]
        public async Task<IActionResult> GetAllRoles(int _page = 0, int _limit = 0, string nameLike = "")
        {
            try
            {
                var result = _roleManager.Roles;
                if (result == null || !result.Any()) return NoData;
                
                return Ok(new BaseResponse<object>(new 
                {
                    ListRoles = _page == 0 
                        ? result.ToList()
                        : nameLike.Equals("null") 
                            ? result.Skip((_page - 1) * _limit).Take(_limit).ToList()
                            : result.Where(s => s.Name.ToLower().Contains(nameLike.ToLower())).Skip((_page - 1) * _limit).Take(_limit).ToList(),

                    Pagination = new Pagination()
                    {
                        _page = _page,
                        _limit = _limit,
                        _totalRows = nameLike.Equals("null") 
                            ? result.Count() 
                            : result.Count(s => s.Name.Contains(nameLike)),
                    }
                }, 1, "Success"));

            }
            catch(Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [Authorize(Roles = "Admin")]
        [HttpPost("add-role")]
        public async Task<IActionResult> AddRole(RoleRequest roleRequest)
        {
            try
            {
                if (roleRequest == null) return NotEnoungh;
                var newRole = new IdentityRole(roleRequest.Role.Name);
                var rsNewRole = await _roleManager.CreateAsync(newRole);

                if (rsNewRole.Succeeded)
                {
                    var result = await _userService.AddListRolePermission(roleRequest.ListRolePermissions);
                    return Ok(new BaseResponse<int>(result, 1, "Success"));
                }
                return Fail;
            }
            catch(Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [Authorize(Roles = "Admin")]
        [HttpPost("delete-role")]
        public async Task<IActionResult> DeleteRole(Role role)
        {
            try
            {
                if (role == null) return NotEnoungh;
                var roleDelete = await _roleManager.FindByNameAsync(role.Name);
                var rsNewRole = await _roleManager.DeleteAsync(roleDelete);
                return rsNewRole.Succeeded
                    ? SuccessNodata
                    : Fail;
            }
            catch(Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [Authorize(Roles = "Admin")]
        [HttpPost("delete-user")]
        public async Task<IActionResult> DeleteUser(UserRequest userRequest)
        {
            try
            {
                if (userRequest == null) return NotEnoungh;
                var userDelete = await _userManager.FindByIdAsync(userRequest.Id);
                if (userDelete == null) return NoData;

                userDelete.IsActive = 0;
                await _userManager.UpdateAsync(userDelete);
                return SuccessNodata;
            }
            catch(Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [Authorize(Roles = "Admin")]
        [HttpGet("get-permissions-by-role")]
        public async Task<IActionResult> GetPermissionsByRole(string roleName)
        {
            try
            {
                if (string.IsNullOrEmpty(roleName)) return NotEnoungh;
                var result = await _userService.GetAllPermissionByRole(roleName);
                return result != null && result.Any()
                    ? Ok(new BaseResponse<object>(result, 1, "Success"))
                    : SuccessNodata;
            }
            catch(Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [Authorize(Roles = "Admin")]
        [HttpPost("update-permissions-of-role")]
        public async Task<IActionResult> UpdatePermissionOfRole(RoleRequest roleRequest)
        {
            try
            {
                if (roleRequest == null) return NotEnoungh;
                var result = await _userService.UpdatePermissionsOfRole(roleRequest.ListRolePermissions);
                return result > 0
                    ? SuccessNodata
                    : Fail;
            }
            catch(Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [HttpPost("change-email")]
        public async Task<IActionResult> ChangeEmail(ChangeEmail emailInfor)
        {
            try
            {
                if (emailInfor == null) return NotEnoungh;
                var userInfor = await _userManager.FindByEmailAsync(emailInfor.OldEmail);
                if (userInfor == null) return NoData;

                var code = await _userManager.GenerateEmailConfirmationTokenAsync(userInfor);
                code = WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(code));

                var href = $"{_config["ClientHost"]}confirm-change-email-result/{userInfor.Id}/{code}/{emailInfor.NewEmail}";
                var content = $"click to confirm email: <a href='{href}'>Click here</a>";
                DataToGmail gmail = new DataToGmail()
                {
                    mailTemplate = "templateEmail.html",
                    from = _config["EmailConfig:from"],
                    to = emailInfor.NewEmail,
                    subject = "Authenticate Email K300",
                    title = "Authenticate email from K300 shop",
                    mainContent = content,
                };
                await _mailHelper.SendMail(gmail);
                return SuccessNodata;
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [AllowAnonymous]
        [HttpPost("confirm-change-email")]
        public async Task<IActionResult> ConfirmChangeEmail(ConfirmEmail confirm)
        {
            try
            {
                if (confirm == null)
                    return NotEnoungh;
                var user = await _userManager.FindByIdAsync(confirm.UserId);
                if (user == null)
                {
                    return Ok(new BaseResponse<string>("Confirmc fail", -1, $"Unable to load user with ID '{confirm.UserId}'."));
                }
                confirm.Code = Encoding.UTF8.GetString(WebEncoders.Base64UrlDecode(confirm.Code));
                var result = await _userManager.ConfirmEmailAsync(user, confirm.Code);
                if (result.Succeeded)
                {
                    await _userManager.SetEmailAsync(user, confirm.NewEmail);
                    user.EmailConfirmed = true;
                    await _userManager.UpdateAsync(user);
                }
                return SuccessNodata;
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
    }
}
