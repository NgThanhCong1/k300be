﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Models.BaseModel;
using Models.RequestModels;
using Models.ResponseModels;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Ultilities;
using Ultilities.Interfaces;

namespace K300.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : BaseController
    {
        private readonly IWebHostEnvironment _host;
        private readonly ICategoryService _categoryService;
        public CategoryController(
            ICategoryService categoryService,
            IWebHostEnvironment host,
            IElastichsearchHelper elastichsearchHelper, 
            IBotTelegramHelper botTelegramHelper,
            IRedisCacheHelper redisCacheHelper
            ) : base(elastichsearchHelper, botTelegramHelper, redisCacheHelper)
        {
            _categoryService = categoryService;
            _host = host;
        }
        [AllowAnonymous]
        [HttpGet("get-all-cate")]
        public async Task<IActionResult> GetAllCate(int _page = 0, int _limit = 0, string nameLike = "")
        {
            try
            {
                var result = await _redisCacheHelper.GetAsync<IEnumerable<Category>>(SolutionsCenter.RedisKey_GetAllCategory);
                if (result == null || !result.Any())
                {
                    result = await _categoryService.GetAll();
                    if (result == null || !result.Any())
                        return NoData;
                    await _redisCacheHelper.SetAsync(SolutionsCenter.RedisKey_GetAllCategory, result, TimeSpan.FromDays(30));
                }

                var outModel = new CateViewModel()
                {
                    /**
                    nếu page = 0 thì lấy tất cả record,
                    nếu page khác 0 và nameLike bằng null thì lấy theo paging
                    nếu page khác 0 và nameLike khác null thì lấy các record contain nameLike theo paging
                     */
                    ListCate = _page == 0 
                        ? result.ToList()
                        : nameLike.Equals("null") 
                            ? result.Skip((_page - 1) * _limit).Take(_limit).ToList()
                            : result.Where(s => s.Name.ToLower().Contains(nameLike.ToLower())).Skip((_page - 1) * _limit).Take(_limit).ToList(),

                    Pagination = new Pagination()
                    {
                        _page = _page,
                        _limit = _limit,
                        _totalRows = nameLike.Equals("null") 
                            ? result.Count() 
                            : result.Count(s => s.Name.Contains(nameLike)),
                    }
                };
                if (outModel.ListCate != null && outModel.ListCate.Any())
                {
                    return Ok(new BaseResponse<CateViewModel>(outModel, 1, "Success"));
                }
                return NoData;
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [AllowAnonymous]
        [HttpGet("get-categories-by-parentCategory")]
        public async Task<IActionResult> GetCategoriesByParent()
        {
            try
            {
                var result = await _categoryService.GetAllCategoryByParentCategory();
                if (result == null || !result.Any()) 
                    return NoData;
                return Ok(new BaseResponse<IEnumerable<Category>>(result, 1, "Success"));
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [AllowAnonymous]
        [HttpGet("get-top-4-cates")]
        public async Task<IActionResult> GetTop4Cates()
        {
            try
            {
                var result = await _categoryService.GetTop4Cates();
                if (result == null || !result.Any()) return NoData;
                result.ForEach(s => s.ListImageLinks = SolutionsCenter.ConvertImagesTextToListImages($"['{s.Images}']"));
                return Ok(new BaseResponse<List<Category>>(result, 1, "Success"));
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [Authorize(Roles = "Admin")]
        [HttpPost("update-cate")]
        public async Task<IActionResult> UpdateCate([FromForm]Category cate)
        {
            try
            {
                if (cate == null) return NotEnoungh;
                if (cate.ImageFiles != null && cate.ImageFiles.Any())
                {
                    string directory = Path.Combine(_host.WebRootPath, "Images/Categories/", Regex.Replace(cate.Name, @"\s+", ""));
                    //directory = Regex.Replace(directory, @"\s+", "");
                    if (!Directory.Exists(directory))
                    {
                        Directory.CreateDirectory(directory);
                    }
                    int flag = 0;
                    string listImages = "";
                    foreach (var image in cate.ImageFiles)
                    {
                        if (image.Length > 0)
                        {
                            var fileName = cate.Id + "num" + flag.ToString();
                            var filePath = Path.Combine(directory, fileName + ".jpg");
                            //filePath = Regex.Replace(filePath, @"\s+", "");
                            using (var stream = new FileStream(filePath, FileMode.Create))
                            {
                                listImages += filePath.Replace(_host.WebRootPath, null).Replace("\\", "/") + ",";
                                await image.CopyToAsync(stream);
                            }
                        }
                        flag++;
                    }
                    cate.Images = listImages.Remove(listImages.Length - 1);
                }
                var result = await _categoryService.UpdateCate(cate);
                if(result > 0)
                {
                    await _redisCacheHelper.DeleteAsync(SolutionsCenter.RedisKey_GetAllCategory);
                    return SuccessNodata;
                }
                return Fail;
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [Authorize(Roles = "Admin")]
        [HttpPost("add-cate")]
        public async Task<IActionResult> AddCate([FromForm]Category cate)
        {
            try
            {
                if (cate == null) return NotEnoungh;
                if (cate.ImageFiles != null && cate.ImageFiles.Any())
                {
                    string directory = Path.Combine(_host.WebRootPath, "Images/Categories/", Regex.Replace(cate.Name, @"\s+", ""));
                    //directory = Regex.Replace(directory, @"\s+", "");
                    if (!Directory.Exists(directory))
                    {
                        Directory.CreateDirectory(directory);
                    }
                    int flag = 0;
                    string listImages = "";
                    foreach (var image in cate.ImageFiles)
                    {
                        if (image.Length > 0)
                        {
                            var fileName = cate.Id + "num" + flag.ToString();
                            var filePath = Path.Combine(directory, fileName + ".jpg");
                            //filePath = Regex.Replace(filePath, @"\s+", "");
                            using (var stream = new FileStream(filePath, FileMode.Create))
                            {
                                listImages += filePath.Replace(_host.WebRootPath, null).Replace("\\", "/") + ",";
                                await image.CopyToAsync(stream);
                            }
                        }
                        flag++;
                    }
                    cate.Images = listImages.Remove(listImages.Length - 1);
                }
                var result = await _categoryService.AddCate(cate);
                if (result > 0)
                {
                    await _redisCacheHelper.DeleteAsync(SolutionsCenter.RedisKey_GetAllCategory);
                    return SuccessNodata;
                }
                return Fail;
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [Authorize(Roles = "Admin")]
        [HttpPost("delete-cate")]
        public async Task<IActionResult> DeleteCate(Category cate)
        {
            try
            {
                if (cate == null) return NotEnoungh;
                cate.UpdatedBy = UserName;
                var result = await _categoryService.DeleteCate(cate);
                if (result > 0)
                {
                    //string directory = Path.Combine(_host.WebRootPath, "Images/Categories/", Regex.Replace(cate.Name, @"\s+", ""));
                    //if (Directory.Exists(directory))
                    //{
                    //    Directory.Delete(directory, true);
                    //}
                    await _redisCacheHelper.DeleteAsync(SolutionsCenter.RedisKey_GetAllCategory);
                    return SuccessNodata;
                }
                return Fail;
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [Authorize(Roles = "Admin")]
        [HttpGet("get-data-for-update-category")]
        public async Task<IActionResult> GetDataForUpdateCategory(int id)
        {
            try
            {
                if (id < 1) return NotEnoungh;
                var category = await _categoryService.GetCategoryById(id);
                var listCategories = await _categoryService.GetAllCategoryByParentCategory();

                return category == null
                    ? NoData
                    : Ok(new BaseResponse<object>(new
                    {
                        category = category,
                        listCategories = listCategories
                    }
                    , 1, "Success"));
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
    }

}
