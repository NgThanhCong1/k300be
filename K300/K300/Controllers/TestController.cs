﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Models;
using Models.BaseModel;
using Models.RequestModels;
using Models.ResponseModels;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ultilities;
using Ultilities.Interfaces;

namespace K300.Controllers
{
    //[Authorize(Roles = "Admin, Staff")]
    [Route("api/[controller]")]
    [ApiController]
    public class TestController : BaseController //: ControllerBase 
    {
        private readonly IGeneralService _generalService;
        private readonly ISendMailHelper _sendMailHelper;
        private readonly IPhoneHelper _phoneHelper;
        public TestController(
            IGeneralService generalService,
            IElastichsearchHelper elastichsearchHelper,
            IBotTelegramHelper botTelegramHelper,
            ISendMailHelper sendMailHelper,
            IRedisCacheHelper redisCacheHelper,
            IPhoneHelper phoneHelper
            ) : base(elastichsearchHelper, botTelegramHelper, redisCacheHelper)
        {
            _generalService = generalService;
            _sendMailHelper = sendMailHelper;
            _phoneHelper = phoneHelper;
        }
        [HttpGet("test")]
        public async Task<IActionResult> TestHandleError(int id)
        {
            try
            {
                //DataToGmail gmail = new DataToGmail()
                //{
                //    mailTemplate = "templateEmail.html",
                //    from = "k300nonreply@gmail.com",
                //    to = "nguyenquangkhai2319@gmail.com",
                //    subject = "Voucher From K300 to you",
                //    title = "Let enjoy my shop",
                //    mainContent = $"Discount code: 123",
                //};
                //await _sendMailHelper.SendMail(gmail);

                string key = $"test1";
                var dataCache = await _redisCacheHelper.GetAsync<string>(key);
                if (dataCache != null)
                {
                    //return Ok(new BaseResponse<string>(dataCache, 1, "Success"));
                }

                await _redisCacheHelper.SetAsync(key, "data in redis compose", TimeSpan.FromDays(1));

                Exception ex = new Exception("test new exception");
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                //await _botTelegramHelper.TelegramBotSendError(ex.Message);
                return Ok(new BaseResponse<object>(1, 1, "Success"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpGet("remove all-redis-keys")]
        public async Task<IActionResult> RemoveRedisKeys()
        {
            try
            {

                await _redisCacheHelper.DeleteAsync(SolutionsCenter.RedisKey_GetAllCategory);
                await _redisCacheHelper.DeleteAsync(SolutionsCenter.RedisKey_GetAllBrand);

                return Ok(new BaseResponse<object>(1, 1, "Success"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPost("send message")]
        public async Task<IActionResult> SendOtp()
        {
            try
            {
                String[] phones = new String[] { "0383980693" };
                String content = "0123456";
                int type = 2;
                String sender = "";

                String response = _phoneHelper.sendSMS(phones, content, type, sender);

                return Ok(new BaseResponse<object>(1, 1, "Success"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
