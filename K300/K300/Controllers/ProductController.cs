﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Models.BaseModel;
using Models.RequestModels;
using Models.ResponseModels;
using Newtonsoft.Json;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Ultilities;
using Ultilities.Interfaces;

namespace K300.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : BaseController //: ControllerBase 
    {
        private readonly IProductService _productService;
        private readonly IBrandService _brandService;
        private readonly ICategoryService _categoryService;
        private readonly IWebHostEnvironment _host;
        public ProductController(
            IProductService productService,
            IBrandService brandService,
            IElastichsearchHelper elastichsearchHelper,
            ICategoryService categoryService,
            IWebHostEnvironment host,
            IBotTelegramHelper botTelegramHelper,
            IConfiguration configuration,
            IRedisCacheHelper redisCacheHelper
            ) : base(elastichsearchHelper, botTelegramHelper, redisCacheHelper)
        {
            _categoryService = categoryService;
            _productService = productService;
            _brandService = brandService;
            _host = host;
        }
        [Authorize(Roles = "Admin, SellStaff, RepoStaff")]
        [HttpGet("get-all-product")]
        public async Task<IActionResult> GetAllProduct(int _page = 0, int _limit = 0, string nameLike = "")
        {
            try
            {
                var result = await _productService.GetAll();
                if (result == null || !result.Any()) return NoData;

                var outModel = new ProductViewModel_New()
                {
                    /**
                    nếu page = 0 thì lấy tất cả record,
                    nếu page khác 0 và nameLike bằng null thì lấy theo paging
                    nếu page khác 0 và nameLike khác null thì lấy các record contain nameLike theo paging
                     */
                    ListProducts = _page == 0 
                        ? result.ToList()
                        : nameLike.Equals("null") 
                            ? result.Skip((_page - 1) * _limit).Take(_limit).ToList()
                            : result.Where(s => s.Name.ToLower().Contains(nameLike.ToLower())).Skip((_page - 1) * _limit).Take(_limit).ToList(),

                    Pagination = new Pagination()
                    {
                        _page = _page,
                        _limit = _limit,
                        _totalRows = nameLike.Equals("null") ? result.Count() : result.Where(s => s.Name.Contains(nameLike)).ToList().Count(),
                    },
                };

                return outModel.ListProducts != null && outModel.ListProducts.Any()
                    ? Ok(new BaseResponse<ProductViewModel_New>(outModel, 1, ReturnMessages["success"]))
                    : NoData;
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [AllowAnonymous]
        [HttpPost("search-product-by-cate")]
        public async Task<IActionResult> SearchProductByCate(Category cate)
        {
            try
            {
                if(cate == null) return NotEnoungh;
                var result = await _productService.SearchProductByCate(cate);
                return result != null && result.Any() 
                    ? Ok(new BaseResponse<IEnumerable<Product>>(result, 1, "Success")) 
                    : NotEnoungh;
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        //[HttpPost("add-product-cate")]
        //public async Task<IActionResult> AddProductCate(AddProductCate productcate)
        //{
        //    try
        //    {
        //        var result = await _productService.AddProductCate(productcate);
        //        return result > 0 ? Ok(new BaseResponse<int>(1, 1, "Success")) : Ok(new BaseResponse<object>(null, -1, "Fail"));
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
        
        //[HttpGet("get-cates-by-product")]
        //public async Task<IActionResult> GetCateByProduct(int productId)
        //{
        //    try
        //    {
        //        var result = await _productService.GetListCateByProduct(productId);
        //        return result != null && result.Any() ? Ok(new BaseResponse<IEnumerable<int>>(result, 1, "Success")) : Ok(new BaseResponse<object>(null, -1, "Fail"));
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
        [AllowAnonymous]
        [HttpGet("get-newin-combine-products")]
        public async Task<IActionResult> GetNewInCombineProducts()
        {
            try
            {
                var result = await _productService.GetTop4NewInProduct();
                if (result == null || !result.Any()) return Ok(new BaseResponse<object>(null, 0, "NoData"));
                //foreach (var item in result)
                //{
                //    item.ListImagesLink = JsonConvert.DeserializeObject<List<string>>(item.ListImages);
                //}
                result.ToList().ForEach(s => s.ListImagesLink = SolutionsCenter.ConvertImagesTextToListImages(s.ListImages));
                return Ok(new BaseResponse<object>(result, 1, "Success"));
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [AllowAnonymous]
        [HttpGet("get-all-combine-products")]
        public async Task<IActionResult> GetAllCombineProducts()
        {
            try
            {
                var result = await _productService.GetAllCombineProducts();
                if (result == null || !result.Any()) return Ok(new BaseResponse<object>(null, 0, "NoData"));
                //foreach (var item in result)
                //{
                //    item.ListImagesLink = JsonConvert.DeserializeObject<List<string>>(item.ListImages);
                //}
                result.ToList().ForEach(s => s.ListImagesLink = SolutionsCenter.ConvertImagesTextToListImages(s.ListImages));
                return Ok(new BaseResponse<object>(result, 1, ReturnMessages["success"]));
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [AllowAnonymous]
        [HttpGet("get-products-by-cate")]
        public async Task<IActionResult> GetProductsByCate(string cateName, int _page = 0, int _limit = 0, int _sortType = -1, int _priceType = -1)
        {
            try
            {
                var result = await _productService.GetListProductsByCate(cateName.ToUpper());
                if (result == null || !result.Any()) return NoData;
                result = _sortType == 0 ? result.OrderByDescending(s => s.IsPopular)
                    : _sortType == 1 ? result.OrderBy(s => s.Name)
                    : _sortType == 2 ? result.OrderByDescending(s => s.Name)
                    : _sortType == 3 ? result.OrderBy(s => s.Price)
                    : _sortType == 4 ? result.OrderByDescending(s => s.Price)
                    : result;
                result = _priceType == 0 ? result.Where(s => Int32.Parse(s.Price) >= 0 && Int32.Parse(s.Price) < 300)
                    : _priceType == 1 ? result.Where(s => Int32.Parse(s.Price) >= 300 && Int32.Parse(s.Price) < 500)
                    : _priceType == 2 ? result.Where(s => Int32.Parse(s.Price) >= 500)
                    : result;

                result.ToList().ForEach(s => s.ListImagesLink = SolutionsCenter.ConvertImagesTextToListImages(s.ListImages));

                var outModel = new CombineProductViewModel()
                {
                    ListCombineProducts = _page == 0 
                        ? result.ToList()
                        : result.Skip((_page - 1) * _limit).Take(_limit).ToList(),

                    Pagination = new Pagination()
                    {
                        _page = _page,
                        _limit = _limit,
                        _totalRows = result.Count(),
                    },
                };

                return outModel.ListCombineProducts == null || !outModel.ListCombineProducts.Any()
                    ? NoData
                    : Ok(new BaseResponse<object>(outModel, 1, "Success"));
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        #region get prodcut by id
        //[AllowAnonymous]
        //[HttpGet("get-products-by-id")]
        //public async Task<IActionResult> GetProductsById(int id)
        //{
        //    try
        //    {
        //        if (id < 1) return Ok(new BaseResponse<int>(0, 0, "Nodata from request"));
        //        var result = await _productService.GetProductsById(id);
        //        if (result == null)
        //        {
        //            return Ok(new BaseResponse<object>(null, 0, "NoData"));
        //        }
        //        foreach (var item in result.ListProductDetails)
        //        {
        //            item.ListImagesLink = JsonConvert.DeserializeObject<List<string>>(item.ListImages);
        //        }
        //        foreach (var item in result.ListCombineProducts)
        //        {
        //            item.ListImagesLink = JsonConvert.DeserializeObject<List<string>>(item.ListImages);
        //        }
        //        var listColors = result.ListProductDetails.Select(s => s.Color).Distinct();
        //        var listSizes = result.ListProductDetails.Select(s => s.Size).Distinct();
        //        return Ok(new BaseResponse<object>(
        //            new
        //            {
        //                listRelatedProducts = result.ListCombineProducts,
        //                listProductDetails = result.ListProductDetails,
        //                listColors = listColors,
        //                listSizes = listSizes
        //            }
        //        , 1, "Success"));
        //    }
        //    catch (Exception ex)
        //    {
        //        await _botTelegramHelper.TelegramBotSendError(ex.Message);
        //        _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
        //        return Error;
        //    }
        //}
#endregion
        [AllowAnonymous]
        [HttpGet("get-products-by-slug")]
        public async Task<IActionResult> GetProductsBySlug(string slug, string customerId)
        {
            try
            {
                if (string.IsNullOrEmpty(slug)) return NotEnoungh;
                if (string.IsNullOrEmpty(customerId)) customerId = "unlogined";
                var result = await _productService.GetProductsBySlug(slug, customerId);
                if (result == null) return NoData;
                
                result.ListProductDetails.ForEach(s => s.ListImagesLink = SolutionsCenter.ConvertImagesTextToListImages(s.ListImages));
                result.ListCombineProducts.ForEach(s => s.ListImagesLink = SolutionsCenter.ConvertImagesTextToListImages(s.ListImages));

                var listColors = result.ListProductDetails.Select(s => s.Color).Distinct();
                var listSizes = result.ListProductDetails.Select(s => s.Size).Distinct();

                return Ok(new BaseResponse<object>(
                    new
                    {
                        listRelatedProducts = result.ListCombineProducts,
                        listProductDetails = result.ListProductDetails,
                        listColors = listColors,
                        listSizes = listSizes,
                        ratingStatic = result.RatingStatic
                    }
                , 1, "Success"));
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        #region add by ggsheet
        //[HttpPost("add-product-ggSheet")]
        //public async Task<IActionResult> AddProductGGSheet(GGSheet productSheet)
        //{
        //    try
        //    {
        //        List<Product_New> listProduct = new List<Product_New>();
        //        var ggSheetResult = await _ggSheetHelper.ReadData(productSheet.Link, productSheet.SheetName, productSheet.RangeData);
        //        if (ggSheetResult != null && ggSheetResult.Count() > 0)
        //        {
        //            Console.WriteLine("Task");
        //            foreach (var row in ggSheetResult)
        //            {
        //                listProduct.Add(new Product_New()
        //                {
        //                    Name = row[0],
        //                    Price = "000",
        //                    Description = row[1],
        //                    Material = row[2],
        //                    CreatedBy = UserName
        //                });
        //            }
        //        }
        //        else
        //        {
        //            return Ok(new BaseResponse<object>(null, 0, "Can not get data from sheet"));
        //        }
        //        var result = await _productService.AddProductGGSheet(listProduct);
        //        return
        //            result > 0 && result < listProduct.Count
        //            ? Ok(new BaseResponse<int>(result, 1, "Success, remove some duplicate"))
        //            : result > 0 && result == listProduct.Count ?
        //                Ok(new BaseResponse<int>(result, 1, "Success"))
        //            : Ok(new BaseResponse<int>(-1, -1, "Fail"));
        //    }
        //    catch (Exception ex)
        //    {
        //        return Ok(new BaseResponse<object>(null, -1, "Get error from api: " + ex.Message));
        //    }
        //}
        #endregion
        [Authorize(Roles = "Admin")]
        [HttpPost("add-product")]
        public async Task<IActionResult> AddProduct(AddProduct addProduct)
        {
            try
            {
                if (addProduct == null) return NotEnoungh;
                addProduct.productNew.Slug = SolutionsCenter.RemoveSign4VietnameseString(addProduct.productNew.Name.Replace(" ", "-"));

                var result = await _productService.AddProduct(addProduct);
                return result > 0
                    ? Ok(new BaseResponse<int>(result, 1, "Acction success"))
                    : Duplicate;
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [Authorize(Roles = "Admin")]
        [HttpGet("get-data-for-add-product")]
        public async Task<IActionResult> GetDataForAddProduct()
        {
            try
            {
                var listCategories = await _categoryService.GetAll();
                var listBrands = await _brandService.GetAll();
                return listCategories == null
                    || !listCategories.Any()
                    || listBrands == null
                    || !listBrands.Any()
                    ? NoData
                    : Ok(new BaseResponse<DataForAddProduct>(new DataForAddProduct()
                    {
                        ListCategories = listCategories.Where(s => s.ParentCategory != 0).ToList(),
                        ListBrands = listBrands.ToList()
                    }, 1, "Success"));
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        //[Authorize(Roles = "Admin, SellStaff, RepoStaff")]
        [AllowAnonymous]
        [HttpGet("get-data-for-update-product")]
        public async Task<IActionResult> GetDataForUpdateProduct(int id)
        {
            try
            {
                if (id < 1) return NotEnoungh;
                var listCategoriesOfProduct = await _productService.GetListCateByProduct(id);
                var product = await _productService.GetProductsByIdAdmin(id);
                var listCategories = await _categoryService.GetAll();
                var listBrands = await _brandService.GetAll();

                return listCategories == null
                    || !listCategories.Any()
                    || listBrands == null
                    || !listBrands.Any()
                    ? NoData
                    : Ok(new BaseResponse<object>(new
                    {
                        Product = product.Product_New,
                        ListSubProducts = product.ListSubProducts,
                        ListCategoriesOfProduct = listCategoriesOfProduct,
                        ListCategories = listCategories.Where(s => s.ParentCategory != 0).ToList(),
                        ListBrands = listBrands.ToList()
                    }, 1, "Success"));
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [Authorize(Roles = "Admin")]
        [HttpPost("update-product")]
        public async Task<IActionResult> UpdateProduct(AddProduct addProduct) //Model update = add 
        {
            try
            {
                if (addProduct == null) return NotEnoungh;
                var result = await _productService.UpdateProduct(addProduct);
                if (result > 0)
                {
                    if(addProduct.ListSubProducts != null || addProduct.ListSubProducts.Any())
                    {
                        addProduct.ListSubProducts.ForEach(s =>
                        {
                            string directory = Path.Combine(_host.WebRootPath, $"Images/Products", s.ProductCode);
                            if (Directory.Exists(directory))
                            {
                                Directory.Delete(directory, true);
                            }
                        });
                    }
                    return SuccessNodata;
                }
                return Fail;
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [Authorize(Roles = "Admin")]
        [HttpPost("delete-product")]
        public async Task<IActionResult> DeleteProduct(Product_New deleteProduct)
        {
            try
            {
                if (deleteProduct.Id < 1) return NotEnoungh;
                Product_New product = new Product_New()
                {
                    Id = deleteProduct.Id,
                    UpdatedBy = UserName
                };
                var result = await _productService.DeleteProduct(product);
                return result > 0 
                    ? SuccessNodata 
                    : Fail;
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [Authorize(Roles = "Admin")]
        [HttpPost("add-subProduct")]
        public async Task<IActionResult> AddSubProduct([FromForm]SubProduct subProduct)
        {
            try
            {
                if (subProduct == null) return NotEnoungh;
                if (subProduct.ListImageFiles != null && subProduct.ListImageFiles.Any())
                {
                    string directory = Path.Combine(_host.WebRootPath, $"Images/Products", subProduct.ProductCode);
                    //directory = Regex.Replace(directory, @"\s+", "");
                    if (!Directory.Exists(directory))
                    {
                        Directory.CreateDirectory(directory);
                    }
                    int flag = 0;
                    foreach (var image in subProduct.ListImageFiles)
                    {
                        if (image.Length > 0)
                        {
                            var fileName = subProduct.ProductCode + flag.ToString();
                            var filePath = Path.Combine(directory, fileName + ".jpg");
                            //filePath = Regex.Replace(filePath, @"\s+", "");
                            using (var stream = new FileStream(filePath, FileMode.Create))
                            {
                                subProduct.ListImagesLink.Add(filePath.Replace(_host.WebRootPath, null).Replace("\\", "/"));
                                await image.CopyToAsync(stream);
                            }
                        }
                        flag++;
                    }
                    subProduct.ListImages = JsonConvert.SerializeObject(subProduct.ListImagesLink);
                }
                subProduct.CreatedBy = UserName;
                var result = await _productService.AddSubProduct(subProduct);
                return result > 0 
                    ? SuccessNodata 
                    : Duplicate;
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [Authorize(Roles = "Admin")]
        [HttpGet("get-all-subProduct")]
        public async Task<IActionResult> GetAllSubProduct(int _page = 0, int _limit = 0, string productCode = "")
        {
            try
            {
                var result = await _productService.GetAllSubProduct();
                if (result == null || !result.Any()) return NoData;

                var outModel = new SubProductViewModel()
                {
                    ListSubProducts = _page == 0 
                        ? result.ToList()
                        : productCode.Equals("null") 
                            ? result.Skip((_page - 1) * _limit).Take(_limit).ToList()
                            : result.Where(s => s.ProductCode.ToLower().Contains(productCode.ToLower())).Skip((_page - 1) * _limit).Take(_limit).ToList(),
                    Pagination = new Pagination()
                    {
                        _page = _page,
                        _limit = _limit,
                        _totalRows = productCode.Equals("null") 
                            ? result.Count() 
                            : result.Count(s => s.ProductCode.Contains(productCode)),
                    }
                };

                outModel.ListSubProducts.ForEach(s => s.ListImagesLink = SolutionsCenter.ConvertImagesTextToListImages(s.ListImages));

                return outModel.ListSubProducts == null || !outModel.ListSubProducts.Any()
                    ? NoData
                    : Ok(new BaseResponse<SubProductViewModel>(outModel, 1, ReturnMessages["success"]));
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [Authorize(Roles = "Admin")]
        [HttpGet("get-data-for-update-subProduct")]
        public async Task<IActionResult> GetDataFromUpdateSubProduct(string productCode)
        {
            try
            {
                if (string.IsNullOrEmpty(productCode)) 
                    return NotEnoungh;
                var result = await _productService.GetSubProductByProductCode(productCode);
                if (result == null)
                      return NoData;

                result.ListImagesLink = SolutionsCenter.ConvertImagesTextToListImages(result.ListImages);

                return Ok(new BaseResponse<SubProduct>(result, 1, ReturnMessages["success"]));
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Ok(new BaseResponse<int>(-1, -1, ReturnMessages["error"]));
            }
        }
        [Authorize(Roles = "Admin")]
        [HttpPost("update-subProduct")]
        public async Task<IActionResult> UpdateSubProduct([FromForm] SubProduct subProduct)
        {
            try
            {
                if (subProduct == null)
                    return NotEnoungh;
                if (subProduct.ListImageFiles != null && subProduct.ListImageFiles.Any())
                {
                    string directory = Path.Combine(_host.WebRootPath, $"Images/Products", subProduct.ProductCode);
                    //directory = Regex.Replace(directory, @"\s+", "");
                    if (!Directory.Exists(directory))
                    {
                        Directory.CreateDirectory(directory);
                    }
                    int flag = 0;
                    foreach (var image in subProduct.ListImageFiles)
                    {
                        if (image.Length > 0)
                        {
                            var fileName = subProduct.ProductCode + flag.ToString();
                            var filePath = Path.Combine(directory, fileName + ".jpg");
                            //filePath = Regex.Replace(filePath, @"\s+", "");
                            using (var stream = new FileStream(filePath, FileMode.Create))
                            {
                                subProduct.ListImagesLink.Add(filePath.Replace(_host.WebRootPath, null).Replace("\\", "/"));
                                await image.CopyToAsync(stream);
                            }
                        }
                        flag++;
                    }
                    subProduct.ListImages = JsonConvert.SerializeObject(subProduct.ListImagesLink);
                }
                subProduct.UpdatedBy = UserName;
                var result = await _productService.UpdateSubProduct(subProduct);
                return result > 0 
                    ? SuccessNodata
                    : Fail;
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [Authorize(Roles = "Admin")]
        [HttpPost("delete-subProduct")]
        public async Task<IActionResult> DeleteSubProduct([FromBody]string productCode)
        {
            try
            {
                if (string.IsNullOrEmpty(productCode))
                    return NotEnoungh;
                var result = await _productService.DeleteSubProduct(productCode, UserName);
                if(result > 0)
                {
                    string directory = Path.Combine(_host.WebRootPath, $"Images/Products", productCode);
                    if (Directory.Exists(directory))
                    {
                        Directory.Delete(directory, true);
                    }
                    return SuccessNodata;
                }
                return Fail;
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [AllowAnonymous]
        [HttpGet("search-products-by-customer")]
        public async Task<IActionResult> SearchProductsByNameOrCategory(int _page, int _limit, string categoryName, string productName)
        {
            try
            {
                var result = await _productService.SearchProductsByNameOrCategory(categoryName, productName);
                var tmpList = result.Skip((_page - 1) * _limit).Take(_limit).ToList();
                tmpList.ForEach(x => x.ListImagesLink = SolutionsCenter.ConvertImagesTextToListImages(x.ListImages));
                return tmpList != null && tmpList.Any()
                    ? Ok(new BaseResponse<CombineProductViewModel>(new CombineProductViewModel()
                    {
                        ListCombineProducts = tmpList,
                        Pagination = new Pagination()
                        {
                            _page = _page,
                            _limit = _limit,
                            _totalRows = result.Count(),
                        }
                    }, 1, "success"))
                    : NoData;
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [AllowAnonymous]
        [HttpGet("get-banned-words")]
        public async Task<IActionResult> GetBannedWords()
        {
            try
            {
                string text = System.IO.File.ReadAllText(Path.Combine(_host.WebRootPath, "banned-words.txt"));
                if (string.IsNullOrEmpty(text))
                    return NoData;
                return Ok(new BaseResponse<string>(text, 1, ReturnMessages["success"]));
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [Authorize(Roles = "Customer")]
        [HttpPost("add-rating")]
        public async Task<IActionResult> AddRating(Rating rating)
        {
            try
            {
                if (rating == null)
                    return NotEnoungh;
                var result = await _productService.AddRating(rating);
                return result == 1 
                    ? Ok(new BaseResponse<string>("you rated this product, thanks you <3", 1, ReturnMessages["success"]))
                    : Fail;
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [HttpPost("add-list-product-to-cart")]
        public async Task<IActionResult> AddProductsToCart(ShoppingCart cart)
        {
            try
            {
                if(cart.Type == 1)
                {
                    cart.Key = UserId;
                }
                string key = $"ProductsInCart_{cart.Key}";
                var dataCache = await _redisCacheHelper.GetAsync<List<ProductInCart>>(key);
                var tmpListProduct = new List<ProductInCart>();
                if (dataCache == null) 
                    dataCache = cart.listProductsInCart;
                else
                {
                    dataCache.ForEach(pro =>
                    {
                        cart.listProductsInCart.ForEach(newPro =>
                        {
                            if (pro.ProductCode == newPro.ProductCode)
                            {
                                pro.Quantity += newPro.Quantity;
                            }else
                            {
                                tmpListProduct.Add(newPro);
                            }
                        });
                    });
                }
                dataCache.AddRange(tmpListProduct);
                await _redisCacheHelper.SetAsync(key, dataCache, TimeSpan.FromDays(cart.Type == 1 ? 1000 : 10));
                return Ok(new BaseResponse<object>(dataCache, 1, "success"));
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [AllowAnonymous]
        [HttpPost("add-product-to-cart")]
        public async Task<IActionResult> AddProductToCart(ShoppingCart cart)
        {
            try
            {
                if (cart.Type == 1)
                {
                    cart.Key = UserId;
                }
                string key = $"ProductsInCart_{cart.Key}";
                var dataCache = await _redisCacheHelper.GetAsync<List<ProductInCart>>(key);


                if (dataCache != null && dataCache.Any() && dataCache.Exists(pro => pro.ProductCode == cart.listProductsInCart[0].ProductCode))
                {
                    foreach (var pro in dataCache)
                    {
                        if(pro.ProductCode == cart.listProductsInCart[0].ProductCode)
                        {
                            pro.CartQuantity += cart.listProductsInCart[0].CartQuantity;
                            await _redisCacheHelper.SetAsync(key, dataCache, TimeSpan.FromDays(cart.Type == 1 ? 1000 : 10));
                            return Ok(new BaseResponse<object>(dataCache, 1, "success"));
                        }
                    }
                    
                }else if (dataCache != null && dataCache.Any() && !dataCache.Exists(pro => pro.ProductCode == cart.listProductsInCart[0].ProductCode))
                {
                    dataCache.Add(cart.listProductsInCart[0]);
                    await _redisCacheHelper.SetAsync(key, dataCache, TimeSpan.FromDays(cart.Type == 1 ? 1000 : 10));
                    return Ok(new BaseResponse<object>(dataCache, 1, "success"));
                }

                dataCache = new List<ProductInCart>();
                dataCache.Add(cart.listProductsInCart[0]);
                await _redisCacheHelper.SetAsync(key, dataCache, TimeSpan.FromDays(cart.Type == 1 ? 1000 : 10));
                return Ok(new BaseResponse<object>(dataCache, 1, "success"));
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [AllowAnonymous]
        [HttpPost("remove-product-in-cart")]
        public async Task<IActionResult> RemoveProductInCart(ShoppingCart cart)
        {
            try
            {
                if (cart.Type == 1)
                {
                    cart.Key = UserId;
                }
                string key = $"ProductsInCart_{cart.Key}";
                var dataCache = await _redisCacheHelper.GetAsync<List<ProductInCart>>(key);
                if(dataCache != null && dataCache.Any())
                {
                    var removeProduct = dataCache.FirstOrDefault(p => p.ProductCode == cart.listProductsInCart[0].ProductCode);
                    dataCache.Remove(removeProduct);
                    await _redisCacheHelper.SetAsync(key, dataCache, TimeSpan.FromDays(cart.Type == 1 ? 1000 : 10));
                    return Ok(new BaseResponse<object>(dataCache, 1, "success"));
                }
                return NoData;
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [AllowAnonymous]
        [HttpPost("get-products-in-cart")]
        public async Task<IActionResult> GetProductsInCart(ShoppingCart cart)
        {
            try
            {
                if (cart.Type == 1)
                {
                    cart.Key = UserId;
                }
                string key = $"ProductsInCart_{cart.Key}";
                var dataCache = await _redisCacheHelper.GetAsync<List<ProductInCart>>(key);
                if (dataCache != null && dataCache.Any())
                {
                    return Ok(new BaseResponse<object>(dataCache, 1, "success"));
                }
                return Ok(new BaseResponse<object>(new Array[] { }, 1, "success"));
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [AllowAnonymous]
        [HttpPost("set-check-product-in-cart")]
        public async Task<IActionResult> SetCheckProductToCart(ShoppingCart cart)
        {
            try
            {
                if (cart.Type == 1)
                {
                    cart.Key = UserId;
                }
                string key = $"ProductsInCart_{cart.Key}";
                var dataCache = await _redisCacheHelper.GetAsync<List<ProductInCart>>(key);


                if (dataCache != null && dataCache.Any() && dataCache.Exists(pro => pro.ProductCode == cart.listProductsInCart[0].ProductCode))
                {
                    foreach (var pro in dataCache)
                    {
                        if (pro.ProductCode == cart.listProductsInCart[0].ProductCode)
                        {
                            pro.IsChecked = pro.IsChecked == 1 ? 0 : 1;
                            await _redisCacheHelper.SetAsync(key, dataCache, TimeSpan.FromDays(cart.Type == 1 ? 1000 : 10));
                            return Ok(new BaseResponse<object>(dataCache, 1, "success"));
                        }
                    }

                }
                return Ok(new BaseResponse<object>(new Array[] { }, 1, "success"));
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [AllowAnonymous]
        [HttpPost("merge-products-in-cart")]
        public async Task<IActionResult> MergeProductsInCart(ShoppingCart cart)
        {
            try
            {
                string cookieKey = $"ProductsInCart_{cart.Key}";
                string userIdKey = $"ProductsInCart_{cart.UserIdKey}";
                var dataCacheCookieKey = await _redisCacheHelper.GetAsync<List<ProductInCart>>(cookieKey);
                var dataCacheUserIdKey = await _redisCacheHelper.GetAsync<List<ProductInCart>>(userIdKey);
                var tmpDataCacheUserIdKey = new List<ProductInCart>();

                if (dataCacheUserIdKey != null && dataCacheUserIdKey.Any())
                {
                    if(dataCacheCookieKey != null && dataCacheCookieKey.Any())
                    {
                        foreach(var proCookie in dataCacheCookieKey)
                        {
                            if(!dataCacheUserIdKey.Exists(s => s.ProductCode == proCookie.ProductCode))
                            {
                                tmpDataCacheUserIdKey.Add(proCookie);
                            }
                        }
                        dataCacheUserIdKey.AddRange(tmpDataCacheUserIdKey);
                    }

                    await _redisCacheHelper.SetAsync(userIdKey, dataCacheUserIdKey, TimeSpan.FromDays(cart.Type == 1 ? 1000 : 10));
                    dataCacheCookieKey = new List<ProductInCart>();
                    await _redisCacheHelper.SetAsync(cookieKey, dataCacheCookieKey, TimeSpan.FromDays(cart.Type == 1 ? 1000 : 10));
                    return Ok(new BaseResponse<object>(dataCacheUserIdKey, 1, "success"));
                }
                else
                {
                    dataCacheUserIdKey = new List<ProductInCart>();
                    if (dataCacheCookieKey != null && dataCacheCookieKey.Any())
                    {
                        dataCacheUserIdKey.AddRange(dataCacheCookieKey);
                    }
                    await _redisCacheHelper.SetAsync(userIdKey, dataCacheUserIdKey, TimeSpan.FromDays(cart.Type == 1 ? 1000 : 10));
                    dataCacheCookieKey = new List<ProductInCart>();
                    await _redisCacheHelper.SetAsync(cookieKey, dataCacheCookieKey, TimeSpan.FromDays(cart.Type == 1 ? 1000 : 10));
                    return Ok(new BaseResponse<object>(dataCacheUserIdKey, 1, "success"));
                }
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [AllowAnonymous]
        [HttpPost("delete-lists-products-in-cart")]
        public async Task<IActionResult> DeleteProductsInCart(ShoppingCart cart)
        {
            try
            {
                cart.Key = UserId;
                string key = $"ProductsInCart_{cart.Key}";
                var dataCache = await _redisCacheHelper.GetAsync<List<ProductInCart>>(key);

                if (dataCache != null && dataCache.Any())
                {
                    //var tmpDataCache = dataCache;
                    //foreach(var item in dataCache)
                    //{
                    //    if(cart.listProductsInCart.Exists(s => s.ProductCode == item.ProductCode))
                    //    {
                    //        tmpDataCache.Remove(item);
                    //    }
                    //}

                    dataCache.RemoveAll(item => cart.listProductsInCart.Any(s => s.ProductCode == item.ProductCode));

                    await _redisCacheHelper.SetAsync(key, dataCache, TimeSpan.FromDays(cart.Type == 1 ? 1000 : 10));
                    return Ok(new BaseResponse<object>(dataCache, 1, "success"));
                }
                return Ok(new BaseResponse<object>(new Array[] { }, 1, "success"));
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
    }
}
