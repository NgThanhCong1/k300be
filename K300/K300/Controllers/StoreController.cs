﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Models.BaseModel;
using Models.RequestModels;
using Models.ResponseModels;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Ultilities.Interfaces;

namespace K300.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class StoreController : BaseController
    {
        private readonly IWebHostEnvironment _host;
        private readonly IStoreService _storeService;
        public StoreController(
            IStoreService storeService,
            IWebHostEnvironment host,
            IElastichsearchHelper elastichsearchHelper, 
            IBotTelegramHelper botTelegramHelper,
            IRedisCacheHelper redisCacheHelper
            ) : base(elastichsearchHelper, botTelegramHelper, redisCacheHelper)
        {
            _storeService = storeService;
            _host = host;
        }
        [AllowAnonymous]
        [HttpGet("get-all-store")]
        public async Task<IActionResult> GetAllStore(int _page = 0, int _limit = 0)
        {
            try
            {
                var result = await _storeService.GetAllStore();
                if (result == null || !result.Any()) return NoData;

                dynamic outModel = new
                {
                    ListStores = _page == 0 
                        ? result.ToList()
                        : result.Skip((_page - 1) * _limit).Take(_limit).ToList(),
                    Pagination = new Pagination()
                    {
                        _page = _page,
                        _limit = _limit,
                        _totalRows = result.Count(),
                    }
                };
                return Ok(new BaseResponse<dynamic>(outModel, 1, "Success"));
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [Authorize(Roles = "Admin")]
        [HttpPost("update-store")]
        public async Task<IActionResult> UpdateStore([FromForm]Store store)
        {
            try
            {
                if (store == null)
                    return NotEnoungh;
                if (store.ImageFiles != null && store.ImageFiles.Any())
                {
                    string directory = Path.Combine(_host.WebRootPath, "Images/Stores/", store.Address);
                    directory = Regex.Replace(directory, @"\s+", "");
                    if (!Directory.Exists(directory))
                    {
                        Directory.CreateDirectory(directory);
                    }
                    int flag = 0;
                    string listImages = "";
                    foreach (var image in store.ImageFiles)
                    {
                        if (image.Length > 0)
                        {
                            var fileName = store.Id + "num" + flag.ToString();
                            var filePath = Path.Combine(directory, fileName + ".jpg");
                            filePath = Regex.Replace(filePath, @"\s+", "");
                            using (var stream = new FileStream(filePath, FileMode.Create))
                            {
                                listImages += filePath.Replace(_host.WebRootPath, null).Replace("\\", "/") + ",";
                                await image.CopyToAsync(stream);
                            }
                        }
                        flag++;
                    }
                    store.Image = listImages.Remove(listImages.Length - 1);
                }
                var result = await _storeService.UpdateStore(store);
                return result > 0
                    ? SuccessNodata
                    : Fail;
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Ok(new BaseResponse<int>(-1, -1, "Error"));
            }
        }
        [Authorize(Roles = "Admin")]
        [HttpPost("add-store")]
        public async Task<IActionResult> AddStore([FromForm]Store store)
        {
            try
            {
                if (store == null) return NotEnoungh;
                if (store.ImageFiles != null && store.ImageFiles.Any())
                {
                    string directory = Path.Combine(_host.WebRootPath, "Images/Stores/", store.Address);
                    directory = Regex.Replace(directory, @"\s+", "");
                    if (!Directory.Exists(directory))
                    {
                        Directory.CreateDirectory(directory);
                    }
                    int flag = 0;
                    string listImages = "";
                    foreach (var image in store.ImageFiles)
                    {
                        if (image.Length > 0)
                        {
                            var fileName = store.Id + "num" + flag.ToString();
                            var filePath = Path.Combine(directory, fileName + ".jpg");
                            filePath = Regex.Replace(filePath, @"\s+", "");
                            using (var stream = new FileStream(filePath, FileMode.Create))
                            {
                                listImages += filePath.Replace(_host.WebRootPath, null).Replace("\\", "/") + ",";
                                await image.CopyToAsync(stream);
                            }
                        }
                        flag++;
                    }
                    store.Image = listImages.Remove(listImages.Length - 1);
                }

                var result = await _storeService.AddStore(store);
                return result > 0 
                    ? SuccessNodata 
                    : Duplicate;
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [Authorize(Roles = "Admin")]
        [HttpPost("delete-store")]
        public async Task<IActionResult> DeleteStore(Store store)
        {
            try
            {
                if (store == null)
                    return NotEnoungh;
                store.UpdatedBy = UserName;
                var result = await _storeService.DeleteStore(store);
                return result > 0
                    ? SuccessNodata
                    : Fail;
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [Authorize(Roles = "Admin")]
        [HttpGet("get-data-for-update-store")]
        public async Task<IActionResult> GetDataForUpdateCategory(int id)
        {
            try
            {
                if (id < 1)
                    return NotEnoungh;
                var result = await _storeService.GetStoreById(id);
                if(result != null)
                    return Ok(new BaseResponse<Store>(result, 1, "Success"));
                return Ok(new BaseResponse<int>(0, 0, "Nodata"));
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
    }

}
