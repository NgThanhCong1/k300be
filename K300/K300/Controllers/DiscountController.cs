﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Models.BaseModel;
using Models.RequestModels;
using Models.ResponseModels;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ultilities.Interfaces;

namespace K300.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class DiscountController : BaseController
    {
        private readonly IDiscountService _discountService;
        public DiscountController(
            IDiscountService discountService,
            IElastichsearchHelper elastichsearchHelper,
            IBotTelegramHelper botTelegramHelper,
            IRedisCacheHelper redisCacheHelper
            ) : base(elastichsearchHelper, botTelegramHelper, redisCacheHelper)
        {
            _discountService = discountService;
        }
        [Authorize(Roles = "Admin, SellStaff, RepoStaff")]
        [HttpGet("get-all-discount")]
        public async Task<IActionResult> GetAllDiscount(int _page = 0, int _limit = 0, string nameLike = "")
        {
            try
            {
                var result = await _discountService.GetAll();
                if (result == null || !result.Any()) return NoData;

                var outModel = new DiscountViewModel()
                {
                    /**
                    nếu page = 0 thì lấy tất cả record,
                    nếu page khác 0 và nameLike bằng null thì lấy theo paging
                    nếu page khác 0 và nameLike khác null thì lấy các record contain nameLike theo paging
                     */
                    ListDiscount = _page == 0 
                        ? result.ToList()
                        : nameLike.Equals("null") 
                            ? result.Skip((_page - 1) * _limit).Take(_limit).ToList()
                            :result.Where(s => s.DiscountCode.ToLower().Contains(nameLike.ToLower())).Skip((_page - 1) * _limit).Take(_limit).ToList(),

                    Pagination = new Pagination()
                    {
                        _page = _page,
                        _limit = _limit,
                        _totalRows = nameLike.Equals("null") 
                            ? result.Count() 
                            : result.Where(s => s.DiscountCode.Contains(nameLike)).ToList().Count(),
                    }
                };
                return outModel.ListDiscount == null || !outModel.ListDiscount.Any()
                    ? NoData
                    : Ok(new BaseResponse<DiscountViewModel>(outModel, 1, "Success"));
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [Authorize(Roles = "Admin")]
        [HttpPost("update-discount")]
        public async Task<IActionResult> UpdateDiscount(Discount discount)
        {
            try
            {
                if (discount == null) return NotEnoungh;
                var result = await _discountService.UpdateDiscount(discount);
                return result > 0
                    ? SuccessNodata
                    : Duplicate;
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [Authorize(Roles = "Admin")]
        [HttpPost("add-discount")]
        public async Task<IActionResult> AddDiscount(Discount discount)
        {
            try
            {
                if (discount == null) return NotEnoungh;
                discount.CreatedBy = UserName;
                var result = await _discountService.AddDiscount(discount);
                return result > 0
                    ? SuccessNodata
                    : Duplicate;
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        #region add by ggsheet
        //[HttpPost("add-discounts-by-ggSheet")]
        //public async Task<IActionResult> AddDiscountsByGGSheet(GGSheet ggSheet)
        //{
        //    try
        //    {
        //        bool flagCheck = false;
        //        var allDiscounts = await _discountService.GetAll();

        //        List<Discount> listDiscount = new List<Discount>();
        //        var ggSheetResult = await _ggSheetHelper.ReadData(ggSheet.Link, ggSheet.SheetName, ggSheet.RangeData);
        //        if (ggSheetResult != null && ggSheetResult.Count() > 0)
        //        {
        //            Console.WriteLine("Task");
        //            foreach (var row in ggSheetResult)
        //            {
        //                if (allDiscounts.Any(s => s.DiscountCode.ToLower() == row[0].ToLower()))
        //                {
        //                    flagCheck = true;
        //                    continue;
        //                }
        //                listDiscount.Add(new Discount()
        //                {
        //                    DiscountCode = row[0],
        //                    Price = row[1],
        //                    Quantity = Int32.Parse(row[2]),
        //                    CreatedBy = UserName
        //                });
        //            }
        //        }
        //        else
        //        {
        //            Console.WriteLine("No data found.");
        //        }
        //        var importResult = await _discountService.AddListDiscount(listDiscount);
        //        return importResult > 0 ?
        //         flagCheck == false ? Ok(new BaseResponse<int>(importResult, 1, "Success"))
        //            : Ok(new BaseResponse<int>(importResult, 2, "Success, Skip some duplicate"))
        //            : Ok(new BaseResponse<int>(importResult, -1, "Import fail, may be full duplicate"));
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
        #endregion
        [Authorize(Roles = "Admin")]
        [HttpPost("delete-discount")]
        public async Task<IActionResult> DeleteDiscount(Discount discount)
        {
            try
            {
                if (discount == null) return NotEnoungh;
                discount.UpdatedBy = UserName;
                var result = await _discountService.DeleteDiscount(discount);
                return result > 0
                    ? SuccessNodata
                    : Fail;
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [Authorize(Roles = "Customer")]
        [HttpGet("search-discount")]
        public async Task<IActionResult> SearchDiscount(string discountCode)
        {
            try
            {
                if (string.IsNullOrEmpty(discountCode))return NotEnoungh;
                var result = await _discountService.SearchDiscount(discountCode);
                return result != null 
                    ? Ok(new BaseResponse<Discount>(result, 1, "Success")) 
                    : NoData;
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
        [Authorize(Roles = "Admin")]
        [HttpGet("get-data-for-update-discount")]
        public async Task<IActionResult> GetDataForUpdateDiscount(int id)
        {
            try
            {
                if (id < 1) return NotEnoungh;
                var result = await _discountService.GetDiscountById(id);
                return result != null 
                    ? Ok(new BaseResponse<Discount>(result, 1, "Success")) 
                    : NoData;
            }
            catch (Exception ex)
            {
                await _botTelegramHelper.TelegramBotSendError(ex.Message);
                await _elastichsearchHelper.UpErrorToElasticSeearch(ex, new { });
                return Error;
            }
        }
    }
}
