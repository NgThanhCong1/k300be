﻿using Microsoft.Extensions.Configuration;
using Models.BaseModel;
using Quartz;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Ultilities.Interfaces;

namespace K300.Jobs
{
    public class StatisticsByMonthJob : IJob
    {
        private readonly IGeneralService _generalService;
        private readonly ISendMailHelper _mailHelper;
        private readonly IConfiguration _configuration;

        public StatisticsByMonthJob(
            IGeneralService generalService,
            ISendMailHelper mailHelper,
            IConfiguration configuration
            )
        {
            _generalService = generalService;
            _mailHelper = mailHelper;
            _configuration = configuration;
        }
        public async Task Execute(IJobExecutionContext context)
        {
            try
            {
                Console.WriteLine("Job Static run at: " + DateTime.Now.Second);
                //return null;
                var dataStatic = await _generalService.GetDataStaticByMonth();
                var excelPath = await _generalService.ExportStaticByMonthToExcel(dataStatic);
                if (excelPath != String.Empty)
                {
                    DataToGmail gmail = new DataToGmail()
                    {
                        mailTemplate = $"templateEmail.html",
                        from = _configuration["EmailConfig:from"],
                        to = _configuration["EmailConfig:adminEmailAddress"],
                        subject = "Static month",
                        title = $"Static month {DateTime.Now.Month}",
                        mainContent = $"K300 system dend you static month {DateTime.Now.Month}",
                        path = excelPath
                    };
                    await _mailHelper.SendMail(gmail);
                }
                Console.WriteLine("Job Static complete at: " + DateTime.Now.Second);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
