﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace K300.Jobs
{
    public class JobSchedulerConfig
    {
        public readonly IScheduler _scheduler;
        public JobSchedulerConfig(IScheduler scheduler)
        {
            _scheduler = scheduler;
        }
        public void InitializeJob()
        {
            StaticJob();
        }
        public void StaticJob()
        {
            IJobDetail job = JobBuilder.Create<StatisticsByMonthJob>()
                .WithIdentity($"K300 static", "d")
                .Build();
            ITrigger trigger = TriggerBuilder.Create()
                .WithIdentity($"K300 static trigger", "d")
                .WithCronSchedule("0 0 12 L * ?")
                //.WithCronSchedule("0 */2 * ? * *")
                .StartNow()
                .Build();
            _scheduler.ScheduleJob(job, trigger);
        }
    }
}
