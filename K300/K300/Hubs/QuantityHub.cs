﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.SignalR;
using Models.RequestModels;
using Models.ResponseModels;
using Newtonsoft.Json;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace K300.Hubs
{
    public class QuantityHub : Hub
    {
        private IProductService _productService;
        public UserManager<ApplicationUser> _userManager;
        private IOutportService _outportService;
        //private HubConnectionContext _connection = new HubConnectionContext(;
        public QuantityHub(IProductService productService,
            IOutportService outportService,
            UserManager<ApplicationUser> userManager)
        {
            _productService = productService;
            _outportService = outportService;
            _userManager = userManager;
            //_emailBasedUserIdProvider = emailBasedUserIdProvider;
        }
        //[Authorize]
        public async Task UpdateQuantity(string userEmail, string message)
        {
            try
            {
                await Clients.All.SendAsync("ReceiveMessage", "asdsa");
            }
            catch
            {

            }
        }
        //public async Task UpdateOutInvoiceData(OutInvoiceRequest outInvoice)
        //{
        //    try
        //    {
        //        var currentHistory = new List<OutInvoiceRequest>();
        //        if (outInvoice.HistoryChanges != null)
        //        {
        //            currentHistory = JsonConvert.DeserializeObject<List<OutInvoiceRequest>>(outInvoice.HistoryChanges);
        //        }
        //        currentHistory.Add(outInvoice);
        //        outInvoice.HistoryChanges = JsonConvert.SerializeObject(currentHistory);
        //        var result = await _outportService.UpdateOutInvoiceData(outInvoice);
        //        await Clients.All.SendAsync("ReceiveMessage", outInvoice.Status + 1);
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //}
    }
}
