﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.Identity;
using Models.ResponseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Services.Interfaces;
using Ultilities.Interfaces;

namespace K300.Hubs
{
    public class NotifyHub : Hub
    {
        private INotifyService _notifyService;
        private readonly IRedisCacheHelper _redisService;

        public NotifyHub(INotifyService notifyService,
            IRedisCacheHelper redisService)
        {
            _notifyService = notifyService;
            _redisService = redisService;
        }
        public async Task UpdateListNotify(string customerId)
        {
            try
            {
                string key = $"Notify_{customerId}";
                var dataCache = await _redisService.GetAsync<List<Notify>>(key);
                if(dataCache != null) dataCache.Reverse();
                await Clients.All.SendAsync("ReceiveMessage", dataCache);
            }
            catch
            {

            }
        }
    }
}
