﻿using Microsoft.AspNetCore.SignalR;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace K300.Hubs
{
    public class CommentHub : Hub
    {
        private ICommentService _commentService; 
        public CommentHub(ICommentService commentService)
        {
            _commentService = commentService;
        }
        public async Task UpdateListComment(int productId)
        {
            try
            {
                await Clients.All.SendAsync("ReceiveMessage", await _commentService.GetCommentByProductId(productId));
            }
            catch
            {

            }
        }
    }
}
