﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.Identity;
using Models.ResponseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Services.Interfaces;
using Ultilities.Interfaces;

namespace K300.Hubs
{
    public class ChatHub : Hub
    {
        private readonly IRedisCacheHelper _redisService;

        public ChatHub(IRedisCacheHelper redisService)
        {
            _redisService = redisService;
        }
        public async Task UpdateListDirectChat()
        {
            try
            {
                string key = $"Direct_chat";
                var dataCache = await _redisService.GetAsync<List<DirectChat>>(key);
                if(dataCache != null || dataCache.Any())
                {
                    dataCache.Reverse();
                    await Clients.All.SendAsync("ReceiveMessage", dataCache);
                }
            }
            catch
            {

            }
        }
    }
}
