﻿using K300.Hubs;
using K300.Jobs;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http.Connections;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Models.BaseModel;
using Models.ResponseModels;
using Nest;
using Quartz;
using Quartz.Impl;
using Services.Implementions;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Threading.Tasks;
using Ultilities.Implementions;
using Ultilities.Interfaces;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using Microsoft.AspNetCore.SignalR;
using Quartz.Spi;

namespace K300
{
    public class Startup
    {
        readonly string _myCors = "cors1";
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            string connectstring = Configuration.GetConnectionString(Configuration["SqlServer"]);
            services.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseSqlServer(connectstring, b => b.MigrationsAssembly("K300"));
            });

            services.AddSingleton(connectstring);

            services.AddHttpContextAccessor();
            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();
            services.Configure<IdentityOptions>(options => {
                //password config
                options.Password.RequireDigit = false; 
                options.Password.RequireLowercase = false; 
                options.Password.RequireNonAlphanumeric = false; 
                options.Password.RequireUppercase = false; 
                options.Password.RequiredLength = 3; 
                options.Password.RequiredUniqueChars = 0; 

                //Lockout
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5); 
                options.Lockout.MaxFailedAccessAttempts = 5; 
                options.Lockout.AllowedForNewUsers = true;

                //User.
                options.User.AllowedUserNameCharacters = 
                    "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@+";
                options.User.RequireUniqueEmail = true;  

                //// Login.
                //options.SignIn.RequireConfirmedEmail = true;            
                //options.SignIn.RequireConfirmedPhoneNumber = false;     

            });
            services.ConfigureApplicationCookie(options =>
            {
                // Cookie settings
                options.Cookie.HttpOnly = true;
                options.ExpireTimeSpan = TimeSpan.FromMinutes(5);

                options.LoginPath = "/Identity/Account/Login";
                options.AccessDeniedPath = "/Identity/Account/AccessDenied";
                options.SlidingExpiration = true;
            });


            services.AddControllers().AddNewtonsoftJson(options =>
            options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);
            services.AddScoped<IBrandService, BrandService>();
            services.AddScoped<IDistributorService, DistributorService>();
            services.AddScoped<IImportProductService, ImportProductService>();
            services.AddScoped<ICategoryService, CategoryService>();
            services.AddScoped<IProductService, ProductService>();
            services.AddScoped<IDiscountService, DiscountService>();
            services.AddScoped<IOutportService, OutportService>();
            services.AddScoped<ICommentService, CommentService>();
            services.AddScoped<INotifyService, NotifyService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IStoreService, StoreService>();
            services.AddScoped<IGeneralService, GeneralService>();
            services.AddScoped<IPhoneHelper, PhoneHelper>();

            services.AddSwaggerGen(swagger =>
            {
                //This is to generate the Default UI of Swagger Documentation  
                swagger.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo
                {
                    Version = "v1",
                    Title = "K300 API",
                    Description = "Death is like a wind, alway by my side :D"
                });
                // To Enable authorization using Swagger (JWT)  
                swagger.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme()
                {
                    Name = "Authorization",
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer",
                    BearerFormat = "JWT",
                    In = ParameterLocation.Header,
                    Description = "JWT Authorization header using the Bearer scheme. \r\n\r\n Enter 'Bearer' [space] and then your token in the text input below.\r\n\r\nExample: \"Bearer 12345abcdef\"",
                });
                swagger.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                          new OpenApiSecurityScheme
                            {
                                Reference = new OpenApiReference
                                {
                                    Type = ReferenceType.SecurityScheme,
                                    Id = "Bearer"
                                }
                            },
                            new string[] {}

                    }
                });
            });
            
            services.AddCors(options =>
            {
                options.AddPolicy(name: _myCors,
                    builder =>
                    {
                        builder.AllowAnyOrigin()
                               .AllowAnyHeader()
                               .AllowAnyMethod();
                    });
            });
            services.AddElastichSearch(Configuration);

            services.AddDistributedMemoryCache();
            services.AddDistributedRedisCache(option =>
            {
                option.Configuration = Configuration["RedisSetting:RedisConfiguration"];
                option.InstanceName = Configuration["RedisSetting:RedisInstanceName"];
            });
            services.AddScoped<IRedisCacheHelper, RedisCacheHelper>();
            services.AddScoped<ISendMailHelper, SendMailHelper>();
            services.AddScoped<IElastichsearchHelper, ElastichsearchHelper>();
            services.AddScoped<IBotTelegramHelper, BotTelegramHelper>();
            services.AddScoped<IGGSheetHelper, GGSheetHelper>();
            services.AddMemoryCache();
            var scheduler = StdSchedulerFactory.GetDefaultScheduler().GetAwaiter().GetResult();
            // Add Quartz services
            //services.AddSingleton<IJobFactory, K300JobFactory>();
            //services.AddSingleton<ISchedulerFactory, StdSchedulerFactory>();
            services.AddScoped<StatisticsByMonthJob>();
            services.AddSingleton<IScheduler>(ConfigureQuartz(services));


            services.AddAuthentication(option =>
            {
                option.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                option.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = Configuration["Jwt:Issuer"],
                    ValidAudience = Configuration["Jwt:Audience"],
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt:Key"])) //Configuration["JwtToken:SecretKey"]  
                };
            });
            services.AddSignalR();
            //services.AddSingleton<IUserIdProvider, EmailBasedUserIdProvider>();
        }
        public IScheduler ConfigureQuartz(IServiceCollection services)
        {
            NameValueCollection props = new NameValueCollection()
            {
                ["quartz.serializer.type"] = "binary"
            };
            StdSchedulerFactory factory = new StdSchedulerFactory(props);
            var scheduler = factory.GetScheduler().Result;
            new JobSchedulerConfig(scheduler).InitializeJob();
            scheduler.JobFactory = new K300JobFactory(services.BuildServiceProvider());
            scheduler.Start().Wait();
            return scheduler;
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            //app.UseHttpsRedirection();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "K300 API");
            });


            app.UseRouting();
            app.UseCors(builder => builder
    .AllowAnyOrigin()
    .AllowAnyMethod()
    .AllowAnyHeader());
            app.UseAuthentication();
            app.UseAuthorization();  

            app.UseCors(_myCors);
            app.UseStaticFiles();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHub<QuantityHub>("/quantityHub", options => 
                {
                    options.Transports =
                        HttpTransportType.WebSockets |
                        HttpTransportType.LongPolling;
                });
                endpoints.MapHub<CommentHub>("/commentHub", options =>
                {
                    options.Transports =
                        HttpTransportType.WebSockets |
                        HttpTransportType.LongPolling;
                });
                endpoints.MapHub<NotifyHub>("/notifyHub", options =>
                {
                    options.Transports =
                        HttpTransportType.WebSockets |
                        HttpTransportType.LongPolling;
                });
                endpoints.MapHub<ChatHub>("/chatHub", options =>
                {
                    options.Transports =
                        HttpTransportType.WebSockets |
                        HttpTransportType.LongPolling;
                });
            });
        }
    }
    public static class ElasticsearchExtensions
    {
        public static void AddElastichSearch(this IServiceCollection services, IConfiguration configuration)
        {
            var url = configuration["elasticsearch:url"];
            var defaultIndex = configuration["elasticsearch:index"];
            var setting = new ConnectionSettings(new Uri(url)).DefaultIndex(defaultIndex);
            var client = new ElasticClient(setting);
            services.AddSingleton<IElasticClient>(client);
        }
    }
}
