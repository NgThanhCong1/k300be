﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Models.ResponseModels
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder builder)
        {

            base.OnModelCreating(builder);
            foreach (var entityType in builder.Model.GetEntityTypes())
            {
                var tableName = entityType.GetTableName();
                if (tableName.StartsWith("AspNet"))
                {
                    entityType.SetTableName(tableName.Replace("AspNet", ""));
                }
            }
        }
    }
    //public class CustomerDbContext : IdentityDbContext<Customer>
    //{
    //    public CustomerDbContext(DbContextOptions<CustomerDbContext> options) : base(options) { }
    //    protected override void OnModelCreating(ModelBuilder builder)
    //    {

    //        base.OnModelCreating(builder);
    //        foreach (var entityType in builder.Model.GetEntityTypes())
    //        {
    //            var tableName = entityType.GetTableName();
    //            if (tableName.StartsWith("AspNet"))
    //            {
    //                entityType.SetTableName(tableName.Replace("AspNet", "Customer"));
    //            }
    //        }
    //    }
    //}
}
