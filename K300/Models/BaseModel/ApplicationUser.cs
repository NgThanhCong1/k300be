﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Models.ResponseModels
{
    public class ApplicationUser : IdentityUser
    {
        public string Address { get; set; }
        public string Avatar { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime CreatedAt { get; set; }
        public int IsActive { get; set; }
    }
    public class ApplicationUserViewModel
    {
        public List<ApplicationUser> ListApplicationUsers { get; set; }
        public Pagination Pagination { get; set; }
    }
    public class RolePermission
    {
        public string Role { get; set; }
        public string Object { get; set; }
        public string Permission { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
    } 
    public class RefreshToken
    {
        public string Token { get; set; }
    }
}
