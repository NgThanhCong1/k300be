﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.BaseModel
{
    public class DataToGmail
    {
        public string mailTemplate { get; set; }
        public string from { get; set; }
        public string to { get; set; }
        public string subject { get; set; }
        public string title { get; set; }
        public string mainContent { get; set; }
        public string path { get; set; }
        public string outInvoiceId { get; set; }
        public string deliveryAddress { get; set; }
        public string totalMoney { get; set; }
    }
}
