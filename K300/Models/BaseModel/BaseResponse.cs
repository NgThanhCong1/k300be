﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.BaseModel
{
    public class BaseResponse<T>
    {
        public bool Result { get; set; }
        public int Code { get; set; }
        public T Data { get; set; }
        public string Message { get; set; }
        public BaseResponse(T data, int code, string message)
        {
            //int.TryParse(code, out int c);
            Code = code;
            Result = code == 1 ? true : false;
            Data = data;
            Message = message;
        }
    }
}
