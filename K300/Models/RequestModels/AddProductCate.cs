﻿using Models.ResponseModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Models.RequestModels
{
    public class AddProductCate
    {
        public int ProductId { get; set; }
        public List<Category> ListCategorie { get; set; }
        public string CreatedBy { get; set; }
    }
}
