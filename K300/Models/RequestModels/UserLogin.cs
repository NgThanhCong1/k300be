﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.RequestModels
{
    public class UserLogin
    {
        public string username { get; set; }
        public string password { get; set; }
    }
}
