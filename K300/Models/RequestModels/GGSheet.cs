﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.RequestModels
{
    public class GGSheet
    {
        public string Link { get; set; }
        public string SheetName { get; set; }
        public string RangeData { get; set; }
    }
}
