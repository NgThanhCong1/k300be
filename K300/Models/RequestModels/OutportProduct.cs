﻿using Models.ResponseModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Models.RequestModels
{
    public class OutportProduct : HistoryOrders
    {
        public int CartQuantity { get; set; }
    }
    public class OutInvoiceRequest
    {
        public int Id { get; set; }
        public string StaffId { get; set; }
        public List<OutportProduct> ListOutportProducts { get; set; }
        public string TotalMoney { get; set; }
        public int DiscountId { get; set; }
        public string CustomerId { get; set; }
        public string ShipPhone { get; set; }
        public string ShipName { get; set; }
        public string ShipAt { get; set; }
        public int Status { get; set; }
        public string HistoryChanges { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedAt { get; set; }
        public string  PaymentMethod { get; set; }
        public string  ShippingCharge { get; set; }
        public List<OutInvoiceRequest> listOutInvoices { get; set; }

    }
    public class OutInvoiceRequestViewModel
    {
        public List<OutInvoiceRequest> ListOutInvoiceRequest { get; set; }
        public Pagination Pagination { get; set; }
    }
    public class OutInvoiceMaster
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public string ShipName { get; set; }
        public string ShipAt { get; set; }
        public string ShipPhone { get; set; }
        public string PaymentMethod { get; set; }
        public string HistoryChanges { get; set; }
        public int TotalQuantity { get; set; }
        public string TotalMoney { get; set; }
        public int Status { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedAt { get; set; }
        public string ShippingCharge { get; set; }
    }
    public class OutInvoiceDetailMaster
    {
        public OutInvoiceMaster OutInvoiceMaster { get; set; }
        public List<SubProduct> ListSubProducts { get; set; }
        public List<OutInvoiceRequest> ListHistoryChanges { get; set; }
    }
    public class HistoryOrders
    {
        public string Name { get; set; }
        public List<string> ListImagesLink { get; set; }
        public string ListImages { get; set; }
        public string Size { get; set; }
        public string Color { get; set; }
        public string Price { get; set; }
        public string Quantity { get; set; }
        public string Status { get; set; }
        public string Total { get; set; }
        public int ProductId { get; set; }
        public int OutInvoiceId { get; set; }
        public string ProductCode { get; set; }
        public string Slug { get; set; }
    }
    public class OutInvoiceForUserTracking : OutInvoiceRequest
    {
        public List<HistoryOrders> ListHistoryOrders { get; set; }
        public string Discount { get; set; }
    }
    public class OutInvoiceForUserTrackingViewModel
    {
        public Pagination Pagination { get; set; }
        public IEnumerable<OutInvoiceForUserTracking> ListOutinvoices { get; set; }
    }
}
