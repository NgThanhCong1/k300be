﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.RequestModels
{
    public class Rating
    {
        public int ProductId { get; set; }
        public int Rank { get; set; }
        public string CustomerId { get; set; }
    }
}
