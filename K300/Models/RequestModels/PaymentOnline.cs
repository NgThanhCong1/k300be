﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.RequestModels
{
    public class PaymentOnline
    {
        public OutInvoiceRequest OrderInfor { get; set; }
        public VNPayPayment PaymentInfor { get; set; }
    }
    public class VNPayPayment
    {
        public string Ip { get; set; }
        public int TotalMoney { get; set; }
        public string BankCode { get; set; }
    }
}
