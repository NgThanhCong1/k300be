﻿using Microsoft.AspNetCore.Http;
using Models.ResponseModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Models.RequestModels
{
    public class UserRequest
    {
        public string Id { get; set; }
        public string Address { get; set; }
        public string Avatar { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string FirstName { get; set; }
        public string Password { get; set; }
        public string LastName { get; set; }
        public IFormFile ImageFile { get; set; }
    }
    public class JWTInfor
    {
        public string AccessToken { get; set; }
        public string MyProperty { get; set; } = "Bearer";
        public int ExpireIn { get; set; } = (int)(DateTime.Now.AddDays(7) - DateTime.Now).TotalSeconds;
        public DateTime Issued { get; set; } = DateTime.Now;
        public DateTime ExpireDate { get; set; } = DateTime.Now.AddDays(7);
        public JWTInfor(string _accessToken)
        {
            AccessToken = _accessToken;
        }
    }
    public class UserResponseViewModel
    {
        public UserRequest User { get; set; }
        public JWTInfor JWTInfor { get; set; }
    }
    public class ConfirmEmail
    {
        public string UserId { get; set; }
        public string Code { get; set; }
        public string NewEmail { get; set; }
    }
    public class ChangeEmail:ConfirmEmail
    {
        public string OldEmail { get; set; }
    }
    public class ResetPassword
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string Code { get; set; }
        public string NewPassword { get; set; }
        public string OldPassword { get; set; }
    }
    public class Role
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
    public class RoleRequest
    {
        public Role Role { get; set; }
        public List<RolePermission> ListRolePermissions { get; set; }
    }
    public class AssignRole
    {
        public List<string> ListRoleNames { get; set; }
        public string UserName { get; set; }
    }
}
