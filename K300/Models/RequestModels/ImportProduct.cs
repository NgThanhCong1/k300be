﻿using Microsoft.AspNetCore.Http;
using Models.ResponseModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Models.RequestModels
{
    public class ImportProductRequest
    {
        public GGSheet GGSheet { get; set; }
        public int DistributorId { get; set; }
        public string CreatedBy { get; set; }
    }
    public class ImportProduct
    {
        public int DistributorId { get; set; }
        public string CreatedBy { get; set; }
        public List<ProductRequest> ListInProduct { get; set; }
    }
    public class ProductRequest
    {
        public string Name { get; set; }
        public string Price { get; set; }
        public string ProductCode { get; set; }
        public string Description { get; set; }
        public string Material { get; set; }
        public string UpdatedBy { get; set; }
    }
    public class ImportProductNew
    {
        public string ProductCode { get; set; }
        public int Quantity { get; set; }
        public string InPrice { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
    }
}
