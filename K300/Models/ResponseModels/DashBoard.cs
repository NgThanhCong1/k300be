﻿using Models.RequestModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Models.ResponseModels
{
    public class Static
    {
        public int BrandCount { get; set; }
        public int CategoryCount { get; set; }
        public int DiscountCount { get; set; }
        public int DistributorCount { get; set; }
        public int InInvoiceCount { get; set; }
        public int OutInvoiceCount { get; set; }
        public int ProductCount { get; set; }
        public int RoleCount { get; set; }
        public int KeywordCount { get; set; }
        public int UserCount { get; set; }
    }
    public class DashBoard
    {
        public Static Static { get; set; }
        public List<InInvoice> ListInInvoices { get; set; }
        public List<OutInvoiceRequest> ListOutInvoiceRequests { get; set; }
        public List<SubProductStatic> ListSubProductStatics { get; set; }
        public List<SearchStaticDashBoard> ListSearchStaticDashBoards { get; set; }
        public List<CategoryStatic> ListCategoryStatics { get; set; }
    }
}
