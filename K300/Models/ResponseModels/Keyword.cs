﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.ResponseModels
{
    public class SearchStatic
    {
        public int Id { get; set; }
        public string Keyword { get; set; }
        public string CustomerId { get; set; }
        public string CreatedAt { get; set; }
    }
    public class SearchStaticDashBoard : SearchStatic
    {
        public int KeywordCount { get; set; }
    }
}
