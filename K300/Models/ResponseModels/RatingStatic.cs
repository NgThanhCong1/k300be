﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.ResponseModels
{
    public class RatingStatic
    {
        public int TotalRatingPoint { get; set; }
        public int TotalRatingCount { get; set; }
        public string IsCustomerRated { get; set; }
    }
}
