﻿using Models.RequestModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Models.ResponseModels
{
    public class StaticByMonth
    {
        public List<ApplicationUser> ListNewCustomers { get; set; }
        public List<InInvoice> ListNewInInvoices { get; set; }
        public List<OutInvoiceForUserTracking> ListNewOutInvoices { get; set; }
        public List<SubProductStatic> ListTopSubProducts { get; set; }
        public List<SearchStaticDashBoard> ListKeywordRankings { get; set; }
        public List<CategoryStatic> ListCategoryRankings { get; set; }
    }
}
