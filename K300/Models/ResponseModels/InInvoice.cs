﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.ResponseModels
{
    public class InInvoiceResponse
    {
        public InInvoice InInvoice { get; set; }
        public List<InInvoiceDetail> ListInvoiceDetail {get; set;}

    }
    public class InInvoice
    {
        public int Id { get; set; }
        public string DistributorName { get; set; }
        public string TotalMoney { get; set; }
        public DateTime CreatedAt { get; set; }
        public string CreatedBy { get; set; }
    }
    public class InInvoiceDetail
    {
        public int InInvoiceId { get; set; }
        public string ProductCode { get; set; }
        public string InPrice { get; set; }
        public int Quantity { get; set; }
        public DateTime CreatedAt { get; set; }
        public string CreatedBy { get; set; }
    }
    public class ImportDetail
    {
        public int ProductId { get; set; }
        public int InInvoceId { get; set; }
        public string Name { get; set; }
        public string ProductCode { get; set; }
        public int IsComplete { get; set; }
        public int Quantity { get; set; }
        public string ProductConfigs { get; set; }
    }
    public class ImportDetailViewModel
    {
        public List<ImportDetail> ListImportDetails { get; set; }
        public Pagination Pagination { get; set; }
    }
    public class InInvoiceViewModel
    {
        public List<InInvoice> ListInInvoices { get; set; }
        public Pagination Pagination { get; set; }
    }
}
