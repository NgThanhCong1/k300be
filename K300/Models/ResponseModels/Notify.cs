﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.ResponseModels
{
    public class Notify
    {
        //type = [
        //1 - Pendding,
        //2 - Approve,
        //3 - Shipping,
        //4 - Recieve,
        //5 - Cancle
        //,6,7]
        public int Id { get; set; }
        public string CustomerId { get; set; }
        public string Image { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public int IsWatched { get; set; }
        public int InvoiceId { get; set; }
        public int Type { get; set; }
        public DateTime CreatedAt { get; set; }
        public string CreatedBy { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string UpdatedBy { get; set; }
    }
}
