﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace Models.ResponseModels
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Price { get; set; }
        public int IsPopular { get; set; }
        public int QuantityInStock { get; set; }
        public string ProductCode { get; set; }
        public int BrandId { get; set; }
        public string Description { get; set; }
        public string Material { get; set; }
        public string ProductConfigs { get; set; }
        public IFormFileCollection ImageFiles { get; set; }
        public string Images { get; set; }
        public List<string> ListImageLinks { get; set; }
        public DateTime CreatedAt { get; set; }
        public string CreatedBy { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string UpdatedBy { get; set; }
        public string ListCate { get; set; }
        public List<ProductCate> ListProductCate { get; set; } = new List<ProductCate>();
        public List<ProductConfig> ListProductConfigs { get; set; }
        public int StatusComplete { get; set; }
        public string BrandName { get; set; }
    }
    public class ProductViewModel
    {
        public List<Product> ListProduct { get; set; }
        public Pagination Pagination { get; set; }
        public List<Brand> ListBrand { get; set; }
        public List<Category> ListCate { get; set; }
        public List<string> ListImageLinks { get; set; }
    }
    public class ProductConfig
    {
        public string Size { get; set; }
        public string Color { get; set; }
        public int Quantity { get; set; }
    }
    public class Product_New
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Price { get; set; }
        public int IsPopular { get; set; }
        public int Quantity { get; set; }
        public int BrandId { get; set; }
        public string BrandName { get; set; }
        public string Description { get; set; }
        public string Material { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public string Slug { get; set; }
        public int SubProductCount { get; set; }
    }
    public class ProductCategory_New
    {
        public int ProductId { get; set; }
        public int CategoryId { get; set; }
    }
    public class AddProduct
    {
        public Product_New productNew { get; set; }
        public List<ProductCategory_New> listProductCategoryNews { get; set; }
        public List<SubProduct> ListSubProducts { get; set; }
    }
    public class SubProduct
    {
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string Price { get; set; }
        public int ProductId { get; set; }
        public string ListImages { get; set; }
        public List<string> ListImagesLink { get; set; } = new List<string>();
        public IFormFileCollection ListImageFiles { get; set; }
        public string Color { get; set; }
        public string Size { get; set; }
        public int Quantity { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
    }
    public class SubProductStatic : SubProduct
    {
        public int QuantitySellInMonth { get; set; }
        public string InPrice { get; set; }
        public string OutPrice { get; set; }
    }
    public class ProductViewModel_New
    {
        public List<Product_New> ListProducts { get; set; }
        public Pagination Pagination { get; set; }
    }
    public class DataForAddProduct
    {
        public List<Category> ListCategories { get; set; }
        public List<Brand> ListBrands { get; set; }
    }
    public class SubProductViewModel
    {
        public List<SubProduct> ListSubProducts { get; set; }
        public Pagination Pagination { get; set; }
    }
    public class CombineProduct
    {
        public int ProductId { get; set; }
        public string Name { get; set; }
        public int IsPopular { get; set; }
        public string Price { get; set; }
        public string ProductCode { get; set; }
        public string ListImages { get; set; }
        public string Slug { get; set; }
        public List<string> ListImagesLink { get; set; }
    }
    public class ProductDetail : CombineProduct
    {
        public string BrandName { get; set; }
        public string Size { get; set; }
        public string Color { get; set; }
        public int Quantity { get; set; }
        public string Category { get; set; }
        public string ProductDescription { get; set; }
        public string Material { get; set; }
        public string BrandDescription { get; set; }
    }
    public class GetProductById
    {
        public List<ProductDetail> ListProductDetails { get; set; }
        public List<CombineProduct> ListCombineProducts { get; set; }
    }
    public class GetProductBySlug
    {
        public List<ProductDetail> ListProductDetails { get; set; }
        public List<ProductDetail> ListCombineProducts { get; set; }
        public RatingStatic RatingStatic { get; set; }
    }
    public class CombineProductViewModel
    {
        public List<ProductDetail> ListCombineProducts { get; set; }
        public Pagination Pagination { get; set; }
    }
    public class GetDataForUpdateProduct
    {
        public List<SubProduct> ListSubProducts { get; set; }
        public Product_New Product_New { get; set; }
    }

    public class ProductInCart : ProductDetail
    {
        public int CartQuantity { get; set; }
        public int IsChecked { get; set; }
    }

    public class ShoppingCart
    {
        public List<ProductInCart> listProductsInCart { get; set; }
        public string Key { get; set; }
        public string UserIdKey { get; set; }
        public int Type { get; set; }
        public int NumberAdd { get; set; }
    }
}
