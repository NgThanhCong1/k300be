﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.ResponseModels
{
    public class ProductCate
    {
        public int ProductId { get; set; }
        public int CateId { get; set; }
    }
}
