﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.ResponseModels
{
    public class Discount
    {
        public int Id { get; set; }
        public string DiscountCode { get; set; }
        public string Price { get; set; }
        public int Quantity { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
    }
    public class DiscountViewModel
    {
        public List<Discount> ListDiscount { get; set; }
        public Pagination Pagination { get; set; }
    }
}
