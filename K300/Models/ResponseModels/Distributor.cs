﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.ResponseModels
{
    public class Distributor
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
    }
    public class DistributorViewModel
    {
        public List<Distributor> ListDistributor { get; set; }
        public Pagination Pagination { get; set; }
    }
}
