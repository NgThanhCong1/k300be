﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.ResponseModels
{
    public class Pagination
    {
        public int _page { get; set; }
        public int _limit { get; set; }
        public int _totalRows { get; set; }
    }
}
