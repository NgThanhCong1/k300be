﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace Models.ResponseModels
{
    public class Store
    {
        public int Id { get; set; }
        public string Image { get; set; }
        public IFormFileCollection ImageFiles { get; set; }
        public List<string> ListImageLinks { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
    }
}
