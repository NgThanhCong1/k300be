﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.ResponseModels
{
    public class DirectChat
    {
        public string StaffId { get; set; }
        public string StaffAvatar { get; set; }
        public string Content { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
