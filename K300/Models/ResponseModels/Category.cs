﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace Models.ResponseModels
{
    public class Category
    {
        public int Id { get; set; }
        public int ParentCategory { get; set; }
        public string Name { get; set; }
        public string ParentName { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public IFormFileCollection ImageFiles { get; set; }
        public List<string> ListImageLinks { get; set; }
        public string Images { get; set; }
        public List<Category> ListCategories { get; set; }
    }
    public class CategoryStatic : Category
    {
        public int BuyQuantityOfCategoryInMonth { get; set; }
    }
    public class CateViewModel
    {
        public List<Category> ListCate { get; set; }
        public Pagination Pagination { get; set; }
    }
}
